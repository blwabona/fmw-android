package views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import controllers.Controller;
import mobi.wimt.findmyway.ExpandedFavouritesAndRecents;
import mobi.wimt.findmyway.FavouriteOrRecent;
import mobi.wimt.findmyway.MainActivity;
import mobi.wimt.findmyway.NestedDynamicList;
import mobi.wimt.findmyway.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import adapters.FavouritesAndRecentsRecycler;
import sqldatabase.SQLiteHelperFavouritesAndRecents;

/**
 * The favourites card stores all of the users favourite
 * and recent locations. the items are displayed in a list view
 * for which there is a custom adapter and can be replicated for expanding the list
 * All the recents and favourites are stored in a arraylist and can be added to if required
 * Created by James on 2015-01-12.
 */
public class FavouritesCard extends Card implements View.OnClickListener,NestedDynamicList {

    public Activity theView;
    public int priority;//stores the cards priority in the pile
    public LinearLayout theScrollLayout;
    public View favouritesCard;
    public ArrayList<FavouriteOrRecent> favouriteAndRecents;
    public Controller theController;//stores the controller with all the cards which allows access to other cards
    public Button All;
    public LinearLayout theRootLayout;
    public ArrayList<FavouriteOrRecent> addressFavouritesStates;
    public boolean searchScreenFavourites;
    public TextView noFavouritesOrRecents;

    public FavouritesCard(Controller cardsController, Activity theView, int priority, LinearLayout theScrollLayout,boolean searchScreenFavourites) {
        this.priority = priority;
        this.searchScreenFavourites = searchScreenFavourites;
        this.theView = theView;
        this.theScrollLayout = theScrollLayout;
        this.theController = cardsController;
        favouriteAndRecents = new ArrayList<>();
        addressFavouritesStates = new ArrayList<>();
        setupViewElements();
    }

    @Override
    public void setupViewElements() {
       favouritesCard = theView.getLayoutInflater().inflate(R.layout.favouritescard, theScrollLayout, false);//inflate the correct layouts
       theRootLayout = (LinearLayout)favouritesCard.findViewById(R.id.rootFavouritesLayout);
       noFavouritesOrRecents = (TextView)favouritesCard.findViewById(R.id.noFavouritesOrRecents);
       noFavouritesOrRecents.setVisibility(View.GONE);
       All = (Button) favouritesCard.findViewById(R.id.buttonAll);
       All.setVisibility(View.GONE);
       All.setOnClickListener(this);
       loadFavouritesAndRecentsFromDatabase();
       addFavouritesAndRecentsToCard();
    }

    private void loadFavouritesAndRecentsFromDatabase() {
        SQLiteHelperFavouritesAndRecents db = MainActivity.theFavouritesAndRecentsDatabase;
        ArrayList<FavouriteOrRecent> list = db.getAllFavouritesAndRecents();
        list = sortFavouritesAndRecentsBasedOnDateAdded(list);
        favouriteAndRecents.clear();
        favouriteAndRecents.addAll(list);

    }

    private ArrayList<FavouriteOrRecent> sortFavouritesAndRecentsBasedOnDateAdded(ArrayList<FavouriteOrRecent> list) {
        Collections.sort(list, new Comparator<FavouriteOrRecent>() {
            @Override
            public int compare(FavouriteOrRecent fav1, FavouriteOrRecent fav2) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd:MMMM:yyyy HH:mm:ss a");
                Date dateFav1 = null;
                Date dateFav2 = null;
                try {
                    dateFav1 = sdf.parse(fav1.getDateLastModified());
                    dateFav2 = sdf.parse(fav2.getDateLastModified());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (dateFav1 != null && dateFav2 != null) {
                    return dateFav2.compareTo(dateFav1);
                } else {
                    return 0;
                }
            }
        });
        return list;
    }

    private void addFavouritesAndRecentsToCard() {//pass via an intent?
        noFavouritesOrRecents.setVisibility(View.GONE);
        LayoutInflater theInflater = (LayoutInflater) theView.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(favouriteAndRecents.size()<1){
            noFavouritesOrRecents.setVisibility(View.VISIBLE);
        }

        if(favouriteAndRecents.size()>3){
            All.setVisibility(View.VISIBLE);
        }
        else{
            All.setVisibility(View.GONE);
        }

        for(int i=0;i<favouriteAndRecents.size();i++){
            if(i>2){
                break;
            }
            else {
                View theRowView = theInflater.inflate(R.layout.favouriteline, null, false);
                ImageView theHeart = (ImageView) theRowView.findViewById(R.id.favouriteHeart);
                TextView theNameLabel = (TextView) theRowView.findViewById(R.id.FavouriteLineLabel);
                FavouriteOrRecent item = new FavouriteOrRecent(favouriteAndRecents.get(i).getDateLastModified(),
                        favouriteAndRecents.get(i).getName(),
                        favouriteAndRecents.get(i).getAddress(),
                        theHeart,
                        favouriteAndRecents.get(i).isFavourite,
                        this,
                        theNameLabel,
                        null,
                        theRowView,
                        favouriteAndRecents.get(i).getLatitude(),
                        favouriteAndRecents.get(i).getLongitude());

                addressFavouritesStates.add(item);
                theRootLayout.addView(theRowView);
                theRowView.setClickable(true);
                theRowView.setOnClickListener(this);
            }
        }
    }

    @Override
    public int getPriority(){
        return priority;
    }

    @Override
    public void addCard(){
        theScrollLayout.addView(favouritesCard);
    }

    private void expandCard() {
        Intent intent;
        intent = new Intent(theView, ExpandedFavouritesAndRecents.class);
        theView.startActivityForResult(intent,1);
    }

    @Override
    public void onClick(View v) {
       int id = v.getId();
       if(id == R.id.buttonAll){
        expandCard();
       }
       else if(getRowItem(v)!=null){
           changeDestinationBox(v);
       }
    }

    private void changeDestinationBox(View v) {
        getRowItem(v).setDateLastModified(getCurrentDate());
        SQLiteHelperFavouritesAndRecents theHelper = MainActivity.theFavouritesAndRecentsDatabase;
        theHelper.updateFavouriteOrRecent(getRowItem(v));
        if(searchScreenFavourites == true){
            FavouritesAndRecentsRecycler temp = (FavouritesAndRecentsRecycler) theView;
            temp.returnResultToParent(getRowItem(v));
        }
        else {
            theController.changeResultBox(getRowItem(v), 2);
        }

    }

    public FavouriteOrRecent getCorrectItem(View v){
        FavouriteOrRecent temp = null;
        for (FavouriteOrRecent favourite : addressFavouritesStates) {
            if(favourite.view.itemHeart.equals(v)){
                temp = favourite;
            }
        }
        return temp;
    }

    public FavouriteOrRecent getRowItem(View v){
        FavouriteOrRecent temp = null;
        for (FavouriteOrRecent favourite : addressFavouritesStates) {
            if(favourite.view.rowView.equals(v)){
                temp = favourite;
            }
        }
        return temp;
    }

    public void removeCard(){
        theScrollLayout.removeView(favouritesCard);
    }

    public void refreshFavourites(){
        theRootLayout.removeAllViews();
        addressFavouritesStates.clear();
        theRootLayout.addView(noFavouritesOrRecents);
        loadFavouritesAndRecentsFromDatabase();
        addFavouritesAndRecentsToCard();
    }

    public String getCurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd:MMMM:yyyy HH:mm:ss a");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }


}
