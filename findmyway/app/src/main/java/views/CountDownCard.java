package views;

import android.app.Activity;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import mobi.wimt.findmyway.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import controllers.CardsController;

/**
 * Created by James on 2015-03-17.
 */
public class CountDownCard extends Card implements View.OnClickListener {

    private Activity theView;
    private int priority;//stores the cards priority in the pile
    private LinearLayout theScrollLayout;
    private View countDownCard;//this cards layout
    public CardsController theController;//stores the controller with all the cards which allows access to other cards
    private ImageButton dismissCard;
    public Date competitionDate;
    public Date currentDate;
    public TextView hours;
    public TextView minutes;
    public TextView seconds;
    public TextView days;
    public long milliDifference;

    public CountDownCard(CardsController cardsController, Activity theView, int priority, LinearLayout theScrollLayout,String currentDate,String competitionDate){
        this.theController = cardsController;
        this.theView = theView;
        this.priority = priority;
        this.theScrollLayout  =theScrollLayout;
        setupViewElements();
        setDates(currentDate,competitionDate);
    }

    @Override
    public void setupViewElements() {
        countDownCard = theView.getLayoutInflater().inflate(R.layout.count_down_card, theScrollLayout, false);
        days = (TextView) countDownCard.findViewById(R.id.DaysLabel);
        hours = (TextView)countDownCard.findViewById(R.id.HoursLabel);
        minutes = (TextView) countDownCard.findViewById(R.id.MinutesLabel);
        seconds = (TextView) countDownCard.findViewById(R.id.SecondsLabel);
        dismissCard = (ImageButton)countDownCard.findViewById(R.id.dismissTimer);
        dismissCard.setOnClickListener(this);
    }


    public void setDates(String currentDate,String competitionDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyy-MM-dd'T'HH:mm:ss.SSSZ");

        try {
            this.currentDate = sdf.parse(currentDate);
            this.competitionDate = sdf.parse(competitionDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long different = this.competitionDate.getTime() - this.currentDate.getTime();

        setTimer(different);

    }

    public void setTimer(long milliDifference) {
    }

    public void printDifference(long millisLeft){


        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = millisLeft / daysInMilli;
        millisLeft = millisLeft % daysInMilli;

        long elapsedHours = millisLeft / hoursInMilli;
        millisLeft = millisLeft % hoursInMilli;

        long elapsedMinutes = millisLeft / minutesInMilli;
        millisLeft = millisLeft % minutesInMilli;

        long elapsedSeconds = millisLeft / secondsInMilli;

        if(elapsedHours == 1){
            hours.setText(elapsedHours+" Hour");
        }
        else{
            hours.setText(elapsedHours+" Hours");
        }

        if(elapsedSeconds == 1){
            seconds.setText(elapsedSeconds+" Second");
        }
        else{
            seconds.setText(elapsedSeconds+" Seconds");
        }

        if(elapsedDays == 1){
            days.setText(elapsedDays+" Day");
        }
        else{
            days.setText(elapsedDays+" Days");
        }

        if(elapsedMinutes == 1){
            minutes.setText(elapsedMinutes+" Minute");
        }
        else{
            minutes.setText(elapsedMinutes+" Minutes");
        }

    }

    @Override
    public int getPriority(){
        return priority;
    }

    @Override
    public void addCard(){
        theScrollLayout.addView(countDownCard);
    }

    @Override
    public void removeCard(){
        theController.activeCards.remove(this);
        theScrollLayout.removeView(countDownCard);
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id) {
            case R.id.dismissTimer:
                removeCard();
                theController.activeCards.remove(this);
                break;
        }

    }
}
