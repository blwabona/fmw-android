package views;

/**
 * Created by James on 2015-01-12.
 * This is the simple base card that every other card will inherit from
 */
public class Card {

    public void setupViewElements() {
    }

    public int getPriority(){
        return 1;
    }

    public void addCard(){

    }

    public void removeCard(){

    }
}
