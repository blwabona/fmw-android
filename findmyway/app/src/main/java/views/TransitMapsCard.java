package views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import controllers.Controller;
import mobi.wimt.findmyway.ExpandedOperators;
import mobi.wimt.findmyway.Operator;
import mobi.wimt.findmyway.R;

import java.util.ArrayList;

/**
 * Created by James on 2015-01-22.
 */
public class TransitMapsCard extends Card implements View.OnClickListener {

    public Activity theView;
    public int priority;//stores the cards priority in the pile
    public LinearLayout theScrollLayout;
    public View TransitMapsCardView;
    public Controller theController;//stores the controller with all the cards which allows access to other cards
    public Button ExpandOperators;
    public LinearLayout theRootMapsLayout;
    public RelativeLayout operatorLayout;
    public ArrayList<Operator> operatorOptions;
    public Operator theActiveOperator;

    public TransitMapsCard(Controller cardsController, Activity theView, int priority, LinearLayout theScrollLayout){
        this.theController = cardsController;
        this.theView = theView;
        this.priority = priority;
        this.theScrollLayout = theScrollLayout;
        setupViewElements();
        getNearbyOperators();
        setActiveOperator();
        addMapsToLayout();
    }

    @Override
    public void setupViewElements() {
        TransitMapsCardView = theView.getLayoutInflater().inflate(R.layout.transitmapscard, theScrollLayout, false);//inflate the correct layouts
        theRootMapsLayout = (LinearLayout)TransitMapsCardView.findViewById(R.id.ListLayoutMaps);
        ExpandOperators = (Button)TransitMapsCardView.findViewById(R.id.expandOperators);
        operatorLayout = (RelativeLayout)TransitMapsCardView.findViewById(R.id.operatorsLayout);
        ExpandOperators.setOnClickListener(this);
    }

    @Override
    public int getPriority(){
        return priority;
    }

    @Override
    public void addCard(){
        theScrollLayout.addView(TransitMapsCardView);
    }


    public void refreshMapsForNewOperator(){
       theRootMapsLayout.removeAllViews();
       addMapsToLayout();
    }

    public void addMapsToLayout(){
        LayoutInflater theInflater = (LayoutInflater) theView.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for(int i=0;i<theActiveOperator.operatorMaps.size();i++) {
            View MapLineItem= theInflater.inflate(R.layout.maps_line, null, false);
            ImageView eye = (ImageView) MapLineItem.findViewById(R.id.eyeHidden);
            TextView mapAreaName = (TextView) MapLineItem.findViewById(R.id.mapAreaName);
            mapAreaName.setText(theActiveOperator.operatorMaps.get(i));
            theRootMapsLayout.addView(MapLineItem);
            MapLineItem.setClickable(true);
            MapLineItem.setOnClickListener(this);
        }

    }

    @Override
    public void removeCard(){//method could be improved remember to remove card from controller active cards
        theScrollLayout.removeView(TransitMapsCardView);
    }

    public void addOperatorOption(int resourceExpanded,String type){
        ImageView logo = new ImageView(theView);
        Operator newOperator = new Operator(logo,"TcT",resourceExpanded,true,type);
        operatorOptions.add(newOperator);
    }

    public void getNearbyOperators(){
        operatorOptions = new ArrayList<Operator>();
        ImageView logo = new ImageView(theView);
        for(int i = 0;i<3;i++){
            Operator newOperator = new Operator(logo,"Jammie",R.drawable.jammielarge,true,"Bus Service");
            operatorOptions.add(newOperator);
            addOperatorToOptions(newOperator.operatorLogo);
        }
    }

    public void setActiveOperator(){
        theActiveOperator = operatorOptions.get(0);
    }

    public void addOperatorToOptions(ImageView logo){
        //operatorLayout.addView(logo);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id){
            case R.id.expandOperators:
                Intent intent = new Intent(theView, ExpandedOperators.class);
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("operator-options", operatorOptions);
                intent.putExtras(bundle);
                theView.startActivity(intent);
                break;
        }
    }

}
