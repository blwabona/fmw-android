package views;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import controllers.CardsController;
import java.util.TimeZone;
import mobi.wimt.findmyway.DialogDateTimePicker;
import mobi.wimt.findmyway.DialogIntervalSetter;
import mobi.wimt.findmyway.FavouriteOrRecent;
import mobi.wimt.findmyway.MainActivity;
import mobi.wimt.findmyway.PathQueryHolder;
import mobi.wimt.findmyway.R;
import mobi.wimt.findmyway.ResultsActivity;
import mobi.wimt.findmyway.Search;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * Created by James on 2015-01-12.
 * The locations destination card deals with the clicks on the
 * arriving and departing buttons as well as a now button that
 * opens a timepicker
 */
public class LocationDestinationCard extends Card implements View.OnClickListener {

    public Activity theView;
    public int priority;
    public Button arriving;
    public Button departing;
    public View locationsDestination;
    public LinearLayout theScrollLayout;
    public Button Now;
    public CardsController theController;
    public TextView destinationBox;
    public TextView currentLocationBox;
    public Button findmywayButton;
    public FavouriteOrRecent startLocationFavouriteOrRecent;
    public FavouriteOrRecent destinationFavouriteOrRecent;
    public ImageView iconStartAddress;
    public ImageView iconendAddress;
    public ImageView swapper;
    public String  startDate;
    public String endDate;
    public boolean loadingGeoLocation = false;
    public boolean departingEnabled = true;
    public static final String ADDITIONALMODES = "ToggleSettingsAdditionalOperators";

    public LocationDestinationCard(CardsController cardsController, Activity theView, int priority, LinearLayout theScrollLayout) {

        this.theController = cardsController;
        this.priority = priority;
        this.theView = theView;
        this.theScrollLayout = theScrollLayout;
        setupViewElements();
    }

    public void querySettings() {
        if(theController.QueryAutoPopulate() == true){
            theController.geocodeLastLocation();
        }
    }

    public void setGeoCodedFavourite(FavouriteOrRecent location){
        if(location !=null){
            if(startLocationFavouriteOrRecent == null) {
                changeCurrentLocationBox(location.getName());
                startLocationFavouriteOrRecent = location;
            }
        }
        else{
            System.out.println("Failed to load");
            currentLocationBox.setText("");
        }
    }

    @Override
    public void setupViewElements() {

        locationsDestination = theView.getLayoutInflater().inflate(R.layout.locationdestinationcard, theScrollLayout, false);

        destinationBox = (TextView)locationsDestination.findViewById(R.id.Destination);
        currentLocationBox = (TextView)locationsDestination.findViewById(R.id.CurrentLocation);
        arriving = (Button) locationsDestination.findViewById(R.id.buttonArriving);
        departing = (Button) locationsDestination.findViewById(R.id.buttonDepart);
        Now = (Button) locationsDestination.findViewById(R.id.buttonNow);
        findmywayButton = (Button)locationsDestination.findViewById(R.id.findmywayButton);
        swapper = (ImageView) locationsDestination.findViewById(R.id.IconSwap);
        iconStartAddress = (ImageView)locationsDestination.findViewById(R.id.IconCurrent);
        iconendAddress = (ImageView)locationsDestination.findViewById(R.id.IconDestination);

        findmywayButton.setVisibility(View.GONE);

        Spannable buttonLabel = new SpannableString("Now");
        Now.setText(buttonLabel);

        Spannable buttonFMW = new SpannableString("FINDMYWAY");
        //buttonFMW.setSpan(new ImageSpan(theView.getApplicationContext(), R.drawable.logo), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        findmywayButton.setText(buttonFMW);

        destinationBox.setOnClickListener(this);
        currentLocationBox.setOnClickListener(this);
        arriving.setOnClickListener(this);
        departing.setOnClickListener(this);
        findmywayButton.setOnClickListener(this);
        Now.setOnClickListener(this);
        swapper.setOnClickListener(this);

        startDate = null;
        endDate = null;

    }

    @Override
    public int getPriority(){
        return priority;
    }

    @Override
    public void addCard(){
        theScrollLayout.addView(locationsDestination);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Intent intent;
        switch(id){
            case R.id.buttonArriving:
                arriving.setBackgroundResource(R.color.findmywayPrimaryColor);
                departing.setBackgroundResource(R.color.findmywayCardBackgroundColor);
                if(endDate == null) {
                    Now.setText("Soonest");
                }
                else{
                    updateTimeButtonEnd();
                }
                departingEnabled = false;
                break;
            case R.id.buttonDepart:
                departing.setBackgroundResource(R.color.findmywayPrimaryColor);
                arriving.setBackgroundResource(R.color.findmywayCardBackgroundColor);
                departingEnabled = true;
                if(startDate == null){
                    departing.setText("Departing");
                    Now.setText("Now");
                }
                else if(queryTimeDifference(startDate) || startDate.equals(getCurrentDateAndTime())){
                    departing.setText("Departing");
                    Now.setText("Now");
                    startDate = null;
                }
                else{
                    departing.setText("Departing in");
                    String deltaTime = updateInterval(startDate);
                    if(startDate == null){
                        Now.setText("Now");
                    }
                    else {
                        updateTimeButtonStart(deltaTime);
                    }
                 }
                break;
            case R.id.buttonNow:
                if(departingEnabled == true){
                    DialogIntervalSetter d = new DialogIntervalSetter(theView,this);
                    d.show();
                }
                else{
                    DialogDateTimePicker d = new DialogDateTimePicker(theView,this);
                    d.show();
                }
                break;
            case R.id.Destination:
                intent = new Intent(theView, Search.class);
                theView.startActivityForResult(intent,2);
                break;
            case R.id.CurrentLocation:
                intent = new Intent(theView, Search.class);
                theView.startActivityForResult(intent,1);
                break;
            case R.id.IconSwap:
                swapLocations();
                break;
            case R.id.findmywayButton:
                QueryDatesAndTimes();
                PathQueryHolder theQuery = new PathQueryHolder();
                theQuery.setEndLatitude(destinationFavouriteOrRecent.latitude);
                theQuery.setEndLongitude(destinationFavouriteOrRecent.longitude);
                theQuery.setStartLatitude(startLocationFavouriteOrRecent.latitude);
                theQuery.setStartLongitude(startLocationFavouriteOrRecent.longitude);
                theQuery.setStartDate(startDate);
                theQuery.setEndDate(endDate);
                theQuery.setExcludedModes(seralizeModes(MainActivity.theMenuFilter.getMenuStates()));
                theQuery.setNameDestination(destinationFavouriteOrRecent.getName());
                theQuery.setNameLocation(startLocationFavouriteOrRecent.getName());
                theQuery.setExcludedOperators(convertToString(getExcludedOperators()));
                intent = new Intent(theView, ResultsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable("theQuery", theQuery);
                intent.putExtras(bundle);
                theView.startActivityForResult(intent, 10);
                break;
        }
    }

    private String updateInterval(String theEndDate) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        String current = sdf.format(c.getTime());
        String diff = "";

        if(queryTimeDifference(theEndDate)){
            diff = "Now";
            departing.setText("Departing");
            startDate = null;
        }
        else {
            Date Date1 = null;
            Date Date2 = null;
            try {
                Date1 = sdf.parse(theEndDate);
                Date2 = sdf.parse(current);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long millse = Date1.getTime() - Date2.getTime();
            long mills = Math.abs(millse);

            int Hours = (int) (mills / (1000 * 60 * 60));
            int Mins = (int) (mills / (1000 * 60)) % 60;
            long Secs = (int) (mills / 1000) % 60;

            if (Hours == 0) {
                diff = " " + Mins + " mins"; // updated value every1 second
            } else if (Mins == 0) {
                diff = " " + Hours + " hours "; // updated value every1 second
            } else {
                diff = " " + Hours + "h " + Mins + " mins"; // updated value every1 second
            }
        }

        return diff;
    }

    public void updateTimeButtonEnd(){
        Now.setText(getCorrectEndDateFormat());
    }

    public void updateTimeButtonStart(String text){
        departing.setText("Departing in");
        Now.setText(text);
    }

    public void changeDestinationText(String text){
        if(checkLocationsAreNotEqual() == true){
            destinationFavouriteOrRecent = null;
            destinationBox.setText("");
            iconendAddress.setImageResource(R.drawable.ic_action_search);
        }
        else {
            destinationBox.setTextColor(Color.BLACK);
            destinationBox.setText(text);
        }
        queryDisplayOfFindMyWayButton();
    }

    public void changeCurrentLocationBox(String text ){
        if(checkLocationsAreNotEqual() == true){
            startLocationFavouriteOrRecent = null;
            currentLocationBox.setText("");
            iconStartAddress.setImageResource(R.drawable.ic_action_search);
        }
        else{
            currentLocationBox.setTextColor(Color.BLACK);
            currentLocationBox.setText(text);
        }
        queryDisplayOfFindMyWayButton();
    }

    public void setInterval(int hours,int minutes){
        String DateAndTime;
        DateAndTime = getCurrentDateAndTime();

        int startDateHour = Integer.valueOf(DateAndTime.substring(DateAndTime.indexOf('T') + 1, DateAndTime.indexOf(':')));
        int startDateMinute = Integer.valueOf(DateAndTime.substring(DateAndTime.indexOf(':') + 1, DateAndTime.length()));

        int newHour = Integer.MAX_VALUE;
        int newMinute = Integer.MAX_VALUE;

        int Year = Integer.valueOf(DateAndTime.substring(0, DateAndTime.indexOf('-')));
        int Month = Integer.valueOf(DateAndTime.substring(DateAndTime.indexOf('-') + 1, DateAndTime.lastIndexOf('-')));
        int Day = Integer.valueOf(DateAndTime.substring(DateAndTime.lastIndexOf('-') + 1, DateAndTime.indexOf('T')));

        if (minutes + startDateMinute >= 60) {
            startDateHour = startDateHour + 1;
            minutes = minutes + startDateMinute - 60;
            newMinute = minutes;
            if (hours + startDateHour >= 24) {
                hours = hours + startDateHour - 24;
                newHour = hours;
                Day = Day + 1;
                if ((Month % 2 != 0 && Day>31) || (Month % 2 ==0 && Day > 30)) {
                    Month = Month + 1;
                    Day = 1;
                    if (Month > 12) {
                        Month = 1;
                        Year = Year + 1;
                    }
                }
            }
        } else if (hours + startDateHour >= 24) {
            hours = hours + startDateHour - 24;
            newHour = hours;
            Day = Day + 1;
            if ((Month % 2 != 0 && Day>31) || (Month % 2 ==0 && Day > 30)) {
                Month = Month + 1;
                Day = 1;
                if (Month > 12) {
                    Month = 1;
                    Year = Year + 1;
                }
            }
        }

        if (newHour == Integer.MAX_VALUE && newMinute == Integer.MAX_VALUE) {
            setIntervalStartDate(Year, Month, Day, hours + startDateHour, minutes + startDateMinute);
        } else if (newHour == Integer.MAX_VALUE && newMinute != Integer.MAX_VALUE) {
            setIntervalStartDate(Year, Month, Day, hours + startDateHour, newMinute);
        } else if (newHour != Integer.MAX_VALUE && newMinute == Integer.MAX_VALUE) {
            setIntervalStartDate(Year, Month, Day, newHour, minutes + startDateMinute);
        } else {
            setIntervalStartDate(Year, Month, Day, newHour, newMinute);
        }
    }

    public void setIntervalStartDate(int Year,int Month,int Day,int hours,int minutes ){
        String tempDate;

        if(Month<10 && Day<10){
            tempDate = Year+"-0"+Month+"-0"+Day;
        }
        else if(Month<10){
            tempDate = Year+"-0"+Month+"-"+Day;
        }
        else if(Day<10){
            tempDate = Year+"-"+Month+"-0"+Day;
        }
        else{
            tempDate = Year+"-"+Month+"-"+Day;
        }

        if(hours<10 && minutes<10){
            startDate = tempDate + "T0" + hours + ":0" + minutes;
        }
        else if(hours<10){
            startDate = tempDate + "T0" + hours + ":" + minutes;
        }
        else if(minutes<10){
            startDate = tempDate + 'T' + hours + ":0" + minutes;
        }
        else {
            startDate = tempDate + 'T' + hours + ":" + minutes;
        }

        updateTimeButtonStart(updateInterval(startDate));
    }

    public String updateEndTime(int hours,int minutes){
        String DateAndTime;
        if(endDate == null){
            DateAndTime =  getCurrentDateAndTime();
        }
        else{
            DateAndTime = endDate;
        }

        if(hours<10 && minutes<10){
            return DateAndTime.substring(0,DateAndTime.indexOf('T'))+"T0"+hours+":0"+minutes;
        }
        else if(hours<10){
            return DateAndTime.substring(0,DateAndTime.indexOf('T'))+"T0"+hours+":"+minutes;
        }
        else if(minutes<10){
            return  DateAndTime.substring(0,DateAndTime.indexOf('T'))+"T"+hours+":0"+minutes;
        }
        else {
           return DateAndTime.substring(0,DateAndTime.indexOf('T')) + "T" + hours + ":" + minutes;
        }
    }

    public boolean checkLocationsAreNotEqual(){
        if(destinationFavouriteOrRecent != null && startLocationFavouriteOrRecent != null) {
            if (destinationFavouriteOrRecent.getLatitude() == startLocationFavouriteOrRecent.getLatitude() && destinationFavouriteOrRecent.getLongitude() == startLocationFavouriteOrRecent.getLongitude()) {
                Toast.makeText(theView, "Location and Destination cannot be the same", Toast.LENGTH_SHORT).show();
                return true;
            } else if (destinationFavouriteOrRecent.isUserCurrentLocation == true && startLocationFavouriteOrRecent.isUserCurrentLocation == true){
                Toast.makeText(theView, "Cannot set two current locations", Toast.LENGTH_SHORT).show();
                return true;
            }
            else {
                return false;
            }
        }
        else{
            return false;
        }
    }

    public void updateEndDate(int year,int month,int day) {
        int  hour;
        int minute;
        if(endDate == null) {
            hour = getHourFromDate(getCurrentDateAndTime());
            minute = getMinuteFromDate(getCurrentDateAndTime());
        }
        else{
            hour = getHourFromDate(endDate);
            minute = getMinuteFromDate(endDate);
        }

        if(month<10 && day<10){
            endDate = year+"-0"+month+"-0"+day+'T'+hour+ ":"+minute;
        }
        else if (month<10) {
            endDate = year + "-0" + month + "-" + day + 'T' + hour + ":" + minute;
        }
        else if(day<10){
            endDate = year + "-" + month + "-0" + day + 'T' + hour + ":" + minute;
        }
        else{
            endDate = year + "-" + month + "-" + day + 'T' + hour + ":" + minute;
        }
    }

    public void queryDisplayOfFindMyWayButton(){
        if (destinationFavouriteOrRecent == null || startLocationFavouriteOrRecent == null) {
            findmywayButton.setVisibility(View.GONE);
        } else {
            findmywayButton.setVisibility(View.VISIBLE);
        }
    }

    public FavouriteOrRecent getDestinationFavouriteOrRecent() {
        return destinationFavouriteOrRecent;
    }

    public void setDestinationFavouriteOrRecent(FavouriteOrRecent destinationFavouriteOrRecent) {
        this.destinationFavouriteOrRecent = destinationFavouriteOrRecent;
    }

    public FavouriteOrRecent getStartLocationFavouriteOrRecent() {
        return startLocationFavouriteOrRecent;
    }

    public void setStartLocationFavouriteOrRecent(FavouriteOrRecent startLocationFavouriteOrRecent) {
        this.startLocationFavouriteOrRecent = startLocationFavouriteOrRecent;
    }

    public String seralizeModes(Map<String,String> thefilters){
        String storageString = "";
        int counter = 0;
        for (Map.Entry<String, String> entry : thefilters.entrySet()) {
            if(entry.getValue().equals("Deactivated")){
                if(counter == 0){
                    storageString = entry.getKey();
                    counter++;
                }
                else{
                    storageString = storageString+","+entry.getKey();
                }
            }
        }
        return storageString;
    }

    public void swapLocations(){
        if(loadingGeoLocation != true){
            if(startLocationFavouriteOrRecent == null && destinationFavouriteOrRecent == null){
                Toast.makeText(theView, "Please enter either a location or destination", Toast.LENGTH_SHORT).show();
            }
            else if(startLocationFavouriteOrRecent == null&& destinationFavouriteOrRecent != null ){
                currentLocationBox.setText(destinationFavouriteOrRecent.getName());
                if(destinationFavouriteOrRecent.isUserCurrentLocation == true){
                    iconStartAddress.setImageResource(R.drawable.ic_action_location_found_black);
                }
                else{
                    iconStartAddress.setImageResource(R.drawable.markerblack);
                }
                currentLocationBox.setTextColor(Color.BLACK);
                destinationBox.setText("");
                iconendAddress.setImageResource(R.drawable.ic_action_search);
            }
            else if(startLocationFavouriteOrRecent != null && destinationFavouriteOrRecent == null ){
                destinationBox.setText(startLocationFavouriteOrRecent.getName());
                if(startLocationFavouriteOrRecent.isUserCurrentLocation == true){
                    iconendAddress.setImageResource(R.drawable.ic_action_location_found_black);
                }
                else{
                    iconendAddress.setImageResource(R.drawable.markerblack);
                }
                destinationBox.setTextColor(Color.BLACK);
                currentLocationBox.setText("");
                iconStartAddress.setImageResource(R.drawable.ic_action_search);
            }
            else{
                destinationBox.setText(startLocationFavouriteOrRecent.getName());
                if(startLocationFavouriteOrRecent.isUserCurrentLocation == true){
                    iconendAddress.setImageResource(R.drawable.ic_action_location_found_black);
                }
                else{
                    iconendAddress.setImageResource(R.drawable.markerblack);
                }

                if(destinationFavouriteOrRecent.isUserCurrentLocation == true){
                    iconStartAddress.setImageResource(R.drawable.ic_action_location_found_black);
                }
                else{
                    iconStartAddress.setImageResource(R.drawable.markerblack);
                }
                currentLocationBox.setText(destinationFavouriteOrRecent.getName());
                currentLocationBox.setTextColor(Color.BLACK);
            }

            FavouriteOrRecent temp = startLocationFavouriteOrRecent;
            startLocationFavouriteOrRecent = destinationFavouriteOrRecent;
            destinationFavouriteOrRecent = temp;
        }
    }

    public String getCurrentDateAndTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        String current = sdf.format(c.getTime());
        return current;
    }

    public void QueryDatesAndTimes(){
        if(startDate == null){
            startDate = getCurrentDateAndTime();
        }
        else if(queryTimeDifference(startDate)){
            startDate = getCurrentDateAndTime();
        }
        if(endDate != null && queryTimeDifference(endDate)){
            endDate = null;
        }


    }

    @Override
    public void removeCard(){
        theScrollLayout.removeView(locationsDestination);
    }

    public boolean queryTimeDifference(String inputtingCurrentDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date testingDate = null;
        Date currentDate = null;
        try {
            testingDate = sdf.parse(inputtingCurrentDate);
            currentDate = sdf.parse(getCurrentDateAndTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (testingDate != null && currentDate != null) {
            if(currentDate.after(testingDate)){
                return true;
            }
        } else {
            return false;
        }

        return false;
    }

    public String getCorrectEndDateFormat(){
        if(endDate!= null & endDate.equals(getCurrentDateAndTime())){
            endDate = null;
            return "Soonest";
        }
        else {
            int startIndex = endDate.indexOf('T');
            String theDate = endDate.substring(0, startIndex);
            String result = endDate.substring(startIndex + 1, endDate.length()).replace(':', 'h');
            String store = theDate + " " + result;
            return store;
        }
    }

    public int getHourFromDate(String date){
        int StartIndex = date.indexOf('T');
        int EndIndex = date.indexOf(':');
        return Integer.valueOf(date.substring(StartIndex+1,EndIndex));
    }

    public int getMinuteFromDate(String date){
        int StartIndex = date.indexOf(':');
        int EndIndex = date.length();
        return Integer.valueOf(date.substring(StartIndex+1,EndIndex));
    }

    public ArrayList<String> retrieveToggleFilterSettingsAdditional(){
        ArrayList<String> excludedOperators = new ArrayList<>();
        SharedPreferences prefs = theView.getSharedPreferences(ADDITIONALMODES, theView.MODE_PRIVATE);
        Map<String,?> keys = prefs.getAll();
        if(keys == null || keys.size()==0){
        }
        else {
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                if(entry.getValue().toString().equals("Deactivated")){
                    excludedOperators.add(entry.getKey());
                }
            }
        }
        return excludedOperators;
    }

    public String convertToString(String[] toConvert){
        String store = "";
        for(int i=0;i<toConvert.length;i++){
            if(i ==0){
                store = toConvert[i];
            }
            else{
                store = store+","+toConvert[i];
            }
        }
        return store;
    }

    public String[] getExcludedOperators(){
        ArrayList<String> additionals = retrieveToggleFilterSettingsAdditional();

        String[] tempArray = new String[additionals.size()];
        for(int i =0;i<additionals.size();i++){
            tempArray[i] = additionals.get(i);
        }

        return tempArray;

    }
}
