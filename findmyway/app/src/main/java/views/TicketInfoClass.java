package views;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import mobi.wimt.findmyway.R;

import java.util.List;

import controllers.Controller;
import controllers.ResultsController;

/**
 * Created by James on 2015-02-20.
 */
public class TicketInfoClass extends Card implements View.OnClickListener {

    private Activity theView;
    private LinearLayout theResultsScrollLayout;
    private ResultsController theResultsController;//stores the controller with all the cards which allows access to other cards
    private LinearLayout rootLayout;
    private ImageView backButton;
    private TextView time;
    private resultsMainCard theCoreCard;
    private View theResultsCard;
    private List<String> fairMessages;
    private RelativeLayout timeContainer;
    private CardView cardViewTicketInfo;

    public TicketInfoClass(Activity theView,LinearLayout theResultsScrollLayout, ResultsController theResultsController,resultsMainCard theCoreCard,View theResultsCard){
        this.theView = theView;
        this.theResultsScrollLayout = theResultsScrollLayout;
        this.theResultsController = theResultsController;
        this.theCoreCard = theCoreCard;
        this.theResultsCard = theResultsCard;
        setupViewElements();
    }

    @Override
    public void setupViewElements(){
        rootLayout = (LinearLayout)theResultsCard.findViewById(R.id.additionalTicketInfo);
        backButton = (ImageView)theResultsCard.findViewById(R.id.backToMainCard);
        time = (TextView)theResultsCard.findViewById(R.id.ticketTime);
        timeContainer = (RelativeLayout) theResultsCard.findViewById(R.id.timeContainer);
        cardViewTicketInfo = (CardView) theResultsCard.findViewById(R.id.card_viewTicketInfoResultsScreen);
        backButton.setOnClickListener(this);
    }

    public void setTimeInterval(){
        time.setText(getTimeInterval());
    }

    public Activity getTheView() {
        return theView;
    }

    public void setTheView(Activity theView) {
        this.theView = theView;
    }

    public LinearLayout getTheResultsScrollLayout() {
        return theResultsScrollLayout;
    }

    public void setTheResultsScrollLayout(LinearLayout theResultsScrollLayout) {
        this.theResultsScrollLayout = theResultsScrollLayout;
    }

    public View getTicketInfoView() {
        return theResultsCard;
    }

    public void setTicketInfoView(View ticketInfoView) {
        this.theResultsCard = ticketInfoView;
    }

    public Controller getTheResultsController() {
        return theResultsController;
    }

    public void setTheResultsController(ResultsController theResultsController) {
        this.theResultsController = theResultsController;
    }

    public LinearLayout getRootLayout() {
        return rootLayout;
    }

    public void setRootLayout(LinearLayout rootLayout) {
        this.rootLayout = rootLayout;
    }

    public ImageView getBackButton() {
        return backButton;
    }

    public void setBackButton(ImageView backButton) {
        this.backButton = backButton;
    }


    @Override
    public void onClick(View v) {
        removeViewsFairs();
        theCoreCard.flipTheCard(this,false);
    }


    public List<String> getFairMessages() {
        return fairMessages;
    }

    public void setFairMessages(List<String> fairMessages) {
        this.fairMessages = fairMessages;
    }

    public void addFairMessages(){
        LayoutInflater theInflater = (LayoutInflater) theView.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for(int i=0;i<fairMessages.size();i++){
            View theFairMessage = theInflater.inflate(R.layout.stages_line_item, null, false);
            ImageView theFairIcon = (ImageView) theFairMessage.findViewById(R.id.stageType);
            TextView theMessage = (TextView) theFairMessage.findViewById(R.id.natLangDescription);
            theFairIcon.setImageResource(R.drawable.ic_action_event);
            theMessage.setText(fairMessages.get(i));
            rootLayout.addView(theFairMessage);
        }

    }

    public boolean checkInfoActuallyExists(){
        int counter = 0;
        for(int i=0;i<fairMessages.size();i++){
            if(fairMessages.get(i).equals("")){
                counter =counter+1;
            }
        }

        if(counter == fairMessages.size()){
            return false;
        }
        else{
            return true;
        }
    }

    public String getTimeInterval(){
        String startTime = getTimeForInterval(theCoreCard.getMainCard().getStartTime());
        String endTime = getTimeForInterval(theCoreCard.getMainCard().getEndTime());

        return startTime+" - "+endTime;


    }

    private String getTimeForInterval(String time) {
        int startIndex = time.indexOf('T');;
        String result = time.substring(startIndex+1,time.length()).replace(':','h');

        return result;
    }

    private void removeViewsFairs(){
        rootLayout.removeAllViews();
    }

    public void setCardSize(){
        CardView.LayoutParams params = (CardView.LayoutParams) cardViewTicketInfo.getLayoutParams();
        timeContainer.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        rootLayout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        int heightContainer = timeContainer.getMeasuredHeight();
        int totalHeight = heightContainer+rootLayout.getMeasuredHeight();
        if(totalHeight>theCoreCard.getMainCard().height){
            //do nothing
        }
        else {
            params.height = theCoreCard.getMainCard().height;
            cardViewTicketInfo.setLayoutParams(params);
        }
    }


}
