package views;

import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;

import controllers.Controller;
import mobi.wimt.findmyway.Operator;

import java.util.ArrayList;

/**
 * Created by James on 2015-01-23.
 */
public class ExpandedOperatorsCard extends Card {

    public Activity theExpandedOperatorsView;
    public LinearLayout theScrollLayout;
    public View AllOperatorsCard;
    public ArrayList<Operator> operatorOptions;
    public Controller theController;//stores the controller with all the cards which allows access to other cards

    public ExpandedOperatorsCard(Activity theExpandedOperatorsView){

    }
}
