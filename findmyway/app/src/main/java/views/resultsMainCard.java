package views;

import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;

import com.google.android.gms.maps.MapsInitializer;
import mobi.wimt.findmyway.R;
import java.util.ArrayList;
import controllers.ResultsController;
import models.PathsResult;

/**
 * Created by James on 2015-02-04.
 */
public class resultsMainCard extends Card {

    public Activity theView;
    public int priority;
    public View resultsCoreCard;
    public LinearLayout theResultsScrollLayout;
    public ResultsController theController;
    public ViewFlipper flipper;
    public Card currentlyVisibleCard;
    public ArrayList<Card> flippableCards;
    public boolean ticketInfoExists;
    public PathsResult result;

    public resultsMainCard(Activity theView,int priority,LinearLayout theResultScrollLayout, ResultsController theController,boolean ticketInfoExists, PathsResult result){
        this.theView = theView;
        this.priority = priority;
        this.theResultsScrollLayout = theResultScrollLayout;
        this.theController = theController;
        this.ticketInfoExists =ticketInfoExists;
        this.result = result;
        flippableCards = new ArrayList<>();
        setupViews();
    }

    private void setupViews() {
        resultsCoreCard = theView.getLayoutInflater().inflate(R.layout.mainscreen_results_card, theResultsScrollLayout, false);
        flipper = (ViewFlipper) resultsCoreCard.findViewById(R.id.theFlipper);
        addFragmentCards();
        MapsInitializer.initialize(theView);
    }

    private void addFragmentCards(){

        MainScreenResultsCard mainCard = new MainScreenResultsCard(theView,theResultsScrollLayout,theController,this,resultsCoreCard);
        currentlyVisibleCard = mainCard;
        flippableCards.add(mainCard);

        if(ticketInfoExists == true){
            TicketInfoClass ticketInfo = new TicketInfoClass(theView,theResultsScrollLayout,theController,this,resultsCoreCard);
            flippableCards.add(ticketInfo);
        }

        EmbeddedMapCard mapCard = new EmbeddedMapCard(theView,theResultsScrollLayout,theController,this,resultsCoreCard);
        flippableCards.add(mapCard);

    }

    @Override
    public void removeCard(){
        theResultsScrollLayout.removeView(resultsCoreCard);
    }


    @Override
    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }


    @Override
    public void addCard(){
        theResultsScrollLayout.addView(resultsCoreCard);
    }

    public void flipTheCard(Card cardCallingFlip,boolean ticketInfoClicked){
        if(cardCallingFlip instanceof MainScreenResultsCard){

            if(ticketInfoClicked == true){
                flipper.setInAnimation(theView, R.anim.grow_from_middle);
                flipper.setOutAnimation(theView, R.anim.shrink_to_middle);
                //go to ticket info card
                flipper.showNext();
            }
            else{
                flipper.setInAnimation(theView, R.anim.grow_from_middle);
                flipper.setOutAnimation(theView, R.anim.shrink_to_middle);
                //go to map card
                flipper.setDisplayedChild(2);
            }

        }
        else if(cardCallingFlip instanceof EmbeddedMapCard){
            flipper.setInAnimation(theView, R.anim.grow_from_middle);
            flipper.setOutAnimation(theView, R.anim.shrink_to_middle);
            //go back to the main card
            flipper.setDisplayedChild(0);
        }
        else{
            flipper.setInAnimation(theView, R.anim.grow_from_middle);
            flipper.setOutAnimation(theView, R.anim.shrink_to_middle);
            //back from ticket info class
            flipper.showPrevious();
        }
    }

    public MainScreenResultsCard getMainCard(){
        MainScreenResultsCard temp = null;
        for(Card card: flippableCards){
            if(card instanceof MainScreenResultsCard){
                temp = (MainScreenResultsCard) card;
            }
        }
        return temp;
    }

    public EmbeddedMapCard getMapCard(){
        EmbeddedMapCard temp = null;
        for(Card card: flippableCards){
            if(card instanceof EmbeddedMapCard){
                temp = (EmbeddedMapCard) card;
            }
        }
        return temp;
    }

    public TicketInfoClass getTicketInfoCard(){
        TicketInfoClass temp = null;
        for(Card card: flippableCards){
            if(card instanceof TicketInfoClass){
                temp = (TicketInfoClass) card;
            }
        }
        return temp;
    }


}
