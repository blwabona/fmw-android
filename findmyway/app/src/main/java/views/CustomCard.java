package views;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import mobi.wimt.findmyway.R;
import controllers.CardsController;

/**
 * Created by James on 2015-02-27.
 */
public class CustomCard extends Card implements View.OnClickListener {

    private Activity theView;
    private int priority;//stores the cards priority in the pile
    private LinearLayout theScrollLayout;
    private View helloPurpleCard;//this cards layout
    public CardsController theController;//stores the controller with all the cards which allows access to other cards
    private ImageButton dismissCard;
    public Button Accept;
    public Button MoreInfo;
    public String team;

    public CustomCard(CardsController cardsController, Activity theView, int priority, LinearLayout theScrollLayout){
        this.theController = cardsController;
        this.theView = theView;
        this.priority = priority;
        this.theScrollLayout = theScrollLayout;
        setupViewElements();
    }

    @Override
    public void setupViewElements(){
        helloPurpleCard = theView.getLayoutInflater().inflate(R.layout.hello_purple_card, theScrollLayout, false);//inflate the correct layouts
        dismissCard = (ImageButton) helloPurpleCard.findViewById(R.id.dismissButtonPurple);
        dismissCard.setOnClickListener(this);
        MoreInfo = (Button)helloPurpleCard.findViewById(R.id.buttonMoreInfo);
        Accept = (Button)helloPurpleCard.findViewById(R.id.buttonAccept);

        MoreInfo.setOnClickListener(this);
        Accept.setOnClickListener(this);
    }

    @Override
    public int getPriority(){
        return priority;
    }

    @Override
    public void addCard(){
        theScrollLayout.addView(helloPurpleCard);
    }

    public void removeCard(){
        theScrollLayout.removeView(helloPurpleCard);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id){
            case R.id.dismissButtonPurple:
                removeCard();
                theController.activeCards.remove(this);
                break;
            case R.id.buttonMoreInfo:
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("http://wwf.panda.org/what_we_do/footprint/cities/earth_hour_city_challenge"));
                theView.startActivity(i);
                break;
        }

    }

    public View getCardView(){
        return helloPurpleCard;
    }

}
