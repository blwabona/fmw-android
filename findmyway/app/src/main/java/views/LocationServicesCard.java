package views;

import android.app.Activity;
import android.content.Intent;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import controllers.Controller;

import mobi.wimt.findmyway.R;

/**
 * Created by James on 2015-01-14.
 */
public class LocationServicesCard extends Card implements View.OnClickListener {

    private Activity theView;
    private int priority;//stores the cards priority in the pile
    private LinearLayout theScrollLayout;
    private View servicesCard;//this cards layout
    private Controller theController;//stores the controller with all the cards which allows access to other cards
    private TextView explanationText;
    private ImageButton dismissCard;
    private Button activateServices;
    public String theCardText;

    public LocationServicesCard(Controller cardsController, Activity theView, int priority, LinearLayout theScrollLayout,String theCardText){
        this.theController = cardsController;
        this.theView = theView;
        this.priority = priority;
        this.theScrollLayout = theScrollLayout;
        this.theCardText = theCardText;
        setupViewElements();
    }

    @Override
    public void setupViewElements() {
        servicesCard = theView.getLayoutInflater().inflate(R.layout.locationervicescard, theScrollLayout, false);//inflate the correct layouts
        dismissCard = (ImageButton) servicesCard.findViewById(R.id.dismissButton);
        explanationText = (TextView) servicesCard.findViewById(R.id.locationsExplanation);
        activateServices = (Button) servicesCard.findViewById(R.id.activateServicesButton);

        explanationText.setText(theCardText);

        dismissCard.setOnClickListener(this);
        activateServices.setOnClickListener(this);
    }

    @Override
    public int getPriority(){
        return priority;
    }

    @Override
    public void addCard(){
        theScrollLayout.addView(servicesCard);
    }

    public void removeCard(){
        theScrollLayout.removeView(servicesCard);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id){
            case R.id.dismissButton:
                removeCard();
                theController.removeServicesCard(this);
                break;
            case R.id.activateServicesButton:
                theView.startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
                break;
        }

    }

    public View getCardView(){
        return servicesCard;
    }
}
