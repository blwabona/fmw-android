package views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import mobi.wimt.findmyway.JourneyModeActivity;
import mobi.wimt.findmyway.R;
import mobi.wimt.findmyway.ResultsActivity;
import mobi.wimt.findmyway.StageItem;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import api.ApiClient;
import controllers.ResultsController;
import models.CoordinateModel;
import models.LoggedTrip;
import models.StageModel;
import models.Trips;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by James on 2015-02-20.
 */
public class MainScreenResultsCard extends Card implements View.OnClickListener {

    public Activity theView;
    public View resultsCard;
    public LinearLayout theResultsScrollLayout;
    public ResultsController theController;
    public TextView headerOneText;
    public TextView headerTwoText;
    public TextView headerThreeText;
    public TextView headerOneSubtext;
    public TextView headerTwoSubtext;
    public TextView headerThreeSubtext;
    public Button showOnMap;
    public Button ticketInfo;
    public Button journeyMode;
    public ImageView expandOverview;
    public TextView departingInTextView;
    public LinearLayout theOverviewExpandLayout;
    public ArrayList<StageItem> stagesToExpand;
    public boolean isExpanded;
    public resultsMainCard coreCard;
    public String startTime;
    public String endTime;
    public int height;
    public int width;
    public CardView mainResultsCard;
    public RelativeLayout iconLayout;
    public HashMap<String,Integer> numberStages;
    public HashMap<String,ImageView> stageIcons;
    public TextView OverViewHeader;
    public TextView cardDeparting;
    public boolean ticketInfoExists = true;


    public MainScreenResultsCard(Activity theView,LinearLayout theResultScrollLayout, ResultsController theController,resultsMainCard coreCard,View resultsCard){
        this.theView = theView;
        this.theResultsScrollLayout = theResultScrollLayout;
        this.theController = theController;
        this.coreCard = coreCard;
        this.resultsCard = resultsCard;
        setupViews();
    }

    private void setupViews() {
        headerOneText = (TextView)resultsCard.findViewById(R.id.Cost);
        headerTwoText = (TextView)resultsCard.findViewById(R.id.TimeArrive);
        headerThreeText = (TextView)resultsCard.findViewById(R.id.WalkingDistance);
        headerOneSubtext = (TextView)resultsCard.findViewById(R.id.headerOneSubtext);
        headerTwoSubtext = (TextView)resultsCard.findViewById(R.id.headerTwoSubtext);
        headerThreeSubtext = (TextView)resultsCard.findViewById(R.id.headerThreeSubtext);

        OverViewHeader = (TextView) resultsCard.findViewById(R.id.OverviewHeader);
        cardDeparting = (TextView) resultsCard.findViewById(R.id.resultsCardMainScreenHeader);
        showOnMap = (Button)resultsCard.findViewById(R.id.showOnMap);
        ticketInfo = (Button)resultsCard.findViewById(R.id.ticketInfo);
        journeyMode = (Button)resultsCard.findViewById(R.id.JourneyMode);

        mainResultsCard = (CardView) resultsCard.findViewById(R.id.card_viewMainResultsScreen);

        iconLayout = (RelativeLayout) resultsCard.findViewById(R.id.rootLayoutIcons);

        ticketInfo.setOnClickListener(this);
        showOnMap.setOnClickListener(this);

        //while journey mode is in development
        journeyMode.setText("");
        journeyMode.setEnabled(false);
        //journeyMode.setOnClickListener(this);

        expandOverview = (ImageView)resultsCard.findViewById(R.id.expandOverview);
        departingInTextView = (TextView)resultsCard.findViewById(R.id.timeTillDepart);

        theOverviewExpandLayout = (LinearLayout)resultsCard.findViewById(R.id.routeOverview);

        expandOverview.setOnClickListener(this);
        OverViewHeader.setOnClickListener(this);
        isExpanded = false;
        stagesToExpand = new ArrayList<>();

    }


    public void addStageIcons(List<StageModel> stages) {

        numberStages = new HashMap<>();
        stageIcons = new HashMap<>();

        int counter = 0;

        for(int i =0;i<stages.size();i++){

            String mode = stages.get(i).getMode();
            String color = stages.get(i).getColour();
            int theIcon = getStageDrawable(mode);
            Drawable mDrawable = theView.getResources().getDrawable(theIcon);
            mDrawable.mutate();
            mDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor(color), PorterDuff.Mode.MULTIPLY));
            ImageView iconToAdd = null;

            if(stageIcons.get(mode) != null){
                theIcon = getOverflowStageDrawable(mode);
                mDrawable = theView.getResources().getDrawable(theIcon);
                iconToAdd = stageIcons.get(mode);
                iconToAdd.setImageDrawable(mDrawable);
                numberStages.put(mode,numberStages.get(mode)+1);
            }
            else{
                if(getStageIcon(counter) != null){
                    iconToAdd = getStageIcon(counter);
                    iconToAdd.setImageDrawable(mDrawable);
                    stageIcons.put(mode,iconToAdd);
                    numberStages.put(mode,1);
                    counter = counter+1;
                }
                else{

                }
            }
        }
    }

    public ImageView getStageIcon(int i){
        ImageView stageIconOne = (ImageView) resultsCard.findViewById(R.id.stageIconOne);
        ImageView stageIconTwo = (ImageView) resultsCard.findViewById(R.id.stageIconTwo);
        ImageView stageIconThree = (ImageView) resultsCard.findViewById(R.id.stageIconThree);
        ImageView stageIconFour = (ImageView) resultsCard.findViewById(R.id.stageIconFour);
        switch(i){
            case 0:
                return stageIconOne;
            case 1:
                return stageIconTwo;
            case 2:
                return stageIconThree;
            case 3:
                return stageIconFour;
            default:
                return null;
        }
    }


    public int getStageDrawable(String stageType){
        switch(stageType){
            case "Pedestrian":
                return R.drawable.stageiconwalk;
            case "Rail":
                return R.drawable.stageicontrain;
            case "Bus":
                return R.drawable.stageiconbus;
            case "Boat":
                return R.drawable.stageiconboat;
            default:
                return R.drawable.stageiconbus;
        }
    }

    public int getOverflowStageDrawable(String stageType){
        switch(stageType){
            case "Taxi":
                return R.drawable.overflowtaxi;
            case "Pedestrian":
                return R.drawable.overflowwalk;
            case "Rail":
                return R.drawable.overflowtrain;
            case "Bus":
                return R.drawable.overflowbus;
            case "Boat":
                return R.drawable.overflowferry;
            default:
                return 0;
        }
    }

    public void disableTicketInfo(){
        ticketInfo.setTextColor(Color.GRAY);
        ticketInfoExists = false;
    }

    public String UTCToLocalTime(String date){
        System.out.println("UTC date before adjust "+date);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date dateToConvert = null;
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            dateToConvert = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sdf.setTimeZone(TimeZone.getDefault());
        System.out.println("date after adjust"+sdf.format(dateToConvert).toString());
        return sdf.format(dateToConvert).toString();
    }


    public void setDataToCard(String startTime,String arriveTime,String estimatedCost,int walkingDistance,List<StageModel> stages){
        System.out.println("Start date back from the api" +startTime);
        this.startTime = UTCToLocalTime(startTime);
        this.endTime = UTCToLocalTime(arriveTime);
        System.out.println("Setting start date back to local: "+startTime);
        System.out.println(stages.get(0).getStageLocations().get(0).getTime());
        String deltaTime = getDeltaTime(UTCToLocalTime(stages.get(0).getStageLocations().get(0).getTime()));
        if(deltaTime.equals("Departed")) {
            departingInTextView.setVisibility(View.GONE);
            cardDeparting.setText(deltaTime);
        }
        else {
            departingInTextView.setText(deltaTime);
        }
        headerOneText.setText(estimatedCost);
        headerTwoText.setText(extractTimeFromDate(UTCToLocalTime(arriveTime)));
        headerThreeText.setText(walkingDistance+"m");
        setStages(stages);
    }

    @Override
    public void removeCard(){
        theResultsScrollLayout.removeView(resultsCard);
    }

    @Override
    public void addCard(){
        theResultsScrollLayout.addView(resultsCard);
    }

    public void setStages(List<StageModel> stages){
        LayoutInflater theInflater = (LayoutInflater) theView.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for(int i =0;i<stages.size();i++){
            View stageItemView= theInflater.inflate(R.layout.stage_overview, null, false);
            LinearLayout rootLayoutToAddItems = (LinearLayout) stageItemView.findViewById(R.id.rootLayoutToAddLines);
            StageItem  stageItem = new StageItem(stageItemView,rootLayoutToAddItems,stages.get(i),theView);
            stagesToExpand.add(stageItem);
            rootLayoutToAddItems.setOnClickListener(this);
        }
    }

    public boolean queryDeparted(String inputtedDate1){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        String currentDate = getCurrentDateAndTime();
        Date testingDate1 = null;
        Date testingDate2 = null;
        try {
            testingDate1 = sdf.parse(currentDate);
            testingDate2 = sdf.parse(inputtedDate1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (testingDate2 != null && testingDate1 != null) {
            if(testingDate2.before(testingDate1)){
                return true;
            }
        } else {
            return false;
        }

        return false;
    }

    private void addStagesToLayout() {
        theOverviewExpandLayout.removeAllViews();
        for(int i=0;i<stagesToExpand.size();i++){
            theOverviewExpandLayout.addView(stagesToExpand.get(i).addLineStages());
        }
    }

    public String getDeltaTime(String startTime){
        if(startTime.contains("Z")){
            startTime = startTime.substring(0,startTime.lastIndexOf(":"));
        }
        System.out.println("format  = "+startTime);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        String current = sdf.format(c.getTime());

        String diff = "";

        if(queryDeparted(startTime)){
            diff = "Departed";
            return diff;
        }
        else{
            Date Date1 = null;
            Date Date2 = null;
            try {
                Date1 = sdf.parse(current);
                Date2 = sdf.parse(startTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            long millse = Date1.getTime() - Date2.getTime();
            long mills = Math.abs(millse);

            int Hours = (int) (mills/(1000 * 60 * 60));
            int Mins = (int) (mills/(1000*60)) % 60;
            long Secs = (int) (mills / 1000) % 60;

            if(Hours == 0){
                diff = " "+Mins + "mins"; // updated value every1 second
            }
            else if(Mins==0){
                diff = " "+Hours + "hours "; // updated value every1 second
            }
            else {
                diff = " " +Hours + "h " + Mins + "mins"; // updated value every1 second
            }

            return diff;
        }
    }

    public String extractTimeFromDate(String date){
        System.out.println("Time "+date);
        int startIndex = date.indexOf("T");
        String tempHourMinute = (date.substring(startIndex+1,date.length()));
        int split = tempHourMinute.indexOf(":");
        String store = tempHourMinute.substring(0,split)+"h"+tempHourMinute.substring(split+1,tempHourMinute.length());
        return store;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id){
            case R.id.OverviewHeader:
            case R.id.expandOverview:
                if(isExpanded == false){
                    expandOverview.setImageResource(R.drawable.collapse);
                    addStagesToLayout();
                    isExpanded = true;
                }
                else{
                    expandOverview.setImageResource(R.drawable.expand);
                    removeStagesFromLayout();
                    isExpanded = false;
                }
                break;

            case R.id.ticketInfo:
                if(ticketInfoExists == false){
                    Toast.makeText(theView, "There is no ticket info for this trip", Toast.LENGTH_SHORT).show();
                }
                else{
                    if(isExpanded == true){
                        removeStagesFromLayout();
                        expandOverview.setImageResource(R.drawable.expand);
                        isExpanded= false;
                    }
                    setMainCardSize();
                    coreCard.flipTheCard(this,true);
                    coreCard.getTicketInfoCard().addFairMessages();
                    coreCard.getTicketInfoCard().setCardSize();
                }
                break;

            case R.id.showOnMap:

                if(isExpanded == true){
                    removeStagesFromLayout();
                    expandOverview.setImageResource(R.drawable.expand);
                    isExpanded= false;
                }

                setMainCardSize();
                coreCard.getMapCard().getMap();
                coreCard.getMapCard().setCardSize();
                coreCard.flipTheCard(this,false);
                break;

            case R.id.JourneyMode:
                System.out.println("Starting journey mode");
                Intent intent;
                intent = new Intent(theView, JourneyModeActivity.class);
                theView.startActivityForResult(intent, 11);
                break;

            case R.id.rootLayoutToAddLines:
                StageItem clickedStage = getCorrectStage(v);
                if(clickedStage.intermediatesExpanded == false) {
                    clickedStage.addIntermediateStages();
                }
                else{
                    clickedStage.collapseIntermediates();
                }
                break;
        }
    }

    public void removeStagesFromLayout() {
        for(int i=0;i<stagesToExpand.size();i++){
            theOverviewExpandLayout.removeView(stagesToExpand.get(i).stageView);
        }
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setMainCardSize(){
        mainResultsCard.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        height = mainResultsCard.getMeasuredHeight();
        width = mainResultsCard.getMeasuredWidth();
    }

    public StageItem getCorrectStage(View v){
        StageItem temp = null;
        for(StageItem stage:stagesToExpand){
            if(stage.stageViewRootLayout.equals(v)){
                temp = stage;
            }
        }
        return temp;
    }

    private void getTripFromTheApi(String theTripId) {
        headerThreeText.setText("Calculating");
        headerThreeSubtext.setText("Carbon Points...");
        ApiClient.mapsToken = true;
        ApiClient.getwimtApiClient(true,theView).getTripRoutes(theTripId,new Callback<Trips>() {
            @Override
            public void success(Trips model, Response response) {
                ApiClient.mapsToken = false;
                consumeTripApiData(model);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                headerThreeText.setText("Failed");
                headerThreeSubtext.setText("Calculating points");
            }
        });
    }


    public void consumeTripApiData(Trips model){
        for(int i=0;i<stagesToExpand.size();i++)
        {

            List<CoordinateModel> thePoints = model.getRoutes().get(i).getPoints();
            String operator = stagesToExpand.get(i).operator;

            double totalDistance = 0;

            for(int j = 0;j<thePoints.size();j++)
            {
                if(j !=0)
                {
                    CoordinateModel point = thePoints.get(j);
                    CoordinateModel previousPoint = thePoints.get(j-1);

                    double pointToPointDistanceDistance = 0;
                    if(!operator.equalsIgnoreCase("Pedestrian"))
                    {
                        pointToPointDistanceDistance = distance(previousPoint.getLatitude(),previousPoint.getLongitude(),point.getLatitude(),point.getLongitude());
                    }

                    totalDistance += 110599* pointToPointDistanceDistance;
                }
            }
        }
    }


    public String getCurrentDateAndTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        String current = sdf.format(c.getTime());
        return current;
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double dist = Math.sqrt((lat1-lat2)*(lat1-lat2) + (lon1-lon2)*(lon1-lon2));
        DecimalFormatSymbols decimalSymbol = new DecimalFormatSymbols(Locale.getDefault());
        decimalSymbol.setDecimalSeparator('.');

        DecimalFormat df2 = new DecimalFormat("#.################");
        df2.setGroupingUsed(false);
        df2.setDecimalFormatSymbols(decimalSymbol);
        dist = Double.valueOf(df2.format(dist));

        return dist;
    }

    public void queryTicketInfo() {
        coreCard.getTicketInfoCard().getFairMessages();
        if(coreCard.getTicketInfoCard().checkInfoActuallyExists() == false){
            ticketInfo.setTextColor(Color.GRAY);
            ticketInfoExists = false;
        }
    }
}


