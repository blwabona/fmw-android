package views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import mobi.wimt.findmyway.FavouriteOrRecent;
import mobi.wimt.findmyway.MapsActivity;
import mobi.wimt.findmyway.R;
import controllers.SearchController;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import adapters.FavouritesAndRecentsRecycler;

/**
 * Created by James on 2015-01-21.
 */
public class SearchCardLocations extends Card implements View.OnClickListener {

    public SearchController theController;
    public Activity searchActivity;
    public int priority;
    public LinearLayout theSearchScrollLayout;
    public View SearchCardLocations;
    public Button FindOnMap;
    public Button CurrentLocation;
    private LocationManager theLocationManager;


    public SearchCardLocations(SearchController searchController, Activity searchActivity, int priority, LinearLayout theSearchScrollLayout) {
        this.searchActivity = searchActivity;
        this.theController = searchController;
        this.priority = priority;
        this.theSearchScrollLayout = theSearchScrollLayout;
        setupViewElements();
        setLocationManager();
    }


    public void setupViewElements() {
        SearchCardLocations= searchActivity.getLayoutInflater().inflate(R.layout.searchlocationscard, theSearchScrollLayout, false);
        FindOnMap = (Button) SearchCardLocations.findViewById(R.id.findOnMap);
        CurrentLocation = (Button) SearchCardLocations.findViewById(R.id.currentLocationButton);
        FindOnMap.setOnClickListener(this);
        FindOnMap.setTextColor(R.color.findmywayPrimaryColor);
        CurrentLocation.setOnClickListener(this);
    }

    public int getPriority(){
        return priority;
    }

    public void addCard(){
        theSearchScrollLayout.addView(SearchCardLocations);
    }

    public void removeCard(){
        theSearchScrollLayout.removeView(SearchCardLocations);
    }

    private Location getCurrentLocation()
    {
        try {
            Location location = null;
            String provider = LocationManager.GPS_PROVIDER;

            if(provider != null) {
                location = theLocationManager.getLastKnownLocation(provider);
            }

            provider = LocationManager.NETWORK_PROVIDER;

            if (location == null && provider != null) {
                location = theLocationManager.getLastKnownLocation(provider);
            }

            provider = LocationManager.PASSIVE_PROVIDER;

            if (location == null && provider != null) {
                location = theLocationManager.getLastKnownLocation(provider);
            }
            return location;
        }
        catch(Exception e){
            return null;
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        int id = v.getId();
        switch(id){
            case R.id.findOnMap:
                if(ensureGoogleMapsInstalled()){
                    intent = new Intent(searchActivity, MapsActivity.class);
                    searchActivity.startActivityForResult(intent,2);
                }
                break;
            case R.id.currentLocationButton:
                Location location = getCurrentLocation();
                if(location == null){
                    Toast.makeText(searchActivity, "Your location is not available", Toast.LENGTH_SHORT).show();
                }
                else {
                    Double[] params = new Double[2];
                    params[0] = location.getLatitude();
                    params[1] = location.getLongitude();
                    theController.geocodeLastLocation(params);
                }
                break;

        }
    }

    public boolean ensureGoogleMapsInstalled(){
        if (!isGoogleMapsInstalled())
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(searchActivity);
            builder.setTitle("Warning");
            builder.setMessage("This part of the app requires Google Maps to be installed.");
            builder.setCancelable(false);
            builder.setPositiveButton("install", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialogInterface, int i)
                {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.apps.maps"));
                    searchActivity.startActivity(intent);
                }
            });
            builder.setNegativeButton("not now", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialogInterface, int i)
                {
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();

            return false;
        }
        else{
            return true;
        }

    }

    public boolean isGoogleMapsInstalled()
    {
        try
        {
            searchActivity.getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e)
        {
            return false;
        }
    }


    public String getCurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd:MMMM:yyyy HH:mm:ss a");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

    public void setLocationManager(){
        theLocationManager = (LocationManager) searchActivity.getApplicationContext().getSystemService(searchActivity.getApplicationContext().LOCATION_SERVICE);
    }

    public void returnGeocodedLocationToMain(String[] address){
        FavouritesAndRecentsRecycler temp = (FavouritesAndRecentsRecycler) searchActivity;
        Location location = theLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(address[0] == null && address[1] == null){
            Double[] params = new Double[2];
            params[0] = location.getLatitude();
            params[1] = location.getLongitude();
            theController.geocodeFromGoogle(params);

        }
        else if(location != null){
            FavouriteOrRecent newFavOrRec = new FavouriteOrRecent(getCurrentDate(), address[0], address[1], false, location.getLatitude(), location.getLongitude(), true);
            theController.dialog.dismiss();
            temp.returnResultToParent(newFavOrRec);
        }
        else{
            theController.dialog.dismiss();
            Toast.makeText(searchActivity, "Your location is not available", Toast.LENGTH_SHORT).show();
        }
    }

    public void returnGeocodedLocationFromGoogleToMain(String address){
        FavouritesAndRecentsRecycler temp = (FavouritesAndRecentsRecycler) searchActivity;
        Location location = theLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        String[] addressList = address.split(",");
        if(address.length()<2 || addressList[0] == null || addressList[1] == null){
            theController.dialog.dismiss();
            Toast.makeText(searchActivity, "Failed to geocode your location", Toast.LENGTH_SHORT).show();
        }
        else if(location != null) {
            theController.dialog.dismiss();
            FavouriteOrRecent newFavOrRec = new FavouriteOrRecent(getCurrentDate(), addressList[0], addressList[1], false, location.getLatitude(), location.getLongitude(),true);
            temp.returnResultToParent(newFavOrRec);
        }
        else
        {
            theController.dialog.dismiss();
            Toast.makeText(searchActivity, "Your location is not available", Toast.LENGTH_SHORT).show();
        }
    }


}
