package views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import mobi.wimt.findmyway.MapsActivity;
import mobi.wimt.findmyway.StageMarker;
import mobi.wimt.findmyway.R;
import mobi.wimt.findmyway.StageCoordinatesForRoutes;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import api.ApiClient;
import controllers.ResultsController;
import models.CoordinateModel;
import models.StageModel;
import models.StagePointModel;
import models.Trips;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by James on 2015-02-20.
 */
public class EmbeddedMapCard extends Card implements View.OnClickListener {

    private Activity theView;
    private LinearLayout theResultsScrollLayout;
    private View mapView;//this cards layout
    private ResultsController theResultsController;//stores the controller with all the cards which allows access to other cards
    private ImageView backButton;
    private resultsMainCard theCoreCard;
    private ImageView theStaticMap;
    private FrameLayout container;
    private TextView time;
    private RelativeLayout rootTripIconLayout;
    private ImageView expandMap;
    public ArrayList<StageCoordinatesForRoutes> stagesRouteCoordinate;
    public ArrayList<CoordinateModel> routePoints;
    public ArrayList<StageMarker> markerLocations;
    public ProgressBar progressMap;
    public String zoomLevel;
    public boolean mapPreviouslyLoaded = false;
    public CardView mapsResultScreen;

    public EmbeddedMapCard(Activity theView, LinearLayout theResultsScrollLayout,ResultsController resultsController,resultsMainCard theCoreCard,View mapView){
        this.theView = theView;
        this.theResultsScrollLayout = theResultsScrollLayout;
        this.theResultsController =resultsController;
        this.theCoreCard =theCoreCard;
        this.mapView = mapView;
        setupViewElements();
    }

    @Override
    public void setupViewElements(){
        theStaticMap  =(ImageView) mapView.findViewById(R.id.staticMapImage);
        container = (FrameLayout) mapView.findViewById(R.id.card_viewMapResultsScreen);
        backButton = (ImageView) mapView.findViewById(R.id.backToMainCardFromMaps);
        rootTripIconLayout = (RelativeLayout) mapView.findViewById(R.id.layoutForTripIcons);
        mapsResultScreen = (CardView)mapView.findViewById(R.id.card_viewMapResultsScreen);
        time = (TextView) mapView.findViewById(R.id.mapsTime);
        progressMap = (ProgressBar) mapView.findViewById(R.id.progressMap);
        expandMap = (ImageView) mapView.findViewById(R.id.expandMapCard);
        theStaticMap.setOnClickListener(this);
        expandMap.setOnClickListener(this);
        backButton.setOnClickListener(this);
    }

    public void setTimeInterval(){
        time.setText(getTimeInterval());
    }

    public void setupStagesIcons(List<StageModel> stages) {
        ImageView previousIcon = null;
        for(int i =0;i<stages.size();i++){
            String mode = stages.get(i).getMode();
            String color = stages.get(i).getColour();
            int theIcon = getStageDrawable(mode);
            ImageView theIconToAdd = new ImageView(theView);
            theIconToAdd.setId(i+1);
            Drawable mDrawable = theView.getResources().getDrawable(theIcon);
            mDrawable.mutate();
            mDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor(color), PorterDuff.Mode.MULTIPLY));
            theIconToAdd.setImageDrawable(mDrawable);

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

            if(previousIcon == null){
                params.addRule(RelativeLayout.ALIGN_PARENT_END,RelativeLayout.TRUE);
            }
            else{
                params.addRule(RelativeLayout.START_OF,previousIcon.getId());
            }

            rootTripIconLayout.addView(theIconToAdd,params);
            previousIcon = theIconToAdd;
        }

        addRouteStops(stages);
    }

    public void addRouteStops(List<StageModel> stages){
        stagesRouteCoordinate = new ArrayList<>();

        for(int i = 0;i<stages.size();i++){

            StagePointModel pointStart = stages.get(i).getStageLocations().get(0);

            int StagePintModelSize = stages.get(i).getStageLocations().size()-1;
            StagePointModel pointEnd = stages.get(i).getStageLocations().get(StagePintModelSize);

            StageCoordinatesForRoutes newItem = new StageCoordinatesForRoutes(pointStart.getPoint().getLatitude(),
                    pointStart.getPoint().getLongitude(),
                    pointEnd.getPoint().getLatitude(),
                    pointEnd.getPoint().getLongitude(),
                    stages.get(i).getColour(),
                    stages.get(i).getMode());

            stagesRouteCoordinate.add(newItem);
        }
    }



    public void getMap(){

        if(mapPreviouslyLoaded == true){
            //Do nothing
        }
        else if(isNetworkAvailable()){
            getRoutePoints();
        }
        else{
            Toast.makeText(theView, "No network available", Toast.LENGTH_SHORT).show();
        }

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager= (ConnectivityManager) theView.getSystemService(theView.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public resultsMainCard getTheCoreCard() {
        return theCoreCard;
    }


    public void setTheResultsController(ResultsController theResultsController) {
        this.theResultsController = theResultsController;
    }

    public Activity getTheView() {
        return theView;
    }

    public void setTheView(Activity theView) {
        this.theView = theView;
    }

    public LinearLayout getTheResultsScrollLayout() {
        return theResultsScrollLayout;
    }

    public void setTheResultsScrollLayout(LinearLayout theResultsScrollLayout) {
        this.theResultsScrollLayout = theResultsScrollLayout;
    }

    public View getMapView() {
        return mapView;
    }

    public void setMapView(View mapView) {
        this.mapView = mapView;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.expandMapCard || id == R.id.staticMapImage){
            if(ensureGoogleMapsInstalled()) {
                Intent intent = new Intent(theView, MapsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("routes", stagesRouteCoordinate);
                intent.putExtra("TripId", theCoreCard.result.getTripId());
                intent.putExtras(bundle);
                theView.startActivityForResult(intent, 2);
            }
        }
        else if(id == R.id.backToMainCardFromMaps){
            theCoreCard.flipTheCard(this,false);
        }
    }

    private class SendTask extends AsyncTask<Bitmap, String, Bitmap> {

        @Override
        protected void onPostExecute(Bitmap bmp){
            progressMap.setVisibility(View.GONE);
            theStaticMap.setImageBitmap(bmp);
            mapPreviouslyLoaded = true;
        }

        @Override
        protected Bitmap doInBackground(Bitmap... params) {
            // TODO Auto-generated method stub
            Bitmap bm = getGoogleMapThumbnail();
            return bm;

        }

    };

    public String getZoomLevel(CoordinateModel boundingboxTopLeft, CoordinateModel boundingBoxBottomRight){
        double width = boundingboxTopLeft.getLongitude() - boundingBoxBottomRight.getLongitude();
        double height = boundingboxTopLeft.getLatitude() - boundingBoxBottomRight.getLatitude();
        String zoom  = "&zoom=";

        if(width <0){
            width = width*-1;
        }

        if(height < 0){
            height = height *-1;
        }

        double zoomAsymptote = 0.1;
        double dependent = 0;

        if(width>height){
            dependent = width;
        }
        else{
            dependent = height;
        }

        double initialZoom = 0;

        if(getScale() == 1){
            initialZoom =10;
        }
        else{
            initialZoom = 12;
        }

        if(dependent<zoomAsymptote){
            while(dependent<zoomAsymptote){
                initialZoom = initialZoom+0.4;
                dependent = dependent+0.05;
            }
        }
        else{
            while(dependent>zoomAsymptote){
                initialZoom = initialZoom-0.6;
                dependent = dependent-0.05;
            }
        }
        return zoom+(int)initialZoom;
    }

    public int getScale(){
        DisplayMetrics dm = new DisplayMetrics();
        theView.getWindowManager().getDefaultDisplay().getMetrics(dm);
        double x = Math.pow(dm.widthPixels/dm.xdpi,2);
        double y = Math.pow(dm.heightPixels/dm.ydpi,2);
        double screenInches = Math.sqrt(x+y);
       if(screenInches>4.0){
           return 2;
       }
       else{
           return 1;
       }
    }

    public Bitmap getGoogleMapThumbnail(){

        int cardHeight = theCoreCard.getMainCard().height;
        int cardWidth = theCoreCard.getMainCard().width;
        String points  = "";



        String BaseURL = "https://maps.googleapis.com/maps/api/staticmap?size="+cardWidth+"x"+cardHeight+"&maptype=roadmap&scale=2&path=color:0x0000ff %7C weight:3 %7C&zoom=11&path=color:0x0000ff%7Cweight:3%7C";
        String markers = "";
        for(int i =0;i<markerLocations.size();i++){
            markers = markers+"&markers=size:mid%7Ccolor:"+markerLocations.get(i).getColor()+"%7Clabel:S%7C"+markerLocations.get(i).getPositon().latitude+","+markerLocations.get(i).getPositon().longitude;
        }
        int reduction = reducePointSize(routePoints,BaseURL+markers);

        for(int i =0;i<routePoints.size();i = i+reduction){
            if(i<routePoints.size()){
                if(i == 0){
                    points = routePoints.get(i).getLatitude()+","+routePoints.get(i).getLongitude();
                }
                else{
                    points =points+"%7C"+routePoints.get(i).getLatitude()+','+routePoints.get(i).getLongitude();
                }
            }
        }

        String pathQuery = "&path=color:0x0000ff%7Cweight:3%7C"+points ;

        String URL = "https://maps.googleapis.com/maps/api/staticmap?size="+cardWidth+"x"+cardHeight+"&maptype=road"+zoomLevel+"&scale=2"+pathQuery+markers;

        Bitmap bmp = null;
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet request = new HttpGet(URL);

        InputStream in = null;
        try {
            in = httpclient.execute(request).getEntity().getContent();
            bmp = BitmapFactory.decodeStream(in);
            in.close();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return bmp;
    }

    public boolean ensureGoogleMapsInstalled(){
        if (!isGoogleMapsInstalled())
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(theView);
            builder.setTitle("Warning");
            builder.setMessage("This part of the app requires Google Maps to be installed.");
            builder.setCancelable(false);
            builder.setPositiveButton("install", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialogInterface, int i)
                {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.apps.maps"));
                    theView.startActivity(intent);
                }
            });
            builder.setNegativeButton("not now", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialogInterface, int i)
                {
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();

            return false;
        }
        else{
            return true;
        }

    }

    public boolean isGoogleMapsInstalled()
    {
        try
        {
            theView.getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e)
        {
            return false;
        }
    }

    private int reducePointSize(ArrayList<CoordinateModel> routePoints,String baseURL) {
        int maxLength = 2048;
        int baseURLLength = baseURL.length();
        int maxTotalPointsLength = maxLength-baseURLLength;

        int totalCharsInRoutePoints = 0;

        for(int i=0;i<routePoints.size();i++){
            String longitude = String.valueOf(routePoints.get(i).getLongitude());
            String latitude = String.valueOf(routePoints.get(i).getLatitude());
            String deliminator = "%7C";
            totalCharsInRoutePoints= totalCharsInRoutePoints+longitude.length()+latitude.length()+deliminator.length();
        }

        int stepCounter = 0;
        while(((double)totalCharsInRoutePoints/stepCounter)>maxTotalPointsLength){
            stepCounter+=1;
        }

        return stepCounter;
    }


    private void getTripFromTheApi(String theTripId) {
        ApiClient.mapsToken = true;
        ApiClient.getwimtApiClient(true,theView).getTripRoutes(theTripId,new Callback<Trips>() {
            @Override
            public void success(Trips model, Response response) {
                ApiClient.mapsToken = false;
                consumeTripApiData(model);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
            }
        });
    }

    public void consumeTripApiData(Trips model){

        for(int i=0;i<model.getRoutes().size();i++){
            setRoutePoints(model.getRoutes().get(i).getPoints());
        }

        addMarkers(stagesRouteCoordinate);
        zoomLevel = getZoomLevel(model.getBoundingBoxTopLeft(),model.getBoundingBoxBottomRight());
        new SendTask().execute();

    }

    private void addMarkers(ArrayList<StageCoordinatesForRoutes> stages) {
        markerLocations = new ArrayList<>();
        for(int i = 0;i<stages.size();i++){
            StageCoordinatesForRoutes stage = stages.get(i);
            LatLng markerStartStage = new LatLng(stage.getStartLat(),stage.getStartLong());
            LatLng markerEndStage = new LatLng(stage.getEndLat(),stage.getEndLong());
            markerLocations.add(new StageMarker(markerStartStage,stage.getRouteColor()));
            markerLocations.add(new StageMarker(markerEndStage,stage.getRouteColor()));
        }
    }


    public String getTimeInterval(){
        String startTime =
                getTime(theCoreCard.getMainCard().getStartTime());
        String endTime = getTime(theCoreCard.getMainCard().getEndTime());

        return startTime+" - "+endTime;
    }

    public void getRoutePoints(){
        routePoints = new ArrayList<>();
        getTripFromTheApi(theCoreCard.result.getTripId());
    }

    public void setRoutePoints(List<CoordinateModel> points){
        routePoints.addAll(points);
    }

    private String getTime(String time) {
        int startIndex = time.indexOf('T');
        String result = time.substring(startIndex+1,time.length()).replace(':','h');

        return result;
    }

    public void setCardSize(){
        CardView.LayoutParams params = (CardView.LayoutParams) mapsResultScreen.getLayoutParams();
        params.height = theCoreCard.getMainCard().height;
        mapsResultScreen.setLayoutParams(params);
    }

    public int getStageDrawable(String stageType){
        switch(stageType){
            case "Taxi":
                return R.drawable.taxi;
            case "Pedestrian":
                return R.drawable.walk;
            case "Rail":
                return R.drawable.train;
            case "Bus":
                return R.drawable.bus;
            case "Boat":
                return R.drawable.ferry;
            default:
                return 0;
        }
    }
}
