package views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import mobi.wimt.findmyway.AnnouncementItem;
import controllers.CardsController;
import mobi.wimt.findmyway.ExpandedAnnouncements;
import mobi.wimt.findmyway.MainActivity;
import mobi.wimt.findmyway.R;
import java.util.ArrayList;
import java.util.List;
import api.ApiClient;
import models.AnnouncementModel;
import models.Announcements;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by James on 2015-01-15.
 */
public class AnnouncementsCard extends Card implements View.OnClickListener {

    public Activity theView;
    public int priority;//stores the cards priority in the pile
    public LinearLayout theScrollLayout;
    public View announcementsCard;
    public ArrayList<AnnouncementItem> announcementsList;
    public ArrayList<AnnouncementModel> announcementsFromApi;
    public CardsController theController;//stores the controller with all the cards which allows access to other cards
    public Button expandAnnouncements;
    public LinearLayout rootLayout;
    public TextView noAnnouncementsLabel;
    public CardView announcementsCardView;
    public ProgressBar loadingSpinner;
    public boolean ApiLoadingSuccess;

    public AnnouncementsCard(CardsController theController,Activity theView,int priority,LinearLayout theScrollLayout){
        this.theController = theController;
        this.theView = theView;
        this.priority = priority;
        this.theScrollLayout = theScrollLayout;
        announcementsList = new ArrayList<AnnouncementItem>();
        ApiLoadingSuccess = false;
        setupViewElements();
        loadAnnouncementsFromApi();
    }

    public void setupViewElements() {
        announcementsCard = theView.getLayoutInflater().inflate(R.layout.announcementscard, theScrollLayout, false);
        rootLayout = (LinearLayout) announcementsCard.findViewById(R.id.Announcements);
        expandAnnouncements = (Button) announcementsCard.findViewById(R.id.expandAnnouncements);
        noAnnouncementsLabel = (TextView) announcementsCard.findViewById(R.id.noAnnouncementsLabel);
        announcementsCardView = (CardView) announcementsCard.findViewById(R.id.card_viewAnnouncements);
        loadingSpinner = (ProgressBar) announcementsCard.findViewById(R.id.progressBarAnnouncements);

        loadingSpinner.setVisibility(View.GONE);
        noAnnouncementsLabel.setVisibility(View.GONE);
        expandAnnouncements.setOnClickListener(this);
        announcementsCardView.setOnClickListener(this);
    }

    public int getPriority(){
        return priority;
    }

    public void addCard(){
        //Not used as announcements need to load before the card is displayed
    }


    public void removeCard(){//method could be improved remember to remove card from controller active cards
        theScrollLayout.removeView(announcementsCard);
    }

    public void addAnnouncementsCard(){
        theScrollLayout.addView(announcementsCard);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id){
            case R.id.expandAnnouncements:
                expandCard();
                break;
            case R.id.card_viewAnnouncements:
                if(announcementsFromApi.size()<=0) {
                    noAnnouncementsLabel.setVisibility(View.GONE);
                    loadingSpinner.setVisibility(View.VISIBLE);
                    loadAnnouncementsFromApi();
                }
                break;
        }
    }

    private void expandCard() {
        MainActivity tempCast = (MainActivity) theView;
        tempCast.saveToggleFilterSettings();
        Intent intent = new Intent(theView, ExpandedAnnouncements.class);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("announcements", announcementsList);
        intent.putExtras(bundle);
        theView.startActivityForResult(intent,5);
    }


    public void loadAnnouncementsFromApi(){
        announcementsFromApi = new ArrayList<AnnouncementModel>();
        ApiClient.getwimtApiClient(false,theView).getAnnouncements(new Callback<Announcements>(){
            @Override
            public void success(Announcements announcementData, Response response) {
                ApiLoadingSuccess = true;
                consumeApiData(announcementData.getAnnouncements());
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                ApiLoadingSuccess = false;
                System.out.println("Failed");
            }
        });

    }

    public void consumeApiData(List<AnnouncementModel> announcements){
        if(announcements.size() == 0 && loadingSpinner.getVisibility() == View.GONE) {
            noAnnouncementsLabel.setVisibility(View.VISIBLE);
            expandAnnouncements.setVisibility(View.GONE);
            loadingSpinner.setVisibility(View.GONE);
            theController.activeCards.add(this);
            theController.shufflePostAnnouncementLoading();
            addAnnouncementsToCard();
        }
        else if(loadingSpinner.getVisibility() == View.VISIBLE &&  announcements.size() != 0){
            loadingSpinner.setVisibility(View.GONE);
            announcementsFromApi.addAll(announcements);
            addAnnouncementsToCard();
        }
        else if(loadingSpinner.getVisibility() == View.VISIBLE){
            noAnnouncementsLabel.setVisibility(View.VISIBLE);
            expandAnnouncements.setVisibility(View.GONE);
            loadingSpinner.setVisibility(View.GONE);
        }
        else {
            announcementsFromApi.addAll(announcements);
            theController.activeCards.add(this);
            theController.shufflePostAnnouncementLoading();
            addAnnouncementsToCard();
        }
    }

    private int getCorrectModeLogo(String operator) {
        if(operator.equals("Taxi")){
            return R.drawable.taxiblack;
        }
        else if(operator.equals("Rail")){
            return R.drawable.trainblack;
        }
        else if(operator.equals("Bus")){
            return R.drawable.busblack;
        }
        else if(operator.equals("Ferry")){
            return R.drawable.ferry;
        }
        else{
            return R.drawable.favouritekarma;
        }
    }

    private void addAnnouncementsToCard() {//pass via an intent?

        LayoutInflater theInflater = (LayoutInflater) theView.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < announcementsFromApi.size(); i++) {
            View theRowView = theInflater.inflate(R.layout.announcement_line, null, false);
            ImageView operatorAnnouncement = (ImageView) theRowView.findViewById(R.id.operatorIcon);
            TextView announcementString = (TextView) theRowView.findViewById(R.id.AnnouncementsLineLabel);
            ImageView shareIcon = (ImageView) theRowView.findViewById(R.id.announceShare);

            String operator = announcementsFromApi.get(i).getOperator();
            String description = announcementsFromApi.get(i).getDescription();
            List<String> modes = announcementsFromApi.get(i).getModes();
            int operatorLogo = getCorrectModeLogo(announcementsFromApi.get(i).getModes().get(0));
            //description = "Update at 4pm:limited services still http://google.com on xxxxx Full Details -> ";
            AnnouncementItem item = new AnnouncementItem(operator,description, announcementString, operatorAnnouncement, shareIcon, operatorLogo, modes,theController);

            if (i > 2) {
                announcementsList.add(item);
            } else {
                announcementsList.add(item);
                rootLayout.addView(theRowView);
            }
        }
    }

    public ArrayList<AnnouncementModel> getAnnouncementsFromApi() {
        return announcementsFromApi;
    }

    public ArrayList<AnnouncementItem> getAnnouncementsList() {
        return announcementsList;
    }

}
