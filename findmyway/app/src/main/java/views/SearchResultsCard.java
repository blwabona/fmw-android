package views;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import mobi.wimt.findmyway.FavouriteOrRecent;
import mobi.wimt.findmyway.NestedDynamicList;
import mobi.wimt.findmyway.R;
import controllers.SearchController;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import adapters.FavouritesAndRecentsRecycler;
import api.ApiClient;
import models.SearchItemModel;
import models.SearchResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by James on 2015-01-21.
 */
public class SearchResultsCard extends Card implements NestedDynamicList, View.OnClickListener {

    public SearchController theSearchController;
    public int priority;
    public Activity searchActivity;
    public LinearLayout theSearchScrollLayout;
    public View SearchResultCard;
    public ArrayList<SearchItemModel> resultsOfQuery;
    public LinearLayout resultsLayout;
    public ArrayList<FavouriteOrRecent> addressFavouritesStatesOfQueries;
    public ProgressBar loadingSpinner;
    public ArrayList<FavouriteOrRecent> favouritesListStatesExpandedFromDataBase;
    public ArrayList<FavouriteOrRecent> refinedFavourites;
    private LocationManager theLocationManager;

    public SearchResultsCard(SearchController searchController, Activity searchActivity, int priority, LinearLayout theSearchScrollLayout, ArrayList<FavouriteOrRecent> favouritesListStatesExpandedFromDataBase){
        this.searchActivity = searchActivity;
        this.theSearchController = searchController;
        this.priority = priority;
        this.theSearchScrollLayout = theSearchScrollLayout;
        resultsOfQuery = new ArrayList<>();
        addressFavouritesStatesOfQueries = new ArrayList<>();
        this.favouritesListStatesExpandedFromDataBase = favouritesListStatesExpandedFromDataBase;
        setupViewElements();
        setLocationManager();
    }

    private Location getCurrentLocation()
    {
        try {
            Location location = null;
            String provider = LocationManager.GPS_PROVIDER;

            if(provider != null) {
                location = theLocationManager.getLastKnownLocation(provider);
            }

            provider = LocationManager.NETWORK_PROVIDER;

            if (location == null && provider != null) {
                location = theLocationManager.getLastKnownLocation(provider);
            }

            provider = LocationManager.PASSIVE_PROVIDER;

            if (location == null && provider != null) {
                location = theLocationManager.getLastKnownLocation(provider);
            }
            return location;
        }
        catch(Exception e){
            return null;
        }
    }

    public void setLocationManager(){
        theLocationManager = (LocationManager) searchActivity.getApplicationContext().getSystemService(searchActivity.getApplicationContext().LOCATION_SERVICE);
    }

    public void getResultsOfQuery(String queryText) {

        loadingSpinner.setVisibility(View.VISIBLE);
        resultsOfQuery = new ArrayList<SearchItemModel>();

        Location location = getCurrentLocation();

        ApiClient.getwimtApiClient(false,searchActivity).getSearchResults(
                queryText,
                (location == null) ? null : ("" + location.getLatitude()),
                (location == null) ? null : ("" + location.getLongitude()),
                new Callback<SearchResults>(){
            @Override
            public void success(SearchResults searchData, Response response) {
                consumeApiData(searchData.getResults());
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                System.out.println(retrofitError);
            }
        });
    }

    public void consumeApiData(List<SearchItemModel> results){
        loadingSpinner.setVisibility(View.GONE);
        resultsOfQuery.addAll(results);
        addItems();
    }

    public void setupViewElements() {
        SearchResultCard= searchActivity.getLayoutInflater().inflate(R.layout.search_result_card, theSearchScrollLayout, false);
        resultsLayout  =(LinearLayout) SearchResultCard.findViewById(R.id.resultsLayout);
        loadingSpinner = (ProgressBar) SearchResultCard.findViewById(R.id.progressBarSearch);
        loadingSpinner.setVisibility(View.GONE);
    }

    public int getPriority(){
        return priority;
    }

    public void addCard(){
        theSearchScrollLayout.addView(SearchResultCard);
    }

    public void removeCard(){
        theSearchScrollLayout.removeView(SearchResultCard);
    }

    public void addItems(){
        LayoutInflater theInflater = (LayoutInflater) searchActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for(int i=0;i<resultsOfQuery.size();i++) {
            View theResultItem = theInflater.inflate(R.layout.search_results_line, null, false);
            ImageView theHeart = (ImageView) theResultItem.findViewById(R.id.favouriteHeart);
            TextView theLabelDescription = (TextView) theResultItem.findViewById(R.id.SearchLineDescription);
            TextView theLabelName = (TextView) theResultItem.findViewById(R.id.SearchLineLabelName);
            FavouriteOrRecent item = new FavouriteOrRecent(getCurrentDate(),
                    resultsOfQuery.get(i).getName(),
                    resultsOfQuery.get(i).getDescription(),
                    theHeart,
                    crossReferenceResultsWithDatabase(resultsOfQuery.get(i)),
                    this,
                    theLabelName,
                    theLabelDescription,
                    theResultItem,
                    resultsOfQuery.get(i).getPoint().getLatitude(),
                    resultsOfQuery.get(i).getPoint().getLongitude());

            addressFavouritesStatesOfQueries.add(item);
            resultsLayout.addView(theResultItem);
            theResultItem.setClickable(true);
            theResultItem.setOnClickListener(this);
        }

    }

    public void clearItems(){
        resultsLayout.removeAllViews();
    }


    @Override
    public FavouriteOrRecent getCorrectItem(View v) {
        FavouriteOrRecent temp = null;
        for (FavouriteOrRecent favourite : addressFavouritesStatesOfQueries) {
            if(favourite.view.itemHeart.equals(v)){
                temp = favourite;
            }
        }
        return temp;
    }

    @Override
    public void onClick(View v) {
        FavouriteOrRecent temp = findRowItem(v);
        FavouritesAndRecentsRecycler activity = (FavouritesAndRecentsRecycler) searchActivity;
        theSearchController.setSelectedFavouriteOrRecent(temp);
        theSearchController.addToFavouritesAndRecentsDatabase(temp);
        activity.returnResultToParent(temp);
    }

    public FavouriteOrRecent findRowItem(View v){
        FavouriteOrRecent temp = null;
        for (FavouriteOrRecent favourite : addressFavouritesStatesOfQueries) {
            if(favourite.view.rowView.equals(v)){
                temp = favourite;
            }
        }
        return temp;
    }

    public String getCurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd:MMMM:yyyy HH:mm:ss a");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }


    public boolean crossReferenceResultsWithDatabase(SearchItemModel theResult){
        refineDatabaseResults();
        boolean checker = false;
        for(int i=0;i<refinedFavourites.size();i++){{
                if((refinedFavourites.get(i).getAddress()).equals(theResult.getDescription()) && (refinedFavourites.get(i).getName()).equals(theResult.getName())){
                    checker = true;
                }
            }
        }
        return checker;

    }

    public void refineDatabaseResults(){
        refinedFavourites = new ArrayList<>(favouritesListStatesExpandedFromDataBase);
        for(int i=0;i<refinedFavourites.size();i++){
            if(refinedFavourites.get(i).isFavourite() == false){
                refinedFavourites.remove(refinedFavourites.get(i));
            }
        }
    }

}
