package sqldatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import android.database.sqlite.SQLiteOpenHelper;

import models.LoggedTrip;


public class SQLiteHelperLoggedTrips extends SQLiteOpenHelper{

    public static final int DATABASE_VERSION= 1;
    public static final String DATABASE_NAME = "LoggedTripsDatabase";


    public SQLiteHelperLoggedTrips(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CreateTable = "CREATE TABLE LoggedTrips ( "+
                "dateCreated TEXT PRIMARY KEY, "+
                "startAddress TEXT, "+
                "destination TEXT, "+
                "carbonPoints TEXT, " +
                "longitude TEXT)";

        db.execSQL(CreateTable);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS LoggedTrips");

        // create fresh books table
        this.onCreate(db);
    }

    //-----------------------------------------------------------------------------//
    //CRUD Operations

    private static final String TABLE_lOGGED_TRIPS = "LoggedTrips";
    private static final String KEY_DATE_CREATED = "dateCreated";
    private static final String KEY_START_ADDRESS= "startAddress";
    private static final String KEY_DESTINATION= "destination";
    private static final String KEY_CARBON_POINTS = "carbonPoints";

    private static final String[] COLUMNS = {KEY_DATE_CREATED,KEY_START_ADDRESS,KEY_DESTINATION,KEY_CARBON_POINTS};

    public void addLoggedTrip(LoggedTrip loggedtrip){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_DATE_CREATED, loggedtrip.creationDate);
        values.put(KEY_START_ADDRESS, loggedtrip.startLocation);
        values.put(KEY_DESTINATION, loggedtrip.endLocation);
        values.put(KEY_CARBON_POINTS, String.valueOf(loggedtrip.carbonPoints));

        db.insert(TABLE_lOGGED_TRIPS, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        db.close();
    }

    public LoggedTrip getLoggedTrip(LoggedTrip loggedTrip) {
        // 1. get reference to readable DB
        SQLiteDatabase db = this.getReadableDatabase();

        //Cursor cursor = db.rawQuery("SELECT date,name,address,isFavourite,latitude,longitude FROM FavouritesAndRecentsTable WHERE latitude = ? AND longitude = ?", new String[] { String.valueOf((favOrRecent.getLatitude())).trim(),String.valueOf(favOrRecent.getLongitude()).trim() });
        //2. build query
        Cursor cursor =
                db.query(TABLE_lOGGED_TRIPS, // a. table
                        COLUMNS, // b. column names
                        "dateCreated =?", // c. selections
                        new String[]{loggedTrip.creationDate}, // d. selections args
                        null, // e. group by
                        null, // f. having
                        null, // g. order by
                        null); // h. limit

        // 3. if we got results get the first one
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            LoggedTrip newLoggedTrip = new LoggedTrip(cursor.getString(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    Integer.valueOf(cursor.getString(3)));
            return newLoggedTrip;
        } else {
            return null;
        }
    }

    public ArrayList<LoggedTrip> getAllLoggedTrips() {
        ArrayList<LoggedTrip> theLoggedTrips = new ArrayList<LoggedTrip>();

        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_lOGGED_TRIPS;

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        LoggedTrip loggedTrip = null;
        if (cursor.moveToFirst()) {
            do {
                loggedTrip = new LoggedTrip(cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        Integer.valueOf(cursor.getString(3)));

                theLoggedTrips.add(loggedTrip);
            } while (cursor.moveToNext());
        }

        return theLoggedTrips;
    }
}
