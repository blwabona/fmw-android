package sqldatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import mobi.wimt.findmyway.FavouriteOrRecent;
import java.util.ArrayList;

/**
 * Created by James on 2015-02-03.
 */
public class SQLiteHelperFavouritesAndRecents extends SQLiteOpenHelper{

    public static final int DATABASE_VERSION= 1;
    public static final String DATABASE_NAME = "FavouritesAndRecentsTableTestDatabase";


    public SQLiteHelperFavouritesAndRecents(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CreateTable = "CREATE TABLE FavouritesAndRecentsTable ( "+
                "date TEXT, "+
                "name TEXT, "+
                "address TEXT, "+
                "isFavourite TEXT, "+
                "latitude TEXT, " +
                "longitude TEXT," +
                "PRIMARY KEY(longitude,latitude))";

        db.execSQL(CreateTable);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS FavouritesAndRecentsTable");

        // create fresh books table
        this.onCreate(db);
    }

    //-----------------------------------------------------------------------------//
    //CRUD Operations

    private static final String TABLE_FAVOURITES = "FavouritesAndRecentsTable";
    private static final String KEY_DATE = "date";
    private static final String KEY_ADDRESS= "address";
    private static final String KEY_NAME = "name";
    private static final String KEY_ISFAVOURITE = "isFavourite";
    private static final String KEY_LATITUDE = "latitude";
    private static final String KEY_LONGITUDE = "longitude";

    private static final String[] COLUMNS = {KEY_DATE,KEY_NAME,KEY_ADDRESS,KEY_ISFAVOURITE,KEY_LATITUDE,KEY_LONGITUDE};

    public void addFavourite(FavouriteOrRecent favouriteOrRecent){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_DATE, favouriteOrRecent.getDateLastModified());
        values.put(KEY_NAME, favouriteOrRecent.getName());
        values.put(KEY_ADDRESS, favouriteOrRecent.getAddress());
        values.put(KEY_ISFAVOURITE, booleanToString(favouriteOrRecent.isFavourite));
        values.put(KEY_LATITUDE, Double.toString(favouriteOrRecent.getLatitude()));
        values.put(KEY_LONGITUDE, Double.toString(favouriteOrRecent.getLongitude()));

        db.insert(TABLE_FAVOURITES, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        db.close();
    }

    public FavouriteOrRecent getFavouriteOrRecent(FavouriteOrRecent favOrRecent){
        // 1. get reference to readable DB
        SQLiteDatabase db = this.getReadableDatabase();

        //Cursor cursor = db.rawQuery("SELECT date,name,address,isFavourite,latitude,longitude FROM FavouritesAndRecentsTable WHERE latitude = ? AND longitude = ?", new String[] { String.valueOf((favOrRecent.getLatitude())).trim(),String.valueOf(favOrRecent.getLongitude()).trim() });
        //2. build query
        Cursor cursor =
                db.query(TABLE_FAVOURITES, // a. table
                        COLUMNS, // b. column names
                        "latitude = ? AND longitude =?", // c. selections
                        new String[] { String.valueOf((favOrRecent.getLatitude())).trim(),String.valueOf(favOrRecent.getLongitude()).trim() }, // d. selections args
                        null, // e. group by
                        null, // f. having
                        null, // g. order by
                        null); // h. limit
        // 3. if we got results get the first one
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            FavouriteOrRecent favouriteorrecent = new FavouriteOrRecent(cursor.getString(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    returnBoolean(cursor.getString(3)),
                    Double.parseDouble(cursor.getString(4)),
                    Double.parseDouble(cursor.getString(5)),
                    false);
            return favouriteorrecent;
        }
        else{
            return null;
        }
    }

    public boolean returnBoolean(String temp){
        if(temp.equals("true")){
            return true;
        }
        else if(temp.equals("false")){
            return false;
        }
        else{
            return true;
        }
    }

    public ArrayList<FavouriteOrRecent> getAllFavouritesAndRecents() {
        ArrayList<FavouriteOrRecent> favouritesorrecent = new ArrayList<FavouriteOrRecent>();

        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_FAVOURITES;

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        FavouriteOrRecent favouriteOrRecentItem = null;
        if (cursor.moveToFirst()) {
            do {
                favouriteOrRecentItem = new FavouriteOrRecent(cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        returnBoolean(cursor.getString(3)),
                        Double.parseDouble(cursor.getString(4)),
                        Double.parseDouble(cursor.getString(5)),false);

                favouritesorrecent.add(favouriteOrRecentItem);
            } while (cursor.moveToNext());
        }

        return favouritesorrecent;
    }

    public int updateFavouriteOrRecent(FavouriteOrRecent favouriteOrRecent) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_DATE, favouriteOrRecent.getDateLastModified());
        values.put(KEY_NAME, favouriteOrRecent.getName());
        values.put(KEY_ADDRESS, favouriteOrRecent.getAddress());
        values.put(KEY_ISFAVOURITE, booleanToString(favouriteOrRecent.isFavourite));
        values.put(KEY_LATITUDE, Double.toString(favouriteOrRecent.getLatitude()));
        values.put(KEY_LONGITUDE, Double.toString(favouriteOrRecent.getLongitude()));
        // 3. updating row
        int i = db.update(TABLE_FAVOURITES, //table
                values, // column/value
                "latitude = ? AND longitude = ?", // selections
                new String[] { String.valueOf(favouriteOrRecent.getLatitude()),String.valueOf(favouriteOrRecent.getLongitude()) }); //selection args

        // 4. close
        db.close();

        return i;

    }

    public void refreshDatabase(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_FAVOURITES);
    }

    public String booleanToString(boolean state){
        if(state == true){
            return "true";
        }
        else{
            return "false";
        }
    }



}
