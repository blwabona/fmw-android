package controllers;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Toast;

import mobi.wimt.findmyway.FavouriteOrRecent;
import mobi.wimt.findmyway.R;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import api.CustomReverseGeocoder;
import views.AnnouncementsCard;
import views.Card;
import views.CountDownCard;
import views.CustomCard;
import views.FavouritesCard;
import views.LocationDestinationCard;
import views.LocationServicesCard;

/**
 * Created by James on 2015-01-12.
 * The card controller is responsible for keeping track of all the
 * active cards and removing and adding cards when neccessary
 */
public class CardsController implements LocationListener, Controller {//maybe move listener to the locations service card?

    public List<Card> activeCards;
    public Activity theView;
    private LinearLayout theScrollLayout; //the layout that allows for scrolling
    private LocationManager theLocationManager;
    public HashMap<String,String> settingsStates;
    public static final String PREFS_NAME = "AppSettings";
    public FavouriteOrRecent geocodedLocation;
    public String currentDateAndTime;

    public CardsController(Activity theView){
        this.theView = theView;
        activeCards = new ArrayList<Card>();
        theScrollLayout = (LinearLayout)theView.findViewById(R.id.theScrollLayout);
        currentDateAndTime = getCurrentDateAndTimeForCompetition();
        setLocationManager();
        setUpCards();
        addCards();
    }


    @Override
    public void setUpCards(){//change priorities
        activeCards.add(new LocationDestinationCard(this,theView,1,theScrollLayout));//add a new card to the active list
        activeCards.add(new FavouritesCard(this,theView,2,theScrollLayout,false));
        AnnouncementsCard newCard = new AnnouncementsCard(this,theView,3,theScrollLayout);
    }

    @Override
    public void addCards(){
        for(int i=0;i<activeCards.size();i++) {
            activeCards.get(i).addCard();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
            for(int i = 0;i<activeCards.size();i++){
                if(activeCards.get(i) instanceof LocationServicesCard){
                    theScrollLayout.removeView(((LocationServicesCard) activeCards.get(i)).getCardView());
                    activeCards.remove(i);
                }
            }
    }

    @Override
    public void onProviderDisabled(String provider) {
        String message = String.valueOf(theView.getResources().getText(R.string.locations));
        LocationServicesCard newCard = new LocationServicesCard(this, theView, 0, theScrollLayout,message);
        activeCards.add(newCard);
        shuffleCards();
    }

    @Override
    public void shuffleCards(){

        theScrollLayout.removeAllViews();

        Collections.sort(activeCards, new Comparator<Card>() {
            @Override
            public int compare(Card card1, Card card2) {
                return card1.getPriority()-card2.getPriority();
            }
        });

        for(Card card: activeCards){
            if(card instanceof AnnouncementsCard){
                AnnouncementsCard announcementsCard = (AnnouncementsCard) card;
                if(announcementsCard.ApiLoadingSuccess == true){
                    announcementsCard.addAnnouncementsCard();
                }
            }
            else {
                card.addCard();
            }
        }
    }

    public void setLocationManager(){
        theLocationManager = (LocationManager) theView.getApplicationContext().getSystemService(theView.getApplicationContext().LOCATION_SERVICE);
        theLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }


    @Override
    public void changeResultBox(FavouriteOrRecent tempRecentOrFavourite,int requestCode){
        for(Card card:activeCards){
            if(card instanceof LocationDestinationCard){
                LocationDestinationCard tempLocation = (LocationDestinationCard) card;
                if(requestCode == 1) {
                    tempLocation.setStartLocationFavouriteOrRecent(tempRecentOrFavourite);
                    if(tempRecentOrFavourite.isUserCurrentLocation == false){
                        tempLocation.iconStartAddress.setImageResource(R.drawable.markerblack);
                    }
                    else{
                        tempLocation.iconStartAddress.setImageResource(R.drawable.ic_action_location_found_black);
                    }
                    tempLocation.changeCurrentLocationBox(tempRecentOrFavourite.name);
                }
                else if(requestCode ==2){
                    tempLocation.setDestinationFavouriteOrRecent(tempRecentOrFavourite);
                    if(tempRecentOrFavourite.isUserCurrentLocation == false){
                        tempLocation.iconendAddress.setImageResource(R.drawable.markerblack);
                    }
                    else{
                        tempLocation.iconendAddress.setImageResource(R.drawable.ic_action_location_found_black);
                    }
                    tempLocation.changeDestinationText(tempRecentOrFavourite.name);
                }
            }
        }
    }

    public void refreshFavouritesCard(){
        for(Card card:activeCards){
            if(card instanceof FavouritesCard) {
                FavouritesCard tempFavourites = (FavouritesCard) card;
                tempFavourites.refreshFavourites();
            }
        }
    }


    public AnnouncementsCard getAnnouncementCard(){
        AnnouncementsCard announceCard = null;
        for(Card card:activeCards) {
            if (card instanceof AnnouncementsCard) {
                announceCard= (AnnouncementsCard) card;
            }
        }
        return announceCard;
    }

    public LocationDestinationCard getLocationsCard(){
        LocationDestinationCard locationsCard = null;
        for(Card card:activeCards) {
            if (card instanceof LocationDestinationCard) {
                locationsCard= (LocationDestinationCard) card;
            }
        }
        return locationsCard;
    }

    public String getCurrentDateAndTimeForCompetition(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyy-MM-dd'T'HH:mm:ss.SSSZ");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

    @Override
    public void removeServicesCard(LocationServicesCard theCard){
        activeCards.remove(theCard);
    }

    public void shufflePostAnnouncementLoading(){

        ArrayList<Card> preShuffle = new ArrayList<>();
        ArrayList<Card> postShuffle = new ArrayList<>();

        preShuffle.addAll(activeCards);

        Collections.sort(activeCards, new Comparator<Card>() {
            @Override
            public int compare(Card card1, Card card2) {
                return card1.getPriority()-card2.getPriority();
            }
        });

        postShuffle.addAll(activeCards);

        int counter = 0;
        int store = 0;
        boolean indexFound  = false;



        for(Card card:postShuffle){
            if((card.getClass()).equals(preShuffle.get(counter).getClass())){
            }
            else{
                if(indexFound == false){
                    store = counter;
                    indexFound = true;
                }
                card.removeCard();
            }
            counter++;
        }

        if(indexFound == true){
            for(int i = store;i<postShuffle.size();i++ ){
                Card card = postShuffle.get(i);
                if(card instanceof AnnouncementsCard){
                    AnnouncementsCard announcementsCard = (AnnouncementsCard) card;
                    if(announcementsCard.ApiLoadingSuccess == true){
                        announcementsCard.addAnnouncementsCard();
                    }
                }
                else {
                    card.addCard();
                }
            }
        }
        else{
            AnnouncementsCard announcementsCard = (AnnouncementsCard) postShuffle.get(postShuffle.size()-1);
            announcementsCard.addAnnouncementsCard();
        }
    }

    public boolean QueryAutoPopulate(){
        Location location = theLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        retrieveAppSettings();
        saveAppSettings();
        if(settingsStates.get("AutoPopulate").equals("Enabled")){
            if(location == null){
                Toast.makeText(theView, "Your location is not available", Toast.LENGTH_SHORT).show();
                return false;
            }
            else{
                return true;
            }
        }
        else{
            return false;
        }
    }

    public void saveAppSettings(){
        SharedPreferences.Editor editor = theView.getSharedPreferences(PREFS_NAME, theView.MODE_PRIVATE).edit();
        for (Map.Entry<String, String> entry : settingsStates.entrySet()) {
            editor.putString(entry.getKey(), entry.getValue());
        }
        editor.commit();
    }


    public void retrieveAppSettings(){
        settingsStates = new HashMap<>();
        SharedPreferences prefs = theView.getSharedPreferences(PREFS_NAME, theView.MODE_PRIVATE);
        Map<String,?> keys = prefs.getAll();
        if(keys.size() ==0){
            settingsStates.put("AutoPopulate","Enabled");
        }
        else {
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                settingsStates.put(entry.getKey().toString(),entry.getValue().toString());
            }
        }
    }

    public void geocodeLastLocation(){
        Location location = theLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Double[] params = new Double[2];
        params[0] = location.getLatitude();
        params[1] = location.getLongitude();
        new SendTaskToGeocode().execute(params);
    }

    public void setFavouriteFromGoogleToBeConfirmed(String address){
        Location location = theLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        String[] addressList = address.split(",");
        if(address.length()<2 || addressList[0] == null || addressList[1] == null){
            Toast.makeText(theView, "Failed to geocode your location", Toast.LENGTH_SHORT).show();
            getLocationsCard().setGeoCodedFavourite(null);
            getLocationsCard().loadingGeoLocation = false;
        }
        else {
            getLocationsCard().loadingGeoLocation = false;
            FavouriteOrRecent newFavOrRec = new FavouriteOrRecent(getCurrentDate(), addressList[0], addressList[1], false, location.getLatitude(), location.getLongitude(),true);
            getLocationsCard().setGeoCodedFavourite(newFavOrRec);
        }

    }


    public void setLocation(String[] address){
        Location location = theLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(address[0] == null && address[1] == null){
            Double[] params = new Double[2];
            params[0] = location.getLatitude();
            params[1] = location.getLongitude();

            new GeocodeAddressFromGoogle().execute(params);
        }
        else{
            getLocationsCard().loadingGeoLocation = false;
            FavouriteOrRecent newFavOrRec = new FavouriteOrRecent(getCurrentDate(), address[0], address[1], false, location.getLatitude(), location.getLongitude(),true);
            getLocationsCard().setGeoCodedFavourite(newFavOrRec);
        }
    }

    public String getCurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd:MMMM:yyyy HH:mm:ss a");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

    private class SendTaskToGeocode extends AsyncTask<Double,String,String[]> {

        @Override
        protected void onPostExecute(String[] address){
            setLocation(address);
        }

        @Override
        protected void onPreExecute() {
            getLocationsCard().loadingGeoLocation = true;
            getLocationsCard().currentLocationBox.setText("Loading your current location...");
            getLocationsCard().currentLocationBox.setTextColor(Color.BLACK);
            getLocationsCard().iconStartAddress.setImageResource(R.drawable.ic_action_location_found_black);
        }


        @Override
        protected String[] doInBackground(Double[] params) {
            String[] strAddress = new String[2] ;
            Geocoder theGeocoder= new Geocoder(theView, Locale.ENGLISH);

            try {
                List<Address> addressesList = theGeocoder.getFromLocation(params[0],params[1],1);

                if(addressesList != null && addressesList.size()>1) {

                    Address fetchedAddressFromGeo = addressesList.get(0);
                    for(int i=0;i<fetchedAddressFromGeo.getMaxAddressLineIndex();i++){
                        if(fetchedAddressFromGeo.getAddressLine(i) != null || fetchedAddressFromGeo.getAddressLine(i) != ""){
                            if(strAddress[0] == null){
                                strAddress[0] = fetchedAddressFromGeo.getAddressLine(i);
                            }
                            else{
                                strAddress[1] = fetchedAddressFromGeo.getAddressLine(i);
                                break;
                            }
                        }
                    }
                }
                else{
                }
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return strAddress;
        }
    };

    private class GeocodeAddressFromGoogle extends AsyncTask<Double,String,String> {

        @Override
        protected void onPostExecute(String address){
            setFavouriteFromGoogleToBeConfirmed(address);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... values) {
        }

        @Override
        protected String doInBackground(Double[] params) {
            CustomReverseGeocoder geocoder = new CustomReverseGeocoder();
            String address = geocoder.reverseGeocode(params[0],params[1]);
            return address;
        }
    };
}
