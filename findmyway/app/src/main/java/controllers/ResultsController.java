package controllers;

import android.app.Activity;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import mobi.wimt.findmyway.FavouriteOrRecent;
import mobi.wimt.findmyway.MainActivity;
import mobi.wimt.findmyway.PathQueryHolder;
import mobi.wimt.findmyway.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import api.ApiClient;
import models.LoggedTrip;
import models.Paths;
import models.PathsResult;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import sqldatabase.SQLiteHelperLoggedTrips;
import views.Card;
import views.LocationServicesCard;
import views.resultsMainCard;

/**
 * Created by James on 2015-02-04.
 */
public class ResultsController implements Controller,LocationListener,View.OnClickListener{

    public ArrayList<Card> activeCards;
    public PathQueryHolder theQuery;
    public Activity theView;
    public ArrayList<PathsResult> pathsFromApi;
    public LinearLayout theResultsScrollLayout;
    private LocationManager theLocationManager;
    public ProgressBar loadingSpinner;
    public TextView status;
    public static final String PREFS_NAME_ADDITIONAL = "ToggleSettingsAdditionalOperators";
    private static final String PREFS_NAME_MENU = MainActivity.PREFS_NAME;
    public boolean loaded = false;

    public ResultsController(Activity theView){
        this.theView = theView;
        activeCards = new ArrayList<Card>();
        theResultsScrollLayout = (LinearLayout)theView.findViewById(R.id.theScrollLayoutResults);
        loadingSpinner = (ProgressBar) theView.findViewById(R.id.loadingResults);
        status = (TextView) theView.findViewById(R.id.noResults);
        status.setText("Loading Journeys");
        loadingSpinner.setVisibility(View.GONE);
        setLocationManager();
    }

    public void refreshTrips(){
        status.setText("Loading Journeys");
        Card locationServices = null;
        for(Card card:activeCards){
            if(card instanceof LocationServicesCard){
                locationServices = card;
            }
            card.removeCard();
        }
        activeCards.clear();
        if(locationServices!= null){
            activeCards.add(locationServices);
        }
    }

    @Override
    public void setUpCards() {
        addSelectableJourneys();
        addCards();
        shuffleCards();
    }

    @Override
    public void addCards() {
        theResultsScrollLayout.removeAllViews();
        for(int i=0;i<activeCards.size();i++) {
            activeCards.get(i).addCard();
        }
    }

    @Override
    public void shuffleCards(){
        theResultsScrollLayout.removeAllViews();
        Collections.sort(activeCards, new Comparator<Card>() {
            @Override
            public int compare(Card card1, Card card2) {
                return card1.getPriority() - card2.getPriority();
            }
        });
        for(Card card: activeCards){
            card.addCard();
        }
    }

    @Override
    public void changeResultBox(FavouriteOrRecent tempFavouriteOrRecent, int BoxCode) {

    }

    @Override
    public void removeServicesCard(LocationServicesCard theCard) {
        activeCards.remove(theCard);
    }

    public void setTheQuery(PathQueryHolder theQuery){
        this.theQuery = theQuery;
        getPathDataFromApi();
    }

    public void testerMethod(){
        theResultsScrollLayout.removeAllViews();
        theResultsScrollLayout.addView(loadingSpinner);
        theResultsScrollLayout.addView(status);
        status.setText("Loading Journeys");
        loadingSpinner.setVisibility(View.VISIBLE);
        status.setVisibility(View.VISIBLE);
    }

    public void getPathDataFromApi(){
        testerMethod();
        pathsFromApi = new ArrayList<PathsResult>();
        ApiClient.mapsToken = true;
        loaded = false;
        System.out.println("Sending in UTC time of "+theQuery.getStartDate());
        ApiClient.getwimtApiClient(true,theView).getPathResults(theQuery.getStartLatitude(),
                theQuery.getStartLongitude(),
                theQuery.getEndLatitude(),
                theQuery.getEndLongitude(),
                convertToApiFormat(localToGMT(theQuery.getStartDate())),
                convertToApiFormat(localToGMT(theQuery.getEndDate())),
                convertToArray(theQuery.getExcludedModes()),convertToArray(theQuery.getExcludedOperators()), new Callback<Paths>() {
                    @Override
                    public void success(Paths pathData, Response response) {
                        System.out.println(response.getUrl());
                        ApiClient.mapsToken = false;
                        loaded = true;
                        consumeApiData(pathData.getResults());
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        loaded = true;
                        System.out.println(retrofitError.getUrl());
                        System.out.println(retrofitError);
                        failedToLoad();
                    }
                });

    }

    public void failedToLoad(){
        status.setVisibility(View.VISIBLE);
        loadingSpinner.setVisibility(View.GONE);
        status.setText("Network Error.Tap To Refresh");
        status.setOnClickListener(this);
    }

    public void consumeApiData(List<PathsResult> pathResults){
        loadingSpinner.setVisibility(View.GONE);
        pathsFromApi.addAll(pathResults);
        if(pathResults.size() ==0){
            status.setText("No Journeys");
        }
        else{
            status.setVisibility(View.GONE);
            setUpCards();
        }
    }

    public String[] convertToArray(String modes){
        return modes.split(",");
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    public void setLocationManager(){
        theLocationManager = (LocationManager) theView.getApplicationContext().getSystemService(theView.getApplicationContext().LOCATION_SERVICE);
        theLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    public void addSelectableJourneys(){
        int numberJourneys = 1;
        for(int i=0;i<pathsFromApi.size();i++){
            PathsResult result = pathsFromApi.get(i);
            boolean ticketInfoExists=false;
            if(result.getFareMessages().size() != 0){
                ticketInfoExists = true;
            }
            resultsMainCard newCard = new resultsMainCard(theView,numberJourneys,theResultsScrollLayout,this,ticketInfoExists,result);

            newCard.getMainCard().setDataToCard(result.getStartTime(),
                    result.getEndTime(),
                    result.getEstimatedTotalCost(),
                    result.getInitialWalkingDistance(),
                    result.getStages());

            newCard.getMainCard().addStageIcons(result.getStages());

            if(ticketInfoExists == true) {
                newCard.getTicketInfoCard().setTimeInterval();
                newCard.getTicketInfoCard().setFairMessages(result.getFareMessages());
                //checks that there are in fact none blank info results
                newCard.getMainCard().queryTicketInfo();
            }
            else{
                newCard.getMainCard().disableTicketInfo();
            }
            newCard.getMapCard().setTimeInterval();

            newCard.getMapCard().setupStagesIcons(result.getStages());

            numberJourneys++;

            activeCards.add(newCard);
        }
    }

    @Override
    public void onProviderEnabled(String provider) {
        for(int i = 0;i<activeCards.size();i++){
            if(activeCards.get(i) instanceof LocationServicesCard){
                theResultsScrollLayout.removeView(((LocationServicesCard) activeCards.get(i)).getCardView());
                activeCards.remove(i);
            }
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        String message = String.valueOf(theView.getResources().getText(R.string.resultsLocationNag));
        LocationServicesCard newCard = new LocationServicesCard(this, theView, 0, theResultsScrollLayout,message);
        activeCards.add(newCard);
        if(loaded == true){
            shuffleCards();
        }
    }

    public String[] getExcludedModes(){
        ArrayList<String> menu = retrieveToggleFilterSettingsMenu();

        String[] tempArray = new String[menu.size()];
        for(int i =0;i<menu.size();i++){
            tempArray[i] = menu.get(i);
        }

        return tempArray;

    }

    public ArrayList<String> retrieveToggleFilterSettingsMenu(){
        ArrayList<String> excludedOperators = new ArrayList<>();
        SharedPreferences prefs = theView.getSharedPreferences(PREFS_NAME_MENU, theView.MODE_PRIVATE);
        Map<String,?> keys = prefs.getAll();
        if(keys == null){
        }
        else {
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                if(entry.getValue().toString().equals("Deactivated")){
                    excludedOperators.add(entry.getKey());
                }
            }
        }
        return excludedOperators;
    }

    public ArrayList<String> retrieveToggleFilterSettingsAdditional(){
        ArrayList<String> excludedOperators = new ArrayList<>();
        SharedPreferences prefs = theView.getSharedPreferences(PREFS_NAME_ADDITIONAL, theView.MODE_PRIVATE);
        Map<String,?> keys = prefs.getAll();
        if(keys == null || keys.size()==0){
        }
        else {
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                if(entry.getValue().toString().equals("Deactivated")){
                    excludedOperators.add(entry.getKey());
                }
            }
        }
        return excludedOperators;
    }

    public String findTheLatestArrivalTime(){
        String latestTime = "";
        for(Card card:activeCards){
            if(card instanceof resultsMainCard){
                resultsMainCard tempCard = (resultsMainCard) card;
                if(latestTime.equals("")){
                    latestTime = tempCard.getMainCard().getEndTime();
                }
                else{
                    if(!queryTimeDifference(tempCard.getMainCard().getEndTime(),latestTime)){
                        latestTime = tempCard.getMainCard().getEndTime();
                    }
                }
            }
        }
        return convertTime(latestTime);
    }


    public boolean queryTimeDifference(String time1,String time2){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date testingTime1 = null;
        Date testingTime2 = null;
        try {
            testingTime1 = sdf.parse(time1);
            testingTime2 = sdf.parse(time2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (testingTime1 != null && testingTime2 != null) {
            if(testingTime1.after(testingTime2)){
                return true;
            }
        } else {
            return false;
        }

        return false;
    }

    public String convertTime(String date){
        int startIndex = date.indexOf("T");
        int endIndex = date.indexOf(":");
        endIndex = date.indexOf(":",endIndex+1);
        return "Arrive by "+date.substring(startIndex+1,endIndex).replace(":","h");
    }

    public String convertToApiFormat(String Date){
        System.out.println("Changing fromat from "+Date);
        if(Date == null){
            return null;
        }
        else{
            System.out.println("To "+Date+":00");
            return Date+":00";
        }
    }

    @Override
    public void onClick(View v) {
        int id= v.getId();
        switch(id){
            case R.id.noResults:
                getPathDataFromApi();
                break;
        }
    }

    public void logTheTrip(LoggedTrip loggedTrip){
        SQLiteHelperLoggedTrips theHelper = MainActivity.theLoggedTripsDatabase;
        if(checkIfLoggedTripExists(loggedTrip) == false){
            theHelper.addLoggedTrip(loggedTrip);
        }
    }

    public boolean checkIfLoggedTripExists(LoggedTrip loggedTrip){
        SQLiteHelperLoggedTrips theHelper = MainActivity.theLoggedTripsDatabase;
        if(theHelper.getLoggedTrip(loggedTrip) !=null){
            return true;
        }
        else{
            return false;
        }
    }

    public String localToGMT(String date){
        System.out.println("Format : "+date);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date dateToConvert = null;
        if(date == null){
            return null;
        }
        try {
            dateToConvert = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf.format(dateToConvert).toString();
    }
}
