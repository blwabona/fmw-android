package controllers;

import mobi.wimt.findmyway.FavouriteOrRecent;

import views.LocationServicesCard;

/**
 * Created by James on 2015-01-21.
 */
public interface Controller {

    public void setUpCards();
    public void addCards();
    public void shuffleCards();
    public void changeResultBox(FavouriteOrRecent tempFavouriteOrRecent,int BoxCode);
    public void removeServicesCard(LocationServicesCard theCard);

}
