package controllers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.widget.LinearLayout;
import android.widget.SearchView;

import mobi.wimt.findmyway.FavouriteOrRecent;
import mobi.wimt.findmyway.MainActivity;
import mobi.wimt.findmyway.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import api.CustomReverseGeocoder;
import sqldatabase.SQLiteHelperFavouritesAndRecents;
import views.Card;
import views.FavouritesCard;
import views.LocationServicesCard;
import views.SearchCardLocations;
import views.SearchResultsCard;

/**
 * Created by James on 2015-01-21.
 */
public class SearchController implements Controller {

    public ArrayList<Card> activeCards;
    public LinearLayout theSearchScrollLayout;
    public Activity searchActivity;
    public SearchResultsCard resultsCard;
    public FavouriteOrRecent selectedFavouriteOrRecent;
    public ArrayList<FavouriteOrRecent> favouritesListStatesExpandedFromDataBase;
    public SearchView theSearchBox;
    public ProgressDialog dialog;

    public SearchController(Activity searchActivity,SearchView theSearchBox){
        this.searchActivity = searchActivity;
        activeCards = new ArrayList<>();
        this.theSearchBox = theSearchBox;
        theSearchScrollLayout = (LinearLayout)searchActivity.findViewById(R.id.theSearchScrollLayout);
        getFavouritesFromDatabase();
        setUpCards();
        addCards();
    }

    @Override
    public void setUpCards() {
        activeCards.add(new SearchCardLocations(this, searchActivity, 1, theSearchScrollLayout));
        activeCards.add(new FavouritesCard(this,searchActivity,2,theSearchScrollLayout,true));
    }

    @Override
    public void addCards() {
        for(int i=0;i<activeCards.size();i++) {
            activeCards.get(i).addCard();
        }
    }

    public void switchToResults(String queryText){
        for(Card theCard: activeCards){
            theCard.removeCard();
        }
        activeCards.clear();
        if(resultsCard == null) {
            SearchResultsCard resultsCard = new SearchResultsCard(this, searchActivity, 1, theSearchScrollLayout,favouritesListStatesExpandedFromDataBase);
            activeCards.add(resultsCard);
            addCards();
            resultsCard.getResultsOfQuery(queryText);
        }
        else{
            resultsCard.clearItems();
            resultsCard.getResultsOfQuery(queryText);
        }

    }

    public void switchBackToMainResults(){
        for(Card card: activeCards){
            if(card instanceof SearchResultsCard){
                theSearchScrollLayout.removeView(((SearchResultsCard)card).SearchResultCard);
            }
        }
        theSearchScrollLayout.removeAllViews();
        activeCards.clear();
        setUpCards();
        addCards();
    }

    @Override
    public void shuffleCards() {
        theSearchScrollLayout.removeAllViews();
        Collections.sort(activeCards, new Comparator<Card>() {
            @Override
            public int compare(Card card1, Card card2) {
                return card1.getPriority() - card2.getPriority();
            }
        });
        for(Card card: activeCards){
            card.addCard();
        }
    }

    @Override
    public void changeResultBox(FavouriteOrRecent temp,int resultCode) {
    }

    @Override
    public void removeServicesCard(LocationServicesCard theCard) {

    }

    public void setSelectedFavouriteOrRecent(FavouriteOrRecent selected){
        selectedFavouriteOrRecent = selected;
    }

    public FavouriteOrRecent getSelectedFavouriteOrRecent(){
        return selectedFavouriteOrRecent;
    }

    public void getFavouritesFromDatabase(){
        favouritesListStatesExpandedFromDataBase = new ArrayList<FavouriteOrRecent>();
        favouritesListStatesExpandedFromDataBase.addAll(MainActivity.theFavouritesAndRecentsDatabase.getAllFavouritesAndRecents());
    }

    private class GeocodeAddress extends AsyncTask<Double,String,String[]> {

        @Override
        protected void onPostExecute(String[] address){
            getSearchCardLocations().returnGeocodedLocationToMain(address);
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(searchActivity);
            dialog.setTitle("Loading your current location");
            dialog.show();
        }

        @Override
        protected String[] doInBackground(Double[] params) {
            String[] strAddress = new String[2] ;
            Geocoder theGeocoder= new Geocoder(searchActivity, Locale.ENGLISH);

            try {
                List<Address> addressesList = theGeocoder.getFromLocation(params[0],params[1],1);

                if(addressesList != null && addressesList.size()>0) {

                    Address fetchedAddressFromGeo = addressesList.get(0);
                    for(int i=0;i<fetchedAddressFromGeo.getMaxAddressLineIndex();i++){
                        if(fetchedAddressFromGeo.getAddressLine(i) != null || fetchedAddressFromGeo.getAddressLine(i) != ""){
                            if(strAddress[0] == null){
                                strAddress[0] = fetchedAddressFromGeo.getAddressLine(i);
                            }
                            else{
                                strAddress[1] = fetchedAddressFromGeo.getAddressLine(i);
                                break;
                            }
                        }
                    }
                }
                else{
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            return strAddress;
        }

    };


    public void addToFavouritesAndRecentsDatabase(FavouriteOrRecent favouriteOrRecentToAdd){
        SQLiteHelperFavouritesAndRecents theHelper = MainActivity.theFavouritesAndRecentsDatabase;
        if(checkIfFavouriteOrRecentExists(favouriteOrRecentToAdd) == false){
            theHelper.addFavourite(favouriteOrRecentToAdd);
        }
    }

    public boolean checkIfFavouriteOrRecentExists(FavouriteOrRecent favOrRecent){
        SQLiteHelperFavouritesAndRecents theHelper = MainActivity.theFavouritesAndRecentsDatabase;
        if(favOrRecent.isFavourite() | theHelper.getFavouriteOrRecent(favOrRecent) !=null){
            return true;
        }
        else{
            return false;
        }
    }

    public void geocodeLastLocation(Double[] params){
        new GeocodeAddress().execute(params);
    }

    public void geocodeFromGoogle(Double[] params){
        new GeocodeAddressFromGoogle().execute(params);
    }

    public FavouritesCard getFavouritesCard(){
        FavouritesCard tempCard = null;
        for(Card card: activeCards){
            if(card instanceof FavouritesCard){
                tempCard = (FavouritesCard) card;
            }
        }
        return tempCard;
    }

    public SearchCardLocations getSearchCardLocations(){
        SearchCardLocations tempCard = null;
        for(Card card: activeCards){
            if(card instanceof SearchCardLocations){
                tempCard = (SearchCardLocations) card;
            }
        }
        return tempCard;
    }

    private class GeocodeAddressFromGoogle extends AsyncTask<Double,String,String> {

        @Override
        protected void onPostExecute(String address){
            getSearchCardLocations().returnGeocodedLocationFromGoogleToMain(address);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... values) {
        }

        @Override
        protected String doInBackground(Double[] params) {
            CustomReverseGeocoder geocoder = new CustomReverseGeocoder();
            String address = geocoder.reverseGeocode(params[0],params[1]);
            return address;
        }
    };


}
