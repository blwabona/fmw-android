package mobi.wimt.findmyway;

import android.content.Context;
import android.graphics.Color;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import api.ApiClient;
import models.Operators;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by James on 2015-02-19.
 */
public class AdditionalModesFilter {

    public Context context;
    public AdditionalModeFilterer pageToFilter;
    public Map<String,String> additionalModesStates = new HashMap<>();
    public ArrayList<AdditionalMode> additionalModes;
    public static final String PREFS_NAME = "ToggleSettingsAdditionalOperators";

    public AdditionalModesFilter(Context context,AdditionalModeFilterer pageToFilter){
        this.context = context;
        this.pageToFilter = pageToFilter;
        additionalModesStates = new HashMap<>();
    }

    public void getAdditionalModesFromApi(){
        additionalModes = new ArrayList<>();
        ApiClient.getwimtApiClient(false,context).getOperators(new Callback<Operators>() {
            @Override
            public void success(Operators operators, Response response) {
                consumeApiData(operators);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
            }
        });
    }

    public void consumeApiData(Operators operators){
        for(int i=0;i<operators.getOperators().size();i++){
            AdditionalMode newMode= new AdditionalMode(operators.getOperators().get(i).getName(),
                    operators.getOperators().get(i).getCategory(),
                    operators.getOperators().get(i).getModes(),
                    operators.getOperators().get(i).getIsPublic());
            additionalModes.add(newMode);
        }
        populateMap();
    }

    private void populateMap() {
        for(int i=0;i<additionalModes.size();i++){
            if(additionalModes.get(i).isPublic == true) {
                addAdditionalMode(additionalModes.get(i).getNameText());
            }
            else{
                additionalModesStates.put(additionalModes.get(i).getNameText(),"Deactivated");
            }
        }
        pageToFilter.getTheSettings();
    }

    public void populateWithExistingPreferencesAdditional(String state,AdditionalMode mode){
        for(Map.Entry<String,String> entry: additionalModesStates.entrySet()){
            if(entry.getKey().equals(mode.getNameText())){
                if (state.equals("Activated") && entry.getValue().equals("Deactivated")) {
                    toggleFilterAdditionalModes(mode);
                } else if (state.equals("Deactivated") && entry.getValue().equals("Activated")) {
                    toggleFilterAdditionalModes(mode);
                }
                else if(state.equals("Deactivated") && entry.getValue().equals("Deactivated")){
                   mode.getNameLabel().setTextColor(Color.GRAY);
                    mode.getCheckbox().setChecked(false);
                }
                else{
                    mode.getNameLabel().setTextColor(Color.BLACK);
                    mode.getCheckbox().setChecked(true);
                }
            }
        }
    }

    public void turnFilterOnAdditionalModes(String type){
        additionalModesStates.put(type,"Activated");
    }

    public void turnFilterOffAdditionModes(String type){
        additionalModesStates.put(type,"Deactivated");
    }

    public void toggleFilterAdditionalModes(AdditionalMode mode){
        if(additionalModesStates.get(mode.getNameText()).equals("Activated")){
            mode.getNameLabel().setTextColor(Color.GRAY);
            turnFilterOffAdditionModes(mode.getNameText());
            mode.setIsActivated("Deactivated");
            mode.getCheckbox().setChecked(false);
        }
        else{
            mode.getNameLabel().setTextColor(Color.BLACK);
            turnFilterOnAdditionalModes(mode.getNameText());
            mode.setIsActivated("Activated");
            mode.getCheckbox().setChecked(true);
        }
    }

    public Map<String,String> getMenuStates(){
        Map<String,String> statesForSaving = new HashMap<String,String>();
        for (Map.Entry<String, String> entry : additionalModesStates.entrySet()){
            statesForSaving.put(entry.getKey(), entry.getValue());
        }
        return statesForSaving;
    }

    public String getMenuItem(String type){
        for (Map.Entry<String, String> entry : additionalModesStates.entrySet()){
            if(entry.getKey().equals(type) ){
                return entry.getKey();
            }
        }
        return null;
    }

    public void addAdditionalMode(String type){
        additionalModesStates.put(type,"Activated");
    }

    public String getMenuItemState(String type){
        return additionalModesStates.get(type);
    }

    public AdditionalMode getAdditionalMode(String nameText){
       AdditionalMode temp = null;
        for(AdditionalMode mode:additionalModes){
            if(mode.getNameText().equals(nameText)){
                temp = mode;
            }
        }
        return temp;
    }

}
