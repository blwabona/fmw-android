package mobi.wimt.findmyway;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import adapters.FavouritesAndRecentsRecycler;
import controllers.SearchController;


public class Search extends ActionBarActivity implements SearchView.OnQueryTextListener, FavouritesAndRecentsRecycler, View.OnClickListener {

    public SearchController theController;
    public SearchView theSearchBox;
    public ImageView closeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        theSearchBox = (SearchView) findViewById(R.id.theSearchView);
        theSearchBox.setOnQueryTextListener(this);
        int searchClose = theSearchBox.getContext().getResources().getIdentifier("android:id/search_close_btn",null,null);

        ImageView cancel = (ImageView) this.theSearchBox.findViewById(searchClose);
        cancel.setOnClickListener(this);
        removeUnderlining();

        theController = new SearchController(this,theSearchBox);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void removeUnderlining(){
        int searchPlateId = theSearchBox.getContext().getResources().getIdentifier("android:id/search_plate", null, null);
        View searchPlateView = theSearchBox.findViewById(searchPlateId);
        if (searchPlateView != null) {
            searchPlateView.setBackgroundColor(Color.WHITE);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if(newText.equals("")){
            SwitchResultsScreen();
        }
        else {
            theController.switchToResults(newText);
        }
        return false;
    }

    public void SwitchResultsScreen(){
        theController.switchBackToMainResults();
    }

    @Override
    public  void returnResultToParent(FavouriteOrRecent temp){
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putParcelable("favourite",temp);
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }

    //get the result of the expanded maps activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if(requestCode == 1){
                if(data.hasExtra("favourite") ==false){
                    theController.getFavouritesCard().refreshFavourites();
                }
                else {
                    FavouriteOrRecent favouriteOrRecentTemp = data.getExtras().getParcelable("favourite");
                    returnResultToParent(favouriteOrRecentTemp);
                }
            }
            else if(requestCode == 2) {
                if (resultCode == RESULT_OK) {
                    if (data.hasExtra("favourite")) {
                        FavouriteOrRecent favouriteOrRecentTemp = data.getExtras().getParcelable("favourite");
                        theController.addToFavouritesAndRecentsDatabase(favouriteOrRecentTemp);
                        returnResultToParent(favouriteOrRecentTemp);
                    }
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        theSearchBox.post(new Runnable(){
            @Override
            public void run(){
                theSearchBox.setQuery("",false);
            }
        });
        SwitchResultsScreen();
    }

}
