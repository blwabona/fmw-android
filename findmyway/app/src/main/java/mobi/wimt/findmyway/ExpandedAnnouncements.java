package mobi.wimt.findmyway;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import adapters.AnnouncementsRecyclerExpanded;
import api.ApiClient;
import models.AnnouncementModel;
import models.Announcements;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ExpandedAnnouncements extends ActionBarActivity implements AdditionalModeFilterer {

    public RecyclerView recyclerListExpanded;
    public ArrayList<AnnouncementItemExpanded> announcementsListExpanded;
    public ArrayList<AnnouncementItemExpanded> unfilteredListExpanded;
    public AnnouncementsRecyclerExpanded theAdapterExpanded;
    public Toolbar toolbar;
    private ToolbarMenuFilter theMenuFilter;
    private AdditionalModesFilter theAdditionalModesFilter;
    private HashMap<String,String> additionalOperatorStates;
    private static final String PREFS_NAME_MENU = MainActivity.PREFS_NAME;
    public static final String ADDITIONALMODES = "ToggleSettingsAdditionalOperators";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expanded_announcements);

        toolbar = (Toolbar) findViewById(R.id.theToolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);//replace the action bar

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        unfilteredListExpanded = new ArrayList<>();
        deseraliseAnnouncements();

        recyclerListExpanded = (RecyclerView) findViewById(R.id.AnnouncementsRecyclerExpanded);
        LinearLayoutManager llmExpanded = new LinearLayoutManager(this);//might be wrong
        llmExpanded.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerListExpanded.setLayoutManager(llmExpanded);

        theAdapterExpanded = new AnnouncementsRecyclerExpanded(announcementsListExpanded,this);
        recyclerListExpanded.setAdapter(theAdapterExpanded);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        additionalOperatorStates = new HashMap<>();
        theMenuFilter = new ToolbarMenuFilter(menu,this);
        theAdditionalModesFilter = new AdditionalModesFilter(this,this);
        if(retrieveToggleFilterSettingsAdditionalModes() == false){
            theAdditionalModesFilter.getAdditionalModesFromApi();
        }
        retrieveToggleFilterSettingsMenu();
        return true;
    }

    public boolean retrieveToggleFilterSettingsAdditionalModes(){
        SharedPreferences prefs = getSharedPreferences(ADDITIONALMODES, MODE_PRIVATE);
        additionalOperatorStates.clear();
        Map<String,?> keys = prefs.getAll();
        if(keys == null || keys.size()==0){
            return false;
        }
        else {
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                additionalOperatorStates.put(entry.getKey().toString(),entry.getValue().toString());
            }
            return true;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.overFlow) {
            Intent intent = new Intent(this,AdditionalOperators.class);
            saveToggleFilterSettings();
            startActivityForResult(intent,9);
        }
        else if(id == android.R.id.home){
            saveToggleFilterSettings();
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
            return true;
        }
        else{//handles all the other menu items
            filterResultsModes(item,theMenuFilter,false);
            theMenuFilter.toggleFilter(item);
            theMenuFilter.setToast(item);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
       retrieveToggleFilterSettingsAdditionalModes();
       changeFilterStatesOnStart();
       retrieveToggleFilterSettingsMenu();
    }

    public void deseraliseAnnouncements(){
        announcementsListExpanded = new ArrayList<>();
        ArrayList<AnnouncementItem> temp = new ArrayList<>();
        Intent intent = this.getIntent();
        if(intent.hasExtra("announcements")){
            temp  = intent.getExtras().getParcelableArrayList("announcements");
        }
        if(temp == null || temp.size() == 0){
            loadAnnouncementsFromApi();
        }
        else {
            for (AnnouncementItem item : temp) {
                announcementsListExpanded.add(new AnnouncementItemExpanded(item.announcement,item.theOperator,item.operatorImage, retrieveOperatorModes(item.modesStringArray)));
            }

            unfilteredListExpanded.addAll(announcementsListExpanded);
        }

    }

    public String[] retrieveOperatorModes(String modes){
        String tempString = modes.substring(0,modes.length()-1);
        return tempString.split(",");
    }


    public void filterResultsModes(MenuItem item,ToolbarMenuFilter filter,boolean onStart){
        boolean checker;
        boolean operatorChecker;
        ArrayList<AnnouncementItemExpanded> toRemove = new ArrayList<>();
        ArrayList<AnnouncementItemExpanded> toAdd = new ArrayList<>();
        for(AnnouncementItemExpanded Expandeditem: unfilteredListExpanded){
            checker = false;
            operatorChecker = false;
            for(int i =0;i<Expandeditem.operatorModes.length;i++) {
                if (Expandeditem.operatorModes[i].equals(filter.getType(item))) {
                    for (Map.Entry<String, String> entry : additionalOperatorStates.entrySet()) {
                        if((entry.getKey().toLowerCase()).equals((Expandeditem.operator).toLowerCase())){
                            if(entry.getValue().equals("Deactivated")){
                                operatorChecker = true;
                            }
                        }
                    }
                    checker = true;
                }
            }
            if(checker == true && operatorChecker == true && filter.getMenuItemState(item).equals("Activated")){
                toRemove.add(Expandeditem);
            }
            else if(checker == true && operatorChecker == false && filter.getMenuItemState(item).equals("Deactivated")) {
                toAdd.add(Expandeditem);
            }
            else if(checker == true && filter.getMenuItemState(item).equals("Activated")){
                toRemove.add(Expandeditem);
            }

        }
        for(AnnouncementItemExpanded itemExpanded: toRemove){
            announcementsListExpanded.remove(itemExpanded);
        }
        for(AnnouncementItemExpanded itemExpanded: toAdd){
            announcementsListExpanded.add(itemExpanded);
        }
        theAdapterExpanded.notifyDataSetChanged();
    }

    public void changeFilterStatesOnStart(){
        announcementsListExpanded.clear();
        ArrayList<AnnouncementItemExpanded> toAdd = new ArrayList<>();
        for(AnnouncementItemExpanded Expandeditem: unfilteredListExpanded){

            for(int i =0;i<Expandeditem.operatorModes.length;i++) {
                if(theMenuFilter.getMenuItem(Expandeditem.operatorModes[i]) == null){
                }
                else{
                    if(theMenuFilter.getMenuItemState(theMenuFilter.getMenuItem(Expandeditem.operatorModes[i])).equals("Activated")) {
                        toAdd.add(Expandeditem);
                    }
                }
            }
        }

        int secondFilterCount = toAdd.size();
        ArrayList<AnnouncementItemExpanded> toAddTemp = new ArrayList<>();
        toAddTemp.addAll(toAdd);

        for(int j=0;j<secondFilterCount;j++){
            for (Map.Entry<String, String> entry : additionalOperatorStates.entrySet()) {
                if((entry.getKey().toLowerCase()).equals((toAddTemp.get(j).operator).toLowerCase())){
                    if(entry.getValue().equals("Deactivated")){
                        toAdd.remove(toAddTemp.get(j));
                    }
                }
            }
        }

        for(AnnouncementItemExpanded itemExpanded: toAdd){
            announcementsListExpanded.add(itemExpanded);
        }
        theAdapterExpanded.notifyDataSetChanged();
    }

    public boolean retrieveToggleFilterSettingsMenu(){
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME_MENU, MODE_PRIVATE);
        Map<String,?> keys = prefs.getAll();
        if(keys == null){
            return false;
        }
        else {
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                theMenuFilter.populateWithExistingPreferences(entry.getKey(),entry.getValue().toString());
            }
            changeFilterStatesOnStart();
            return true;
        }
    }

    public void saveToggleFilterSettings(){
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME_MENU, MODE_PRIVATE).edit();
        editor.clear();
        Map<String,String> states= theMenuFilter.getMenuStates();
        for (Map.Entry<String, String> entry : states.entrySet()) {
            editor.putString(entry.getKey(), entry.getValue());
        }
        editor.commit();
    }

    public void loadAnnouncementsFromApi(){
        ApiClient.getwimtApiClient(false,this).getAnnouncements(new Callback<Announcements>(){
            @Override
            public void success(Announcements announcementData, Response response) {
                convertData(announcementData.getAnnouncements());
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                System.out.println("Failed");
            }
        });

    }

    public void convertData(List<AnnouncementModel> listModels){
        for (int i = 0; i < listModels.size(); i++) {
            String operator = listModels.get(i).getOperator();
            String description = listModels.get(i).getDescription();
            List<String> modes = listModels.get(i).getModes();
            announcementsListExpanded.add(new AnnouncementItemExpanded(description,
                    operator,
                    getCorrectModeLogo(listModels.get(i).getOperator()),
                    retrieveOperatorModes(convertToString(modes))));
        }
        unfilteredListExpanded.addAll(announcementsListExpanded);
        theAdapterExpanded.notifyDataSetChanged();
    }

    private int getCorrectModeLogo(String operator) {
        if(operator.equals("Taxi")){
            return R.drawable.taxiblack;
        }
        else if(operator.equals("Rail")){
            return R.drawable.trainblack;
        }
        else if(operator.equals("Bus")){
            return R.drawable.busblack;
        }
        else if(operator.equals("Boat")){
            return R.drawable.boatblack;
        }
        else{
            return R.drawable.favouritekarma;
        }
    }

    public String convertToString(List<String> modes){
        String store="";
        String deliminator = ",";
        for(String mode: modes){
            store = store+mode+deliminator;
        }

        return store;
    }

    public void saveToggleFilterSettingsAdditional(){
        SharedPreferences.Editor editor = getSharedPreferences(ADDITIONALMODES, MODE_PRIVATE).edit();
        Map<String,String> states= theAdditionalModesFilter.getMenuStates();
        for (Map.Entry<String, String> entry : states.entrySet()) {
            editor.putString(entry.getKey(), entry.getValue());
        }
        editor.commit();
    }

    @Override
    public void onBackPressed(){
        saveToggleFilterSettings();
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void getTheSettings() {
        if(retrieveToggleFilterSettingsAdditionalModes() == false){
            saveToggleFilterSettingsAdditional();
            retrieveToggleFilterSettingsAdditionalModes();
        }

        retrieveToggleFilterSettingsAdditionalModes();
    }

}
