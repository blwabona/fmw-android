package mobi.wimt.findmyway;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import views.Card;


public class JourneyModeActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journey_mode);
        System.out.println("Creatign sroller");
        HorizontalCardScroller scroller = new HorizontalCardScroller(this);
        scroller.addStageCardsToLayout(new ArrayList<Card>());
        LayoutInflater theInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        System.out.println("Adding views");
        View stageCard = theInflater.inflate(R.layout.expandedstagecard, null, false);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_journey_mode, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
