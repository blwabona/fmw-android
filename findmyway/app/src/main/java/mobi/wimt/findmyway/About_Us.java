package mobi.wimt.findmyway;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


public class About_Us extends ActionBarActivity implements View.OnClickListener {

    private Toolbar toolbar;
    public TextView website;
    public TextView contactUs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        toolbar = (Toolbar) findViewById(R.id.theAboutUsToolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);//replace the action bar

        TextView versionTextView = (TextView) findViewById(R.id.version);
        try {
            versionTextView.setText(this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName);
        }
        catch(Exception e)
        {
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setupViewElements();
    }

    private void setupViewElements() {
        website = (TextView) findViewById(R.id.webAddress);
        contactUs = (TextView)findViewById(R.id.supportLink);

        website.setOnClickListener(this);
        contactUs.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_about_us, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == android.R.id.home){
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Intent intent;
        switch(id){
            case R.id.supportLink:
                intent = new Intent(this, Contact_Us.class);
                startActivity(intent);
                break;
            case R.id.webAddress:
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse((String) website.getText()));
                startActivity(i);
                break;
        }
    }
}
