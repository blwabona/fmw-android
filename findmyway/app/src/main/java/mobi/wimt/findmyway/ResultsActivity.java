package mobi.wimt.findmyway;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import controllers.ResultsController;


public class ResultsActivity extends ActionBarActivity implements View.OnClickListener {

    public ResultsController theController;
    public PathQueryHolder thePathQuery;
    public TextView startLocation;
    public TextView endLocation;
    public TextView arriveBy;
    public ImageView swapper;
    public static final String ADDITIONALMODES = "ToggleSettingsAdditionalOperators";
    public ImageView additionalOperators;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);


        Intent intent = this.getIntent();

        if(intent.hasExtra("theQuery")){
            thePathQuery  = intent.getExtras().getParcelable("theQuery");
        }

        theController = new ResultsController(this);
        theController.setTheQuery(thePathQuery);

        setupElements();

        setUpHeader();


    }

    private void setupElements() {
        startLocation = (TextView)findViewById(R.id.CurrentLocationResults);
        endLocation = (TextView)findViewById(R.id.DestinationResults);
        arriveBy = (TextView)findViewById(R.id.TimeResults);
        swapper =  (ImageView)findViewById(R.id.IconSwapResults);
        additionalOperators = (ImageView)findViewById(R.id.Operators);

        additionalOperators.setOnClickListener(this);
        swapper.setOnClickListener(this);
        startLocation.setOnClickListener(this);
        endLocation.setOnClickListener(this);
        arriveBy.setOnClickListener(this);
    }

    private void setUpHeader() {
        startLocation.setText(thePathQuery.getNameLocation());
        endLocation.setText(thePathQuery.getNameDestination());
        arriveBy.setText(extractArrivingDate(thePathQuery.getEndDate()));
    }

    private String extractArrivingDate(String endDate) {
        if(endDate == null){
            return "Arrive soonest";
        }
        else {
            int startIndex = endDate.indexOf('T');
            String result = endDate.substring(startIndex + 1,endDate.length()).replace(':', 'h');
            return "Arrive by "+ result;
        }
    }

    public String UTCToLocalTime(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date dateToConvert = null;
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            dateToConvert = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(dateToConvert).toString();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_results, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        Intent intent;
        switch(id){
            case R.id.CurrentLocationResults:
                intent = new Intent(this, Search.class);
                startActivityForResult(intent, 1);
                break;
            case R.id.DestinationResults:
                intent = new Intent(this, Search.class);
                startActivityForResult(intent, 2);
                break;
            case R.id.IconSwapResults:
                swapLocations();
                break;
            case R.id.Operators:
                intent = new Intent(this, AdditionalOperators.class);
                startActivityForResult(intent,7);
                break;
            case R.id.TimeResults:
                DatePickerResultsScreen d = new DatePickerResultsScreen(this,this,thePathQuery.getEndDate());
                d.show();
                break;
        }

    }

    public void swapLocations() {

        theController.refreshTrips();

        startLocation.setText(thePathQuery.getNameDestination());
        endLocation.setText(thePathQuery.getNameLocation());

        double tempStartLat = thePathQuery.getStartLatitude();
        double tempStartLong = thePathQuery.getStartLongitude();

        thePathQuery.setStartLatitude(thePathQuery.getEndLatitude());
        thePathQuery.setStartLongitude(thePathQuery.getEndLongitude());

        thePathQuery.setEndLatitude(tempStartLat);
        thePathQuery.setEndLongitude(tempStartLong);

        String tempNameStart = thePathQuery.getNameLocation();

        thePathQuery.setNameLocation(thePathQuery.getNameDestination());
        thePathQuery.setNameDestination(tempNameStart);

        theController.setTheQuery(thePathQuery);

    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if(requestCode == 7){
                theController.refreshTrips();
                thePathQuery.excludedModes = convertToString(theController.getExcludedModes());
                thePathQuery.excludedOperators = convertToString(getExcludedOperators());
                theController.setTheQuery(thePathQuery);
            }
            if(data.hasExtra("favourite")){
                if(requestCode ==1) {
                    FavouriteOrRecent favouriteOrRecentTemp = data.getExtras().getParcelable("favourite");
                    resetQueryStart(favouriteOrRecentTemp);
                }
                else if(requestCode ==2){
                    FavouriteOrRecent favouriteOrRecentTemp = data.getExtras().getParcelable("favourite");
                    resetQueryEnd(favouriteOrRecentTemp);
                }
            }
        }
    }

    public ArrayList<String> retrieveToggleFilterSettingsAdditional(){
        ArrayList<String> excludedOperators = new ArrayList<>();
        SharedPreferences prefs = getSharedPreferences(ADDITIONALMODES, MODE_PRIVATE);
        Map<String,?> keys = prefs.getAll();
        if(keys == null || keys.size()==0){
        }
        else {
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                if(entry.getValue().toString().equals("Deactivated")){
                    excludedOperators.add(entry.getKey());
                }
            }
        }
        return excludedOperators;
    }

    public String[] getExcludedOperators(){
        ArrayList<String> additionals = retrieveToggleFilterSettingsAdditional();

        String[] tempArray = new String[additionals.size()];
        for(int i =0;i<additionals.size();i++){
            tempArray[i] = additionals.get(i);
        }

        return tempArray;

    }



    private void resetQueryStart(FavouriteOrRecent favOrRecent) {
        if(thePathQuery.getEndLatitude() == favOrRecent.getLatitude() && thePathQuery.getEndLongitude() == favOrRecent.getLongitude()){
            Toast.makeText(this, "Location and Destination cannot be the same", Toast.LENGTH_SHORT).show();
        }
        else {
            startLocation.setText(favOrRecent.name);
            theController.refreshTrips();
            thePathQuery.setStartLatitude(favOrRecent.getLatitude());
            thePathQuery.setStartLongitude(favOrRecent.getLongitude());
            thePathQuery.setNameLocation(favOrRecent.getName());
            theController.setTheQuery(thePathQuery);
        }

    }

    private void resetQueryEnd(FavouriteOrRecent favOrRecent){
        if(thePathQuery.getStartLatitude() == favOrRecent.getLatitude() && thePathQuery.getStartLongitude() == favOrRecent.getLongitude()){
            Toast.makeText(this, "Location and Destination cannot be the same", Toast.LENGTH_SHORT).show();
        }
        else {
            endLocation.setText(favOrRecent.name);
            theController.refreshTrips();
            thePathQuery.setEndLatitude(favOrRecent.getLatitude());
            thePathQuery.setEndLongitude(favOrRecent.getLongitude());
            thePathQuery.setNameDestination(favOrRecent.getName());
            theController.setTheQuery(thePathQuery);
        }
    }

    public void resetQueryArriveDate(String endDate){
        if(queryTimeDifference(endDate,getCurrentDateAndTime())) {
            arriveBy.setText(extractArrivingDate(endDate));
            theController.refreshTrips();
            thePathQuery.setEndDate(endDate);
            if(queryTimeDifference(thePathQuery.getStartDate(),getCurrentDateAndTime())){
                theController.setTheQuery(thePathQuery);
            }
            else{
                thePathQuery.setStartDate(getCurrentDateAndTime());
                theController.setTheQuery(thePathQuery);
            }
        }
    }

    public String convertToString(String[] toConvert){
        String store = "";
        for(int i=0;i<toConvert.length;i++){
            if(i ==0){
                store = toConvert[i];
            }
            else{
                store = store+","+toConvert[i];
            }
        }
        return store;
    }

    public String getCurrentDateAndTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        String current = sdf.format(c.getTime());
        return current;
    }

    public String localToGMT(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date dateToConvert = null;
        try {
            dateToConvert = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf.format(dateToConvert).toString();
    }

    public boolean queryTimeDifference(String inputtedDate1,String inputtedDate2){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date testingDate1 = null;
        Date testingDate2 = null;

        try {
            testingDate1 = sdf.parse(inputtedDate1);
            testingDate2 = sdf.parse(inputtedDate2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (testingDate2 != null && testingDate1 != null) {
            if(testingDate1.after(testingDate2)){
                return true;
            }
        } else {
            return false;
        }

        return false;
    }

}
