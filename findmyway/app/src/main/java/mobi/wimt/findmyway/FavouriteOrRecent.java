package mobi.wimt.findmyway;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by James on 2015-01-20.
 */
public class FavouriteOrRecent implements Parcelable {

    public String address;
    public boolean isFavourite;
    public String name;
    public double latitude;
    public double longitude;
    public FavouriteOrRecentViewHolder view;
    public String dateLastModified;
    public boolean isUserCurrentLocation = false;


    public FavouriteOrRecent(String dateCreated,String name,String address,ImageView itemHeart,boolean isFavourite,NestedDynamicList context,TextView theNameLabel,TextView theAddressLabel,View rowView,double latitude,double longitude){
        this.address = address;
        this.isFavourite = isFavourite;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.dateLastModified = dateCreated;
        view = new FavouriteOrRecentViewHolder(theNameLabel,theAddressLabel,rowView,itemHeart,context);
        setupViews(theNameLabel,theAddressLabel,isFavourite,itemHeart);
    }

    public FavouriteOrRecent(String dateCreated,String name,String address,boolean isFavourite,double latitude,double longitude,boolean isUserCurrentLocation){
        this.name = name;
        this.address = address;
        this.isFavourite = isFavourite;
        this.longitude = longitude;
        this.latitude = latitude;
        this.dateLastModified = dateCreated;
        this.isUserCurrentLocation = isUserCurrentLocation;
    }

    public FavouriteOrRecent (Parcel in){
        String[] data = new String[7];

        in.readStringArray(data);

        this.dateLastModified = data[0];
        this.name  = data[1];
        this.address = data[2];
        this.latitude = Double.parseDouble(data[3]);
        this.longitude= Double.parseDouble(data[4]);
        this.isFavourite = Boolean.parseBoolean(data[5]);
        this.isUserCurrentLocation = Boolean.parseBoolean(data[6]);
    }

    private void setupViews(TextView theNameLabel,TextView theAddressLabel,boolean isFavourite,ImageView itemHeart) {
        theNameLabel.setText(name);
        if(theAddressLabel !=null){
            theAddressLabel.setText(address);
        }
        if(isFavourite == true){
            itemHeart.setImageResource(R.drawable.heartfilled);
        }
        else{
            itemHeart.setImageResource(R.drawable.heartoutline);
        }
    }


    public String getDateLastModified() {
        return dateLastModified;
    }

    public void setDateLastModified(String dateCreated) {
        this.dateLastModified = dateCreated;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {this.dateLastModified,this.name,this.address,String.valueOf(this.latitude),String.valueOf(this.longitude),Boolean.toString(this.isFavourite),Boolean.toString(this.isUserCurrentLocation)});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public FavouriteOrRecent createFromParcel(Parcel in) {
            return new FavouriteOrRecent (in);
        }

        public FavouriteOrRecent [] newArray(int size) {
            return new FavouriteOrRecent[size];
        }
    };

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean isFavourite) {
        this.isFavourite = isFavourite;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
