package mobi.wimt.findmyway;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by James on 2015-02-24.
 */
public class StageCoordinatesForRoutes implements Parcelable {

    public double startLat;
    public double startLong;
    public double endLat;
    public double endLong;
    public String routeColor;
    public String routeMode;

    public StageCoordinatesForRoutes(double startLat,double startLong, double endLat,double endLong,String routeColor,String routeMode){
        this.startLat = startLat;
        this.startLong = startLong;
        this.endLat = endLat;
        this.endLong = endLong;
        this.routeColor = routeColor;
        this.routeMode = routeMode;
    }

    public StageCoordinatesForRoutes (Parcel in){
        String[] data = new String[6];

        in.readStringArray(data);
        this.startLat= Double.parseDouble(data[0]);
        this.startLong = Double.parseDouble(data[1]);
        this.endLat = Double.parseDouble(data[2]);
        this.endLong = Double.parseDouble(data[3]);
        this.routeColor = data[4];
        this.routeMode = data[5];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {String.valueOf(this.startLat),
                String.valueOf(this.startLong),
                String.valueOf(this.endLat),
                String.valueOf(this.endLong),
                this.routeColor,
                this.routeMode});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        public StageCoordinatesForRoutes createFromParcel(Parcel in) {
            return new StageCoordinatesForRoutes (in);
        }

        public StageCoordinatesForRoutes [] newArray(int size) {
            return new StageCoordinatesForRoutes[size];
        }
    };

    public double getStartLat() {
        return startLat;
    }

    public double getStartLong() {
        return startLong;
    }

    public double getEndLat() {
        return endLat;
    }

    public double getEndLong() {
        return endLong;
    }

    public String getRouteColor() {
        return routeColor;
    }

    public String getRouteMode() {
        return routeMode;
    }



}
