package mobi.wimt.findmyway;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Toast;

import java.util.Map;

import api.ApiClient;
import models.Users;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class SplashScreen extends Activity {

    public static final String TOKEN = "TheUserToken";
    public String theToken;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.activity_splash_screen);
        retrieveToken();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager= (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void runMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void loadTokenFromApi() {
        if(isNetworkAvailable()){
        }
        else{
            Toast.makeText(this, "No network available some features may not work", Toast.LENGTH_LONG).show();
        }
        loadTheTokenFromApi();
    }

    public void saveToken(String theTokenToSave){
        SharedPreferences.Editor editor = getSharedPreferences(TOKEN, MODE_PRIVATE).edit();
        editor.putString("Token", theTokenToSave);
        editor.commit();
    }

    public void retrieveToken(){
        SharedPreferences prefs = getSharedPreferences(TOKEN, MODE_PRIVATE);
        Map<String,?> keys = prefs.getAll();
        if(keys == null || keys.size()<=0){
            loadTokenFromApi();
        }
        else {
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                if(entry.getKey().equals("Token")){
                    theToken = entry.getValue().toString();
                    runMainActivity();
                }
            }
        }
    }

    public void loadTheTokenFromApi(){
        ApiClient.getfindmywayApiClient(this).getUserToken(new Callback<Users>() {
            @Override
            public void success(Users user, Response response) {
                saveToken(user.getToken());
                runMainActivity();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                System.out.println("Failed");
                runMainActivity();
            }
        });

    }


}
