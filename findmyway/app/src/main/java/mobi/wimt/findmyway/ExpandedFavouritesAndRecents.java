package mobi.wimt.findmyway;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import adapters.FavouritesAndRecentsRecycler;
import sqldatabase.SQLiteHelperFavouritesAndRecents;

public class ExpandedFavouritesAndRecents extends ActionBarActivity implements FavouritesAndRecentsRecycler,NestedDynamicList, View.OnClickListener{

    public ArrayList<FavouriteOrRecent> favouritesListStatesExpandedFromDataBase;
    public ArrayList<FavouriteOrRecent> favouritesOrRecentsExpandedList;
    private Toolbar toolbar;
    public LinearLayout rootLayout;
    public TextView noFavouritesOrRecents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expanded_favourites_and_recents);

        noFavouritesOrRecents = (TextView)findViewById(R.id.noFavouritesOrRecentsExpanded);
        noFavouritesOrRecents.setVisibility(View.GONE);

        addItemsToExpandedAnnouncementsFromDatabase();

        toolbar = (Toolbar) findViewById(R.id.theToolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);//replace the action bar

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rootLayout = (LinearLayout) findViewById(R.id.favouritesAndRecentsLayout);

        favouritesOrRecentsExpandedList = new ArrayList<>();

        addItemsToCard();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_expanded_favourites_and_recents, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == android.R.id.home){
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addItemsToExpandedAnnouncementsFromDatabase(){
        favouritesListStatesExpandedFromDataBase = new ArrayList<>();
        favouritesListStatesExpandedFromDataBase.addAll(MainActivity.theFavouritesAndRecentsDatabase.getAllFavouritesAndRecents());
        if(favouritesListStatesExpandedFromDataBase.size() ==0){
            noFavouritesOrRecents.setVisibility(View.VISIBLE);
        }
        sortFavouritesAndRecentsBasedOnDateAdded(favouritesListStatesExpandedFromDataBase);
    }

    private ArrayList<FavouriteOrRecent> sortFavouritesAndRecentsBasedOnDateAdded(ArrayList<FavouriteOrRecent> list) {
        Collections.sort(list, new Comparator<FavouriteOrRecent>() {
            @Override
            public int compare(FavouriteOrRecent fav1, FavouriteOrRecent fav2) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd:MMMM:yyyy HH:mm:ss a");
                Date dateFav1 = null;
                Date dateFav2 = null;
                try {
                    dateFav1 = sdf.parse(fav1.getDateLastModified());
                    dateFav2 = sdf.parse(fav2.getDateLastModified());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (dateFav1 != null && dateFav2 != null) {
                    return dateFav2.compareTo(dateFav1);
                } else {
                    return 0;
                }
            }
        });
        return list;
    }

    @Override
    public  void returnResultToParent(FavouriteOrRecent tempFavourite){
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putParcelable("favourite",tempFavourite);
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void addItemsToCard(){
        LayoutInflater theInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for(int i=0;i<favouritesListStatesExpandedFromDataBase.size();i++) {
            View theResultItem = theInflater.inflate(R.layout.search_results_line, null, false);
            ImageView theHeart = (ImageView) theResultItem.findViewById(R.id.favouriteHeart);
            TextView theLabelDescription = (TextView) theResultItem.findViewById(R.id.SearchLineDescription);
            TextView theLabelName = (TextView) theResultItem.findViewById(R.id.SearchLineLabelName);

            FavouriteOrRecent item = new FavouriteOrRecent(favouritesListStatesExpandedFromDataBase.get(i).getDateLastModified(),
                    favouritesListStatesExpandedFromDataBase.get(i).getName(),
                    favouritesListStatesExpandedFromDataBase.get(i).getAddress(),
                    theHeart,
                    favouritesListStatesExpandedFromDataBase.get(i).isFavourite,
                    this,
                    theLabelName,
                    theLabelDescription,
                    theResultItem,
                    favouritesListStatesExpandedFromDataBase.get(i).getLatitude(),
                    favouritesListStatesExpandedFromDataBase.get(i).getLongitude());

            favouritesOrRecentsExpandedList.add(item);
            rootLayout.addView(theResultItem);
            theResultItem.setClickable(true);
            theResultItem.setOnClickListener(this);
        }

    }

    @Override
    public FavouriteOrRecent getCorrectItem(View v) {
        FavouriteOrRecent temp = null;
        for (FavouriteOrRecent favourite : favouritesOrRecentsExpandedList) {
            if (favourite.view.itemHeart.equals(v)) {
                temp = favourite;
            }
        }
        return temp;
    }

    public FavouriteOrRecent getRowItem(View v){
        FavouriteOrRecent temp = null;
        for (FavouriteOrRecent favourite : favouritesOrRecentsExpandedList) {
            if(favourite.view.rowView.equals(v)){
                temp = favourite;
            }
        }
        return temp;
    }


    @Override
    public void onClick(View v) {
        FavouriteOrRecent tempFav;
        tempFav = getRowItem(v);
        tempFav.setDateLastModified(getCurrentDate());
        SQLiteHelperFavouritesAndRecents theHelper = MainActivity.theFavouritesAndRecentsDatabase;
        theHelper.updateFavouriteOrRecent(tempFav);
        returnResultToParent(tempFav);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }
    public String getCurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd:MMMM:yyyy HH:mm:ss a");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

}
