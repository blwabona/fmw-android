package mobi.wimt.findmyway;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class ExpandedOperators extends ActionBarActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private ToolbarMenuFilter theMenuFilter;
    public LinearLayout operatorsExpandedLayout;
    public ArrayList<Operator> allOperatorOptions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expanded_operators);

        toolbar = (Toolbar) findViewById(R.id.theToolbarOperatorsExpanded);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);//replace the action bar

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        operatorsExpandedLayout = (LinearLayout) findViewById(R.id.operatorsExpandedLayout);
        allOperatorOptions = new ArrayList<Operator>();

        Intent intent = this.getIntent();

        if(intent.hasExtra("operator-options")){
            allOperatorOptions  = intent.getExtras().getParcelableArrayList("operator-options");
        }

       // getAllOperatorOptions();
        addOperatorOptions();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        theMenuFilter = new ToolbarMenuFilter(menu,this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id == android.R.id.home){
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        else{//handles all the other menu items
            theMenuFilter.toggleFilter(item);
            theMenuFilter.setToast(item);
        }

        return super.onOptionsItemSelected(item);
    }

    public void addOperatorOptions(){
        LayoutInflater theInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for(int i=0;i<allOperatorOptions.size();i++) {
            View operatorOption= theInflater.inflate(R.layout.expanded_operators_line, null, false);
            CheckBox LogoLarge = (CheckBox) operatorOption.findViewById(R.id.operatorCheckboxExpanded);
            TextView OperatorName = (TextView) operatorOption.findViewById(R.id.OperatorLabelExpanded);
            TextView OperatorType= (TextView) operatorOption.findViewById(R.id.OperatorTypeLabel);
            OperatorName.setText(allOperatorOptions.get(i).operatorName);
            OperatorType.setText(allOperatorOptions.get(i).operatorType);
            operatorOption.setClickable(true);
            operatorOption.setOnClickListener(this);
            operatorsExpandedLayout.addView(operatorOption);
        }
    }

    @Override
    public void onClick(View v) {

    }
}
