package mobi.wimt.findmyway;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by James on 2015-01-08.
 * This class deals with the menu icon filters and populates a map that
 * stores each one with its id being the key and its state, either Activated or Deactivated, as
 * its value. New menu options can be added but toasts will only be displayed
 * for the original menu options. The adjusting of transparency is done via ColorFilters
 */
public class ToolbarMenuFilter {

    public Map<MenuItem,String> menuStates= new HashMap<MenuItem,String>();
    public Context context;

    public ToolbarMenuFilter(Menu theMenu,Context context){
        populateMap(theMenu);
        this.context = context;
    }


    private void populateMap(Menu theMenu) {
        for(int i= 0;i<theMenu.size();i++){
            MenuItem tempItem = theMenu.getItem(i);
            addMenuIcon(tempItem);
        }
    }

    public void disableAdditionalOperators(Menu theMenu){
        for(int i= 0;i<theMenu.size();i++){
            MenuItem tempItem = theMenu.getItem(i);
            if(tempItem.getItemId() == R.id.overFlow){
                tempItem.setVisible(false);
            }
        }
    }

    public void populateWithExistingPreferences(String type,String state){
        for (Map.Entry<MenuItem, String> entry : menuStates.entrySet()) {
            if (getType(entry.getKey()) != null){//excludes the settings menu item
                if (getType(entry.getKey()).equals(type)) {
                    if (state.equals("Activated") && entry.getValue().equals("Deactivated")) {
                        toggleFilter(entry.getKey());
                    } else if (state.equals("Deactivated") && entry.getValue().equals("Activated")) {
                        toggleFilter(entry.getKey());
                    }

                }
            }
        }
    }

    public Map<String,String> getMenuStates(){
        Map<String,String> statesForSaving = new HashMap<String,String>();
        for (Map.Entry<MenuItem, String> entry : menuStates.entrySet()){
            if(getType(entry.getKey()) != null) {//exclude the expand option
                statesForSaving.put(getType(entry.getKey()), entry.getValue());
            }
        }
        return statesForSaving;
    }

    public MenuItem getMenuItem(String type){
        for (Map.Entry<MenuItem, String> entry : menuStates.entrySet()){
            if(getType(entry.getKey()) != null) {//exclude the expand option
               if(getType(entry.getKey()).equals(type) ){
                   return entry.getKey();
               }
            }
        }
        return null;
    }

    public void addMenuIcon(MenuItem item){
        menuStates.put(item,"Activated");
    }

    public void turnFilterOn(MenuItem item){
        menuStates.put(item,"Activated");
    }

    public void turnFilterOff(MenuItem item){
        menuStates.put(item,"Deactivated");
    }

    public String getMenuItemState(MenuItem item){
       return menuStates.get(item);
    }

    public void toggleFilter(MenuItem item){
        if(getMenuItemState(item).equals("Activated")){
            Drawable newIcon = (Drawable)item.getIcon();
            newIcon.mutate().setColorFilter(Color.argb(100, 255, 255, 255), PorterDuff.Mode.SRC_IN);
            item.setIcon(newIcon);
            turnFilterOff(item);
        }
        else{
            Drawable newIcon = (Drawable)item.getIcon();
            newIcon.setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            item.setIcon(newIcon);
            turnFilterOn(item);
        }
    }

    public void setToast(MenuItem item){
        int id = item.getItemId();
        switch(id){
//            case R.id.taxiIcon:
//                String tempWalkString = "You have "+getMenuItemState(item)+" the Taxi filter";
//                Toast.makeText(context, tempWalkString, Toast.LENGTH_SHORT).show();
//                break;
            case R.id.boatIcon:
                String tempFerryString = "You have "+getMenuItemState(item)+" the Boat filter";
                Toast.makeText(context, tempFerryString, Toast.LENGTH_SHORT).show();
                break;
            case R.id.busIcon:
                String tempBusString = "You have "+getMenuItemState(item)+" the Bus filter";
                Toast.makeText(context, tempBusString, Toast.LENGTH_SHORT).show();
                break;
            case R.id.trainIcon:
                String tempTrainString = "You have "+getMenuItemState(item)+" the Rail filter";
                Toast.makeText(context, tempTrainString, Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }

    public String getType(MenuItem item){
        int id = item.getItemId();
        switch(id){
//            case R.id.taxiIcon:
//                return "Taxi";
            case R.id.boatIcon:
                return "Boat";
            case R.id.busIcon:
                return "Bus";
            case R.id.trainIcon:
                return "Rail";
            default:
                return null;
        }
    }


}
