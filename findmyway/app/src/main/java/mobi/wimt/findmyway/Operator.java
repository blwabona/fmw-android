package mobi.wimt.findmyway;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by James on 2015-01-22.
 */
public class Operator implements Parcelable{

    public ImageView operatorLogo;
    public String operatorName;
    public int OperatorExpandedResource;
    public ArrayList<String> operatorMaps;
    public boolean isCardOption;
    public String operatorType;

    public Operator(ImageView operatorLogo,String operatorName,int operatorLogoExpandedResource,boolean isCardOption,String operatorType){
        this.operatorLogo = operatorLogo;
        this.operatorName = operatorName;
        this.OperatorExpandedResource = operatorLogoExpandedResource;
        this.isCardOption = isCardOption;
        this.operatorType = operatorType;
        getOperatorMapsFromApi();
        getCorrectOperatorLogo("Golden Arrow");
    }

    public void getOperatorMapsFromApi(){
        operatorMaps = new ArrayList<String>();
        for(int i=0;i<4;i++){
            operatorMaps.add("Test map"+i);
        }
    }

    public void getCorrectOperatorLogo(String operator){
        operatorLogo.setImageResource(R.drawable.jammielarge);
    }

    public Operator (Parcel in){
        String[] data = new String[4];

        in.readStringArray(data);
        this.operatorName= data[0];
        this.isCardOption = Boolean.parseBoolean(data[1]);
        this.OperatorExpandedResource = Integer.parseInt(data[2]);
        this.operatorType = data[3];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {this.operatorName,Boolean.toString(this.isCardOption),String.valueOf(this.OperatorExpandedResource),this.operatorType});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Operator createFromParcel(Parcel in) {
            return new Operator (in);
        }

        public Operator [] newArray(int size) {
            return new Operator[size];
        }
    };
}
