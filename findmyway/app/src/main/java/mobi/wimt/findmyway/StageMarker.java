package mobi.wimt.findmyway;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by James on 2015-03-12.
 */
public class StageMarker {
    public LatLng position;
    public String color;

    public StageMarker(LatLng position, String color){
        this.position = position;
        this.color = color;
    }

    public LatLng getPositon(){
        return position;
    }

    public String getColor(){
        String Store ="";
        Store = "0x"+color.substring(1,color.length());
        return Store;
    }
}
