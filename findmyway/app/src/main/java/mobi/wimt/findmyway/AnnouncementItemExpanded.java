package mobi.wimt.findmyway;


/**
 * Created by James on 2015-01-19.
 */
public class AnnouncementItemExpanded {

    public int operatorImage;
    public String announcement;
    public String operator;
    public String[] operatorModes;

    public AnnouncementItemExpanded(String theAnnouncement,String operator,int operatorImage,String[] modes) {
        this.operator= operator;
        this.operatorImage = operatorImage;
        this.announcement = theAnnouncement;
        this.operatorModes = modes;
    }

}
