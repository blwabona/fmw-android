package mobi.wimt.findmyway;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import models.LoggedTrip;
import sqldatabase.SQLiteHelperLoggedTrips;


public class ExpandedLoggedTrips extends ActionBarActivity{

    public ArrayList<LoggedTrip> loggedTripsFromDatabase;
    public ArrayList<LoggedTrip> theLoggedTripList;
    private Toolbar toolbar;
    public LinearLayout rootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expanded_logged_trips);

        setupViewElements();

        addItemsToExpandedLoggedTripsFromDatabase();

        toolbar = (Toolbar) findViewById(R.id.theToolbarExpandedLogs);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);//replace the action bar

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        theLoggedTripList = new ArrayList<>();

        addItemsToCard();

    }

    private void setupViewElements() {
        rootLayout = (LinearLayout)findViewById(R.id.loggedTripsLayout);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_expanded_logged_trips, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == android.R.id.home){
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addItemsToExpandedLoggedTripsFromDatabase(){
        loggedTripsFromDatabase = new ArrayList<LoggedTrip>();
        SQLiteHelperLoggedTrips db = MainActivity.theLoggedTripsDatabase;
        loggedTripsFromDatabase = db.getAllLoggedTrips();
    }

    public void addItemsToCard(){
        LayoutInflater theInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for(int i=loggedTripsFromDatabase.size()-1;i>=0;i--){
            View theRowView = theInflater.inflate(R.layout.logged_trip_item, null, false);
            TextView creationDate = (TextView) theRowView.findViewById(R.id.dateCreated);
            TextView startLocation = (TextView) theRowView.findViewById(R.id.startAddress);
            TextView endLocation = (TextView) theRowView.findViewById(R.id.endAddress);
            TextView carbonPointsImage = (TextView) theRowView.findViewById(R.id.carbonPoints);
            LoggedTrip item = new LoggedTrip(loggedTripsFromDatabase.get(i).creationDate,
                    loggedTripsFromDatabase.get(i).startLocation,
                    loggedTripsFromDatabase.get(i).endLocation,
                    loggedTripsFromDatabase.get(i).carbonPoints,
                    startLocation,
                    endLocation,
                    creationDate,
                    carbonPointsImage,
                    theRowView);

            theLoggedTripList.add(item);
            rootLayout.addView(theRowView);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }
}
