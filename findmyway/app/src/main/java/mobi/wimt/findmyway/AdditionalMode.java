package mobi.wimt.findmyway;

import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

/**
 * Created by James on 2015-02-19.
 */
public class AdditionalMode {

    public CheckBox checkbox;
    public TextView NameLabel;
    public TextView categoryLabel;
    public String NameText;
    public String categoryText;
    public View theView;
    public String isActivated;
    public List<String> modes;
    public boolean isPublic;


    public AdditionalMode(String name, String category, List<String> modes,boolean isPublic ){
        this.isPublic = isPublic;
        NameText = name;
        categoryText = category;
        this.modes = modes;
    }

    public void setAdditionalData(View theView,CheckBox checkbox,TextView NameLabel,TextView categoryLabel,String isActivated) {
        this.theView = theView;
        this.NameLabel = NameLabel;
        this.categoryLabel = categoryLabel;
        this.checkbox = checkbox;
        this.isActivated = isActivated;
        setupViewElements();
    }

    private void setupViewElements() {
        NameLabel.setText(NameText);
        categoryLabel.setText(categoryText);
    }

    public String getCategoryText() {
        return categoryText;
    }

    public void setSubHeaderText(String categoryText) {
        this.categoryText = categoryText;
    }

    public TextView getNameLabel() {
        return NameLabel;
    }

    public void setNameLabel(TextView NameLabel) {
        NameLabel = NameLabel;
    }

    public TextView getCategoryLabel() {
        return categoryLabel;
    }

    public void setCategoryText(TextView categoryLabel) {
        this.categoryLabel = categoryLabel;
    }

    public String getNameText() {
        return NameText;
    }

    public void setNameText(String NameText) {
        this.NameText = NameText;
    }

    public CheckBox getCheckbox() {
        return checkbox;
    }

    public void setCheckbox(CheckBox checkbox) {
        checkbox = checkbox;
    }

    public String getIsActivated() {
        return isActivated;
    }

    public void setIsActivated(String isActivated) {
        this.isActivated = isActivated;
    }


}
