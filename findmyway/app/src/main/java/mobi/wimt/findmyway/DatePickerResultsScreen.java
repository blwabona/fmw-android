package mobi.wimt.findmyway;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TimePicker;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by James on 2015-03-01.
 */
public class DatePickerResultsScreen  extends Dialog implements View.OnClickListener, TimePicker.OnTimeChangedListener {

    public Activity activity;
    public Dialog d;
    public LinearLayout mainDialogLayout;
    public TimePicker timepicker;
    public Button done;
    public ResultsActivity resultsActivity;
    public DatePicker datePicker;
    public int height;
    public int width;
    public LinearLayout dialogLayout;
    public String endDate;

    public DatePickerResultsScreen(Context context,ResultsActivity resultsActivity,String endDate) {
        super(context);
        this.resultsActivity = resultsActivity;
        this.endDate = endDate;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.custom_timer_picker_dialog);

        dialogLayout = (LinearLayout)findViewById(R.id.dialogLayout);
        mainDialogLayout = (LinearLayout)findViewById(R.id.mainDialogLayout);
        done = (Button)findViewById(R.id.datePickerDone);
        datePicker = (DatePicker) findViewById(R.id.datePicker);

        initialiseDatePicker();

        timepicker = (TimePicker) findViewById(R.id.timepicker);
        timepicker.setIs24HourView(true);

        querySetTimes();

        timepicker.setOnTimeChangedListener(this);
        done.setOnClickListener(this);

    }

    public void initialiseDatePicker(){
        String currentDate;
        if(endDate == null){
            currentDate = getCurrentDateAndTime();
            endDate = currentDate;
        }
        else{
            currentDate = endDate;
        }

        String Year = currentDate.substring(0,currentDate.indexOf("-"));
        String Month = currentDate.substring(currentDate.indexOf("-")+1, currentDate.lastIndexOf('-'));
        String Day = currentDate.substring(currentDate.lastIndexOf('-')+1,currentDate.indexOf('T'));
        Month = Month.replace("0","");
        Day = Day.replace("0","");

        datePicker.init(Integer.valueOf(Year),Integer.valueOf(Month)-1, Integer.valueOf(Day), new DatePicker.OnDateChangedListener()
        {
            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month,int day) {
                boolean checker = false;

                if(year<getCurrentYear()) {
                    datePicker.updateDate(getCurrentYear(), month, day);
                    checker = true;
                }
                if(month<getCurrentMonth()-1 && year<=getCurrentYear()){
                    datePicker.updateDate(year,getCurrentMonth()-1,getCurrentDay());
                    checker = true;
                }

                if(day<getCurrentDay() && month<=getCurrentMonth()-1 && year<=getCurrentYear()){
                    datePicker.updateDate(year, month, getCurrentDay());
                    checker = true;
                }

                if(checker == false){
                    updateEndDate(year, month+1, day);
                }
            }

        });
    }

    public void updateEndDate(int year,int month,int day) {
        int  hour;
        int minute;
        if(endDate == null) {
            hour = getHourFromDate(getCurrentDateAndTime());
            minute = getMinuteFromDate(getCurrentDateAndTime());
        }
        else{
            hour = getHourFromDate(endDate);
            minute = getMinuteFromDate(endDate);
        }
        if(month<10 && day<10){
            endDate = year+"-0"+month+"-0"+day+'T'+hour+ ":"+minute;
        }
        else if (month<10) {
            endDate = year + "-0" + month + "-" + day + 'T' + hour + ":" + minute;
        }
        else if(day<10){
            endDate = year + "-" + month + "-0" + day + 'T' + hour + ":" + minute;
        }
        else{
            endDate = year + "-" + month + "-" + day + 'T' + hour + ":" + minute;
        }
    }


    public void querySetTimes(){
        if(endDate == null){
            timepicker.setCurrentHour(getCurrentHour());
            timepicker.setCurrentMinute(getCurrentMinutes());
        }
        else{
            timepicker.setCurrentHour(getHourFromDate(endDate));
            timepicker.setCurrentMinute(getMinuteFromDate(endDate));
        }
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id) {
            case R.id.datePickerDone:
                resultsActivity.resetQueryArriveDate(endDate);
                dismiss();
        }
    }

    public String updateEndTime(int minutes,int hours){
        String DateAndTime;
        if(endDate == null){
            DateAndTime =  getCurrentDateAndTime();
        }
        else{
            DateAndTime = endDate;
        }

        if(hours<10 && minutes<10){
            return DateAndTime.substring(0,DateAndTime.indexOf('T'))+"T0"+hours+":0"+minutes;
        }
        else if(hours<10){
            return DateAndTime.substring(0,DateAndTime.indexOf('T'))+"T0"+hours+":"+minutes;
        }
        else if(minutes<10){
            return  DateAndTime.substring(0,DateAndTime.indexOf('T'))+"T"+hours+":0"+minutes;
        }
        else {
            return DateAndTime.substring(0,DateAndTime.indexOf('T')) + "T" + hours + ":" + minutes;
        }
    }

    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        if(queryTimeDifference(updateEndTime(Integer.valueOf(minute),Integer.valueOf(hourOfDay)),getCurrentDateAndTime())){
            endDate = updateEndTime(timepicker.getCurrentMinute(), timepicker.getCurrentHour());
        }
        else{
            if (hourOfDay < getCurrentHour()) {

                timepicker.setCurrentHour(getCurrentHour());

                if(minute < getCurrentMinutes()){
                    timepicker.setCurrentMinute(getCurrentMinutes());
                }
            }

            if(minute<getCurrentMinutes() && hourOfDay<=getCurrentHour()){
                timepicker.setCurrentMinute(getCurrentMinutes());
            }
            else{
                endDate = updateEndTime(timepicker.getCurrentMinute(), timepicker.getCurrentHour());
            }
        }
    }


    public int getCurrentHour(){
        return new Time(System.currentTimeMillis()).getHours();
    }

    public int getCurrentMinutes(){
        return new Time(System.currentTimeMillis()).getMinutes();
    }

    public int getCurrentYear(){
        String currentDate = getCurrentDateAndTime();
        return Integer.valueOf(currentDate.substring(0, currentDate.indexOf("-")));
    }

    public int getCurrentMonth(){
        String currentDate = getCurrentDateAndTime();
        return Integer.valueOf(currentDate.substring(currentDate.indexOf("-") + 1, currentDate.lastIndexOf('-')));
    }

    public int getCurrentDay(){
        String currentDate = getCurrentDateAndTime();
        return Integer.valueOf(currentDate.substring(currentDate.lastIndexOf('-') + 1, currentDate.indexOf('T')));
    }

    public int getHourFromDate(String date){
        int StartIndex = date.indexOf('T');
        int EndIndex = date.indexOf(':');
        return Integer.valueOf(date.substring(StartIndex+1,EndIndex));
    }

    public int getMinuteFromDate(String date){
        int StartIndex = date.indexOf(':');
        int EndIndex = date.length();
        return Integer.valueOf(date.substring(StartIndex+1,EndIndex));
    }

    public boolean queryTimeDifference(String inputtedDate1,String inputtedDate2){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date testingDate1 = null;
        Date testingDate2 = null;
        try {
            testingDate1 = sdf.parse(inputtedDate1);
            testingDate2 = sdf.parse(inputtedDate2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (testingDate2 != null && testingDate1 != null) {
            if(testingDate1.after(testingDate2)){
                return true;
            }
        } else {
            return false;
        }

        return false;
    }

    public String getCurrentDateAndTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        String current = sdf.format(c.getTime());
        return current;
    }
}
