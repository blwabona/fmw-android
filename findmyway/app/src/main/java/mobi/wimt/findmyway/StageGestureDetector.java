package mobi.wimt.findmyway;

import android.view.GestureDetector;
import android.view.MotionEvent;

import java.util.ArrayList;

import views.Card;

/**
 * Created by James on 2015-04-14.
 */
public class StageGestureDetector extends GestureDetector.SimpleOnGestureListener {

    private static final int SWIPE_MIN_DISTANCE = 5;
    private static final int SWIPE_THRESHOLD_VELOCITY = 300;
    private HorizontalCardScroller context;
    private ArrayList<Card> stages;

    public StageGestureDetector(HorizontalCardScroller context,ArrayList<Card> stages){
        this.context = context;
        this.stages = stages;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        try {
            //right to left
            if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                int featureWidth = context.getMeasuredWidth();
                context.mActiveFeature = (context.mActiveFeature < (stages.size() - 1))? context.mActiveFeature + 1:stages.size() -1;
                context.smoothScrollTo(context.mActiveFeature*featureWidth, 0);
                return true;
            }
            //left to right
            else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                int featureWidth = context.getMeasuredWidth();
                context.mActiveFeature = (context.mActiveFeature > 0)? context.mActiveFeature - 1:0;
                context.smoothScrollTo(context.mActiveFeature*featureWidth, 0);
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }
}
