package mobi.wimt.findmyway;

import android.view.View;

/**
 * Created by James on 2015-01-21.
 */
public interface NestedDynamicList {

    public FavouriteOrRecent getCorrectItem(View v);

}
