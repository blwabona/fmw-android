package mobi.wimt.findmyway;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;


public class AdditionalOperators extends ActionBarActivity implements View.OnClickListener,AdditionalModeFilterer, CompoundButton.OnCheckedChangeListener {

    public Toolbar toolbar;
    public static AdditionalModesFilter theAdditionalModesFilter;
    public static final String PREFS_NAME = "ToggleSettingsAdditionalOperators";
    private static final String PREFS_NAME_MENU = MainActivity.PREFS_NAME;
    public ArrayList<AdditionalMode> modes;
    public LinearLayout theCardLayout;
    public ToolbarMenuFilter theMenuFilter;
    public ProgressBar loadingSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional_operators);

        toolbar = (Toolbar) findViewById(R.id.additionalOperatorsToolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);//replace the action bar

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setupViewElements();

        theAdditionalModesFilter = new AdditionalModesFilter(this.getApplicationContext(),this);
        theAdditionalModesFilter.getAdditionalModesFromApi();
        modes = new ArrayList<>();
    }

    public void getTheSettings(){
        if(theAdditionalModesFilter.additionalModes.size()<=0){
            TextView noAdditionalOperators = new TextView(this);
            noAdditionalOperators.setText("There are no additional operators");
        }
        else{
            loadingSpinner.setVisibility(View.GONE);
        }

        if(retrieveToggleFilterSettings() == false){
            saveToggleFilterSettings();
            retrieveToggleFilterSettings();
        }
    }

    public void setupViewElements(){
        theCardLayout =(LinearLayout) findViewById(R.id.additionalOperatorsLayout);
        loadingSpinner = (ProgressBar) findViewById(R.id.progressBarAdditionalOps);
        loadingSpinner.setVisibility(View.VISIBLE);
    }

    public boolean retrieveToggleFilterSettings(){
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        Map<String,?> keys = prefs.getAll();
        LayoutInflater theInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(keys == null || keys.size()==0){
            return false;
        }
        else {
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                View theAdditionalMode = theInflater.inflate(R.layout.expanded_operators_line, null, false);
                CheckBox checkBox = (CheckBox) theAdditionalMode.findViewById(R.id.operatorCheckboxExpanded);
                TextView Label = (TextView) theAdditionalMode.findViewById(R.id.OperatorLabelExpanded);
                TextView type = (TextView) theAdditionalMode.findViewById(R.id.OperatorTypeLabel);

                AdditionalMode theMode = theAdditionalModesFilter.getAdditionalMode(entry.getKey());


                if(theMode != null){
                    theMode.setAdditionalData(theAdditionalMode,
                            checkBox,
                            Label,
                            type,entry.getValue().toString());

                    modes.add(theMode);
                    theAdditionalModesFilter.populateWithExistingPreferencesAdditional(entry.getValue().toString(), theMode);
                    theCardLayout.addView(theAdditionalMode);
                    checkBox.setOnCheckedChangeListener(this);
                    theAdditionalMode.setClickable(true);
                    //theAdditionalMode.setOnClickListener(this);
                }
            }
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        theMenuFilter = new ToolbarMenuFilter(menu,this);
        theMenuFilter.disableAdditionalOperators(menu);
        retrieveToggleFilterSettingsMenu();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id == android.R.id.home){
            saveToggleFilterSettings();
            saveToggleFilterSettingsMenu();
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
            return true;
        }
        else{//handles all the other menu items
            theMenuFilter.toggleFilter(item);
            theMenuFilter.setToast(item);
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        AdditionalMode mode = findRowItem(v);
        theAdditionalModesFilter.toggleFilterAdditionalModes(mode);
        saveToggleFilterSettings();
    }

    public AdditionalMode findRowItem(View v){
        AdditionalMode temp = null;
        for (AdditionalMode mode : modes) {
            if(mode.theView.equals(v)){
                temp = mode;
            }
        }
        return temp;
    }

    public AdditionalMode findRowItemBasedOnCheckBox(CompoundButton v){
        AdditionalMode temp = null;
        for (AdditionalMode mode : modes) {
            if(mode.getCheckbox() == v){
                temp = mode;
            }
        }
        return temp;
    }

    public void saveToggleFilterSettings(){
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        Map<String,String> states= theAdditionalModesFilter.getMenuStates();
        for (Map.Entry<String, String> entry : states.entrySet()) {
            editor.putString(entry.getKey(), entry.getValue());
        }
        editor.commit();
    }

    @Override
    public void onBackPressed(){
        saveToggleFilterSettings();
        saveToggleFilterSettingsMenu();
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    public boolean retrieveToggleFilterSettingsMenu(){
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME_MENU, MODE_PRIVATE);
        Map<String,?> keys = prefs.getAll();
        if(keys == null){
            return false;
        }
        else {
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                theMenuFilter.populateWithExistingPreferences(entry.getKey(),entry.getValue().toString());
            }
            return true;
        }
    }

    public void saveToggleFilterSettingsMenu(){
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME_MENU, MODE_PRIVATE).edit();
        editor.clear();
        Map<String,String> states= theMenuFilter.getMenuStates();
        for (Map.Entry<String, String> entry : states.entrySet()) {
            editor.putString(entry.getKey(), entry.getValue());
        }
        editor.commit();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        AdditionalMode mode = findRowItemBasedOnCheckBox(buttonView);
        theAdditionalModesFilter.toggleFilterAdditionalModes(mode);
        saveToggleFilterSettings();
    }
}
