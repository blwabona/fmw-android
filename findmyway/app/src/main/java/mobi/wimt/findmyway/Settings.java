package mobi.wimt.findmyway;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import java.io.File;
import java.util.HashMap;
import java.util.Map;


public class Settings extends ActionBarActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private Toolbar toolbar;
    public RelativeLayout clearFavOption;
    public CheckBox autoPopulate;
    public RelativeLayout clearCache;
    public HashMap<String,String> settingsStates;
    public static final String PREFS_NAME = "AppSettings";
    public static final String TOKEN = "TheUserToken";
    public int settingsCount = 1;
    public static final String PREFS_NAME_TOGGLE_SETTINGS = "ToggleSettings";
    public static final String PREFS_NAME_ADDITIONAL_OPERATOR_SETTINGS = "ToggleSettingsAdditionalOperators";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        toolbar = (Toolbar) findViewById(R.id.theSettingsToolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);//replace the action bar

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        settingsStates = new HashMap<>();
        retrieveAppSettings();
        setupViewElements();

        setSettings();
    }

    private void setSettings() {
        for(int i=0;i<settingsStates.size();i++){
            if(settingsStates.get("AutoPopulate") != null){
                if(settingsStates.get("AutoPopulate").equals("Enabled")){
                    autoPopulate.setChecked(true);
                }
                else{
                    autoPopulate.setChecked(false);
                }
            }
        }
    }

    public void setupViewElements(){
        clearFavOption = (RelativeLayout)findViewById(R.id.SettingLineTwoLayout);
        clearCache = (RelativeLayout)findViewById(R.id.SettingsClearCache);
        autoPopulate = (CheckBox)findViewById(R.id.autoPopulateLocation);
        autoPopulate.setOnCheckedChangeListener(this);
        clearCache.setOnClickListener(this);
        clearFavOption.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id == android.R.id.home){
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id){
            case R.id.SettingLineTwoLayout:
                queryDialogResult();
                break;
            case R.id.SettingsClearCache:
                new AlertDialog.Builder(this)
                        .setTitle("Confirmation")
                        .setMessage("Are you sure you want to clear your cache? All settings will be lost?")
                        .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                clearSharedPreferences(PREFS_NAME);
                                clearSharedPreferences(PREFS_NAME_ADDITIONAL_OPERATOR_SETTINGS);
                                clearSharedPreferences(PREFS_NAME_TOGGLE_SETTINGS);
                                clearSharedPreferences(TOKEN);
                                autoPopulate.setChecked(false);
                                restartNotice();
                            }
                        })
                        .setNegativeButton("No",null).show();

                break;
        }
    }

    public void restartNotice(){
        new AlertDialog.Builder(this)
                .setTitle("Restart")
                .setMessage("The app needs to be restarted. Restart?")
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        clearCache();
                        System.exit(0);
                    }
                })
                .setNegativeButton("No",null).show();
    }

    public void clearCache(){
        try{
            deleteCache(this);
        }
        catch(Exception e){

        }
    }

    private void queryDialogResult() {
        new AlertDialog.Builder(this)
                .setTitle("Confirmation")
                .setMessage("Are you sure you want to clear your favourites and recents?")
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.theFavouritesAndRecentsDatabase.refreshDatabase();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No",null).show();
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(buttonView.getId() == R.id.autoPopulateLocation){
            if(isChecked == true){
                settingsStates.put("AutoPopulate","Enabled");
                saveAppSettings();
            }
            else{
                settingsStates.put("AutoPopulate","Disabled");
                saveAppSettings();
            }
        }
    }

    public void saveAppSettings(){
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        for (Map.Entry<String, String> entry : settingsStates.entrySet()) {
            editor.putString(entry.getKey(), entry.getValue());
        }
        editor.commit();
    }

    public void retrieveAppSettings(){
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        Map<String,?> keys = prefs.getAll();
        if(keys == null){
            for(int i =0;i<settingsCount;i++){
                settingsStates.put("AutoPopulate","Enabled");
            }
        }
        else {
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                settingsStates.put(entry.getKey().toString(),entry.getValue().toString());
            }
        }
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    public void clearSharedPreferences(String sharedPrefName){
        SharedPreferences settings = this.getSharedPreferences(sharedPrefName, Context.MODE_PRIVATE);
        settings.edit().clear().commit();

    }
}
