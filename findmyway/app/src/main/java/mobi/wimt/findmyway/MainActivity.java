package mobi.wimt.findmyway;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Map;
import adapters.CustomDrawerAdapter;
import controllers.CardsController;
import sqldatabase.SQLiteHelperFavouritesAndRecents;
import sqldatabase.SQLiteHelperLoggedTrips;
import views.AnnouncementsCard;


public class MainActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    private String[] drawerListViewItems;
    private ListView drawerListView;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    public static ToolbarMenuFilter theMenuFilter;
    private CardsController theController;
    public static final String PREFS_NAME = "ToggleSettings";
    public static SQLiteHelperFavouritesAndRecents theFavouritesAndRecentsDatabase;
    public static SQLiteHelperLoggedTrips theLoggedTripsDatabase;
    public LinearLayout adBannerLayout;
    public boolean competitionOn = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Set up the drawer and the toolbar
        toolbar = (Toolbar) findViewById(R.id.theToolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);//replace the action bar


        drawerLayout = (DrawerLayout) findViewById(R.id.my_drawer_layout);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.Empty, R.string.Empty);//check validity
        drawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle.syncState();

        //Gets all the menu items from the strings resource and sets a custom adapter to them to display in the drawer
        drawerListViewItems = getResources().getStringArray(R.array.menuOptions);
        drawerListView = (ListView) findViewById(R.id.left_drawer);
        CustomDrawerAdapter adapter = new CustomDrawerAdapter(this,drawerListViewItems);
        drawerListView.setAdapter(adapter);

        drawerListView.setOnItemClickListener(this);

        //setup the controller to handle the cards

        theFavouritesAndRecentsDatabase = new SQLiteHelperFavouritesAndRecents(this);

        if(competitionOn == true){
            theLoggedTripsDatabase = new SQLiteHelperLoggedTrips(this);
        }

        theController= new CardsController(this);

        querySettings();

        adBannerLayout = (LinearLayout)findViewById(R.id.adBannerLayout);
        adBannerLayout.setVisibility(View.GONE);

    }

    public void querySettings(){
        theController.getLocationsCard().querySettings();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        theMenuFilter = new ToolbarMenuFilter(menu,this);

        retrieveToggleFilterSettings();
        return true;
    }

    //get the result of the expanded activities
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 || requestCode == 2){
            if (resultCode == RESULT_OK) {
                theController.refreshFavouritesCard();
                if (data.hasExtra("favourite")) {
                    FavouriteOrRecent favouriteOrRecentTemp = data.getExtras().getParcelable("favourite");
                    theController.changeResultBox(favouriteOrRecentTemp, requestCode);
                    hideKeyboard();
                }
            }
        }
        else{
            theController.refreshFavouritesCard();
            retrieveToggleFilterSettings();
            hideKeyboard();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.overFlow) {
            Intent intent = new Intent(this,AdditionalOperators.class);
            startActivityForResult(intent,7);
        }
        else{//handles all the other menu items
            theMenuFilter.toggleFilter(item);
            theMenuFilter.setToast(item);
            saveToggleFilterSettings();
        }
        return super.onOptionsItemSelected(item);
    }


    public void openSettings(View v) {
        Intent intent = new Intent(this, Settings.class);
        startActivityForResult(intent,8);
    }

    public void openContactUs(View v) {
        Intent intent = new Intent(this, Contact_Us.class);
        startActivity(intent);
    }

    public void hideKeyboard(){
       getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    public void openAboutPage(View v) {
        Intent intent = new Intent(this, About_Us.class);
        startActivity(intent);
    }

    public void saveToggleFilterSettings(){
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        Map<String,String> states= theMenuFilter.getMenuStates();
        for (Map.Entry<String, String> entry : states.entrySet()) {
            editor.putString(entry.getKey(), entry.getValue());
        }
        editor.commit();
    }

    public boolean retrieveToggleFilterSettings(){
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        Map<String,?> keys = prefs.getAll();
        if(keys == null){
            return false;
        }
        else {
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                theMenuFilter.populateWithExistingPreferences(entry.getKey(),entry.getValue().toString());
            }
            return true;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent;
        switch(position){
            case 0:
                intent = new Intent(this, ExpandedFavouritesAndRecents.class);
                startActivity(intent);
                break;
            case 1:
                AnnouncementsCard announceCard = theController.getAnnouncementCard();
                saveToggleFilterSettings();
                intent = new Intent(this, ExpandedAnnouncements.class);
                Bundle bundle = new Bundle();
                if(announceCard == null){
                    bundle.putParcelableArrayList("announcements", null);
                }
                else{
                    ArrayList<AnnouncementItem> announcementsFromApi = announceCard.getAnnouncementsList();
                    bundle.putParcelableArrayList("announcements", announcementsFromApi);
                }

                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            default:
                break;
        }
    }
}

