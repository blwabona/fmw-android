package mobi.wimt.findmyway;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import views.LocationDestinationCard;

/**
 * Created by James on 2015-02-27.
 */
public class DialogIntervalSetter extends Dialog implements SeekBar.OnSeekBarChangeListener, View.OnClickListener {

    public Activity activity;
    public Dialog d;
    public Button done;
    public SeekBar timerHours;
    public SeekBar timerMinutes;
    public TextView bigNumber;
    public TextView timerText;
    public LocationDestinationCard locationsCard;

    public DialogIntervalSetter(Activity activity,LocationDestinationCard locationsCard){
        super(activity);
        this.locationsCard = locationsCard;
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.timer);
        setUpViewElements();
    }

    private void setUpViewElements() {
        done = (Button) findViewById(R.id.TimerDone);
        timerHours = (SeekBar) findViewById(R.id.seekHours);
        timerMinutes = (SeekBar) findViewById(R.id.seekMinutes);
        bigNumber = (TextView)findViewById(R.id.bigNumber);
        timerText  =(TextView)findViewById(R.id.SummeryLabel);

        queryIntervalTimes();

        timerMinutes.setOnSeekBarChangeListener(this);
        timerHours.setOnSeekBarChangeListener(this);
        timerMinutes.setMax(59);
        timerHours.setMax(23);

        done.setOnClickListener(this);

    }

    public void queryIntervalTimes() {
        int hours = 0;
        int minutes= 0;
        if(locationsCard.startDate != null) {
            String times = locationsCard.Now.getText().toString();
            times = times.replace(" ", "");
            if (times.contains("h")) {
                hours = Integer.valueOf(times.substring(0, times.indexOf('h')));
                if (times.contains("mins")) {
                    minutes = Integer.valueOf(times.substring(times.indexOf('h') + 1, times.indexOf('m')));
                }
            } else if (times.contains("mins")) {
                times.substring(0, times.indexOf('m'));
                minutes = Integer.valueOf(times.substring(0, times.indexOf('m')));
            }

            timerHours.setProgress(hours);
            timerMinutes.setProgress(minutes);
        }


        if(minutes == 0 && hours != 0){
            bigNumber.setText(hours+"");
        }
        else if(minutes != 0 && hours == 0){
            bigNumber.setText(minutes+"");
        }
        else if(minutes == 0 && hours == 0){
            locationsCard.startDate = null;
            locationsCard.Now.setText("Now");
        }

    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        int id = seekBar.getId();
        switch (id) {
            case R.id.seekHours:
                if(progress == 0 && timerMinutes.getProgress() == 0){
                    locationsCard.startDate = null;
                    locationsCard.Now.setText("Now");
                    locationsCard.departing.setText("Departing");
                    timerText.setText(progress + " Hours & " + timerMinutes.getProgress() + " Minutes");
                    bigNumber.setText(progress + "");
                }
                else {
                    locationsCard.setInterval(progress, timerMinutes.getProgress());
                    timerText.setText(progress + " Hours & " + timerMinutes.getProgress() + " Minutes");
                    bigNumber.setText(progress + "");
                }
                break;
            case R.id.seekMinutes:
                if(progress == 0 && timerHours.getProgress() == 0){
                    locationsCard.startDate = null;
                    locationsCard.Now.setText("Now");
                    locationsCard.departing.setText("Departing");
                    timerText.setText(timerHours.getProgress() + " Hours & " + progress + " Minutes");
                    bigNumber.setText(progress + "");
                }
                else{
                    locationsCard.setInterval(timerHours.getProgress(),progress);
                    timerText.setText(timerHours.getProgress() + " Hours & " + progress + " Minutes");
                    bigNumber.setText(progress + "");
                }
                break;
            default:
                break;

        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id){
            case R.id.TimerDone:
                if(timerMinutes.getProgress() == 0 && timerHours.getProgress() == 0){
                    locationsCard.startDate = null;
                    locationsCard.Now.setText("Now");
                    locationsCard.departing.setText("Departing");
                }
                else {
                    locationsCard.setInterval(timerHours.getProgress(), timerMinutes.getProgress());
                }
                dismiss();
                break;
        }
    }
}
