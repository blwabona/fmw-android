package mobi.wimt.findmyway;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TimePicker;
import android.widget.DatePicker;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import views.LocationDestinationCard;

/**
 * Created by James on 2015-01-13.
 */
public class DialogDateTimePicker extends Dialog implements View.OnClickListener, TimePicker.OnTimeChangedListener {

    public Activity activity;
    public Dialog d;
    public LinearLayout mainDialogLayout;
    public TimePicker timepicker;
    public Button done;
    public LocationDestinationCard locationsCard;
    public DatePicker datePicker;
    public int height;
    public int width;
    public LinearLayout dialogLayout;

    public DialogDateTimePicker(Activity activity, LocationDestinationCard locationDestinationCard) {
        super(activity);
        this.activity = activity;
        locationsCard = locationDestinationCard;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_timer_picker_dialog);

        dialogLayout = (LinearLayout)findViewById(R.id.dialogLayout);
        mainDialogLayout = (LinearLayout)findViewById(R.id.mainDialogLayout);
        done = (Button)findViewById(R.id.datePickerDone);
        datePicker = (DatePicker) findViewById(R.id.datePicker);

        initialiseDatePicker();

        timepicker = (TimePicker) findViewById(R.id.timepicker);
        timepicker.setIs24HourView(true);

        querySetTimes();

        timepicker.setOnTimeChangedListener(this);
        done.setOnClickListener(this);

    }

    public void initialiseDatePicker(){
        String currentDate = "";
        if(locationsCard.endDate == null){
            currentDate = locationsCard.getCurrentDateAndTime();
        }
        else{
            currentDate = locationsCard.endDate;
        }

        String Year = currentDate.substring(0,currentDate.indexOf("-"));
        String Month = currentDate.substring(currentDate.indexOf("-")+1, currentDate.lastIndexOf('-'));
        String Day = currentDate.substring(currentDate.lastIndexOf('-')+1,currentDate.indexOf('T'));
        Month = Month.replace("0","");
        Day = Day.replace("0","");


        datePicker.init(Integer.valueOf(Year),Integer.valueOf(Month)-1, Integer.valueOf(Day), new DatePicker.OnDateChangedListener()
        {
            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month,int day) {
                boolean checker = false;
                if(year<getCurrentYear()) {
                    datePicker.updateDate(getCurrentYear(), month, day);
                    checker = true;
                }
                if(month<(getCurrentMonth()-1) && year<=getCurrentYear()){
                    datePicker.updateDate(year,getCurrentMonth()-1,getCurrentDay());
                    checker = true;
                }

                if(day<getCurrentDay() && month<=(getCurrentMonth()-1)&& year<=getCurrentYear()){
                    datePicker.updateDate(year, month, getCurrentDay());
                    checker = true;
                }

                if(checker == false){
                    locationsCard.updateEndDate(year, month+1, day);
                    locationsCard.updateTimeButtonEnd();
                }
        }

        });
    }



    public void querySetTimes(){
        if(locationsCard.endDate == null){
            timepicker.setCurrentHour(getCurrentHour());
            timepicker.setCurrentMinute(getCurrentMinutes());
        }
        else{
            timepicker.setCurrentHour(getHourFromDate(locationsCard.endDate));
            timepicker.setCurrentMinute(getMinuteFromDate(locationsCard.endDate));
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id) {
            case R.id.datePickerDone:
                locationsCard.endDate = locationsCard.updateEndTime(timepicker.getCurrentHour(), timepicker.getCurrentMinute());
                locationsCard.updateTimeButtonEnd();
                dismiss();
        }
    }


    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        if(queryTimeDifference(locationsCard.updateEndTime(Integer.valueOf(hourOfDay),Integer.valueOf(minute)),locationsCard.getCurrentDateAndTime())){
            locationsCard.endDate = locationsCard.updateEndTime(timepicker.getCurrentHour(),timepicker.getCurrentMinute());
            locationsCard.updateTimeButtonEnd();
        }
        else{
            if (hourOfDay < getCurrentHour()) {

                timepicker.setCurrentHour(getCurrentHour());

                if(minute < getCurrentMinutes()){
                    timepicker.setCurrentMinute(getCurrentMinutes());
                }
            }

            if(minute<getCurrentMinutes() && hourOfDay<=getCurrentHour()){
                timepicker.setCurrentMinute(getCurrentMinutes());
            }
            else{
                locationsCard.endDate = locationsCard.updateEndTime(timepicker.getCurrentHour(),timepicker.getCurrentMinute());
                locationsCard.updateTimeButtonEnd();
            }
        }
    }


    public int getCurrentHour(){
        return new Time(System.currentTimeMillis()).getHours();
    }

    public int getCurrentMinutes(){
        return new Time(System.currentTimeMillis()).getMinutes();
    }

    public int getCurrentYear(){
        String currentDate = locationsCard.getCurrentDateAndTime();
        return Integer.valueOf(currentDate.substring(0, currentDate.indexOf("-")));
    }

    public int getCurrentMonth(){
        String currentDate = locationsCard.getCurrentDateAndTime();
        return Integer.valueOf(currentDate.substring(currentDate.indexOf("-") + 1, currentDate.lastIndexOf('-')));
    }

    public int getCurrentDay(){
        String currentDate = locationsCard.getCurrentDateAndTime();
        return Integer.valueOf(currentDate.substring(currentDate.lastIndexOf('-') + 1, currentDate.indexOf('T')));
    }

    public int getHourFromDate(String date){
        int StartIndex = date.indexOf('T');
        int EndIndex = date.indexOf(':');
        return Integer.valueOf(date.substring(StartIndex+1,EndIndex));
    }

    public int getMinuteFromDate(String date){
        int StartIndex = date.indexOf(':');
        int EndIndex = date.length();
        return Integer.valueOf(date.substring(StartIndex+1,EndIndex));
    }

    public boolean queryTimeDifference(String inputtedDate1,String inputtedDate2){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date testingDate1 = null;
        Date testingDate2 = null;
        try {
            testingDate1 = sdf.parse(inputtedDate1);
            testingDate2 = sdf.parse(inputtedDate2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (testingDate2 != null && testingDate1 != null) {
            if(testingDate1.after(testingDate2)){
                return true;
            }
        } else {
            return false;
        }

        return false;
    }
}
