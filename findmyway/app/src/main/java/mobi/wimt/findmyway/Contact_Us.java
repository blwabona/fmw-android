package mobi.wimt.findmyway;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import api.ApiClient;
import models.MessageFindMyWay;
import models.MessagePost;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class Contact_Us extends ActionBarActivity implements View.OnClickListener {

    private Toolbar toolbar;
    public EditText emailAddress;
    public EditText body;
    public EditText subject;
    public LinearLayout buttonLayout;
    public Button send;
    public Button clear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        toolbar = (Toolbar) findViewById(R.id.theContactUsToolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);//replace the action bar

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setupViewElements();
    }

    private void setupViewElements() {
        emailAddress = (EditText)findViewById(R.id.emailAddressLabel);
        subject = (EditText)findViewById(R.id.messageSubject);
        body = (EditText)findViewById(R.id.body);
        buttonLayout = (LinearLayout)findViewById(R.id.contactUsButtons);
        send = (Button) findViewById(R.id.send);
        clear = (Button)findViewById(R.id.clear);

        send.setOnClickListener(this);
        clear.setOnClickListener(this);
        emailAddress.setOnClickListener(this);
        subject.setOnClickListener(this);

        body.setBackground(null);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contact_us, menu);
        return true;
    }

    public boolean queryAllInfoEntered(){
        System.out.println(emailAddress.getText().toString().equals(""));
        System.out.println(subject.getText().toString().equals(""));
        System.out.println(body.getText().toString().equals(""));
        if(!emailAddress.getText().toString().equals("") && !subject.getText().toString().equals("") && !body.getText().toString().equals("")){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == android.R.id.home){
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id){
            case R.id.send:
                if(queryAllInfoEntered() == true){
                    MessagePost message = new MessagePost();
                    message.setBody(body.getText().toString());
                    message.setSubject(subject.getText().toString());
                    message.setEmailAddress(emailAddress.getText().toString());
                    //System.out.println(message.getFormattedMessage());
                    sendMessageToTheApi(new MessageFindMyWay(message.getFormattedMessage()));
                }
                else{
                    Toast.makeText(this, "Please fill in all the boxes", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.clear:
                    body.setText("");
                break;
            default:
                break;
        }
    }

    private void sendMessageToTheApi(MessageFindMyWay formattedMessage) {
        postMessageToApi(formattedMessage);
    }

    public void postMessageToApi(MessageFindMyWay message){
        Toast.makeText(this, "Sending message...", Toast.LENGTH_SHORT).show();
        ApiClient.messageToken = true;
        ApiClient.getfindmywayApiClient(this).postMessage(message, new Callback<Object>(){
            @Override
            public void success(Object returnedString, Response response) {
                messageSendingResult(true);
                ApiClient.messageToken = false;
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                System.out.println(retrofitError.getUrl());
                System.out.println(retrofitError);
                messageSendingResult(false);
                ApiClient.messageToken = false;
            }
        });

    }

    public void messageSendingResult(boolean success){
        if(success == true){
            Toast.makeText(this, "Message sent", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, "Failed to send message", Toast.LENGTH_SHORT).show();
        }

    }
}
