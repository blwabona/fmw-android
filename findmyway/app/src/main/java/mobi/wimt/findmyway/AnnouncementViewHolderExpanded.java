package mobi.wimt.findmyway;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by James on 2015-01-19.
 */
public class AnnouncementViewHolderExpanded extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView theAnnouncementLabel;
    public ImageView operatorLogo;
    public RelativeLayout theContainer;
    public ImageView shareIcon;
    public Activity context;

    public AnnouncementViewHolderExpanded(View itemView,Activity context) {
        super(itemView);
        this.context = context;
        shareIcon = (ImageView) itemView.findViewById(R.id.announceShare);
        theAnnouncementLabel = (TextView) itemView.findViewById(R.id.AnnouncementsLineLabel);
        operatorLogo = (ImageView)itemView.findViewById(R.id.operatorIcon);
        theContainer = (RelativeLayout) itemView.findViewById(R.id.rootAnnounceRelativeLayoutExpanded);

        shareIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, theAnnouncementLabel.getText());
        context.startActivity(Intent.createChooser(intent, "Share with"));
    }
}
