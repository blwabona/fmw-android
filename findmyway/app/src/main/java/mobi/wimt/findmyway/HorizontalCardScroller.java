package mobi.wimt.findmyway;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import java.util.ArrayList;

import views.Card;

/**
 * Created by James on 2015-04-13.
 */
public class HorizontalCardScroller extends HorizontalScrollView implements View.OnTouchListener{

    private ArrayList<Card> stages = null;
    private GestureDetector mGestureDetector;
    public int mActiveFeature = 0; // shared variable

    public HorizontalCardScroller(Context context) {
        super(context);
    }

    public HorizontalCardScroller(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HorizontalCardScroller(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void addStageCardsToLayout(ArrayList<Card> stages){
        LinearLayout linearHori = new LinearLayout(getContext());
        linearHori.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        linearHori.setOrientation(LinearLayout.HORIZONTAL);
        addView(linearHori);
        this.stages = stages;
        LayoutInflater theInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for(int i = 0; i < 4; i++){
            System.out.println("Adding views");
            View stageCard = theInflater.inflate(R.layout.expandedstagecard, null, false);
            linearHori.addView(stageCard);
        }
        setOnTouchListener(this);
        mGestureDetector = new GestureDetector(new StageGestureDetector(this,stages));
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        //If the user swipes
        if (mGestureDetector.onTouchEvent(event)) {
            return true;
        }
        else if(event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL ){
            int scrollX = getScrollX();
            int featureWidth = v.getMeasuredWidth();
            mActiveFeature = ((scrollX + (featureWidth/2))/featureWidth);
            int scrollTo = mActiveFeature*featureWidth;
            smoothScrollTo(scrollTo, 0);
            return true;
        }
        else{
            return false;
        }
    }

}
