package mobi.wimt.findmyway;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import api.ApiClient;
import api.CustomReverseGeocoder;
import models.CoordinateModel;
import models.Trips;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MapsActivity extends ActionBarActivity implements GoogleMap.OnMapClickListener, View.OnClickListener {

    private GoogleMap map;
    public ArrayList<StageCoordinatesForRoutes> stagesRouteCoordinate;
    public ArrayList<CoordinateModel> points;
    public String color;
    public String theTripId;
    public Double tempLatToBeConfirmed;
    public Double tempLongToBeConfirmed;
    public FavouriteOrRecent favOrRecentToBeConfirmed;
    public Button confirmationButtonYes;
    public Button confirmationButtonNo;
    public TextView theAddressLabel;
    public LinearLayout theConfirmationLayout;
    public Activity context;
    public ProgressDialog dialog;
    public ProgressDialog routeLoadingDialog;
    private LocationManager theLocationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        context = this;

        setupViewElements();
        setLocationManager();

        if(getTripIdExtra() == true){
            points = new ArrayList<>();
            getTripFromTheApi(theTripId);
        }
        else{
            setMapToSA();
            map.setOnMapClickListener(this);
        }
    }

    private Location getCurrentLocation()
    {
        try {
            Location location = null;
            String provider = LocationManager.GPS_PROVIDER;

            if(provider != null) {
                location = theLocationManager.getLastKnownLocation(provider);
            }

            provider = LocationManager.NETWORK_PROVIDER;

            if (location == null && provider != null) {
                location = theLocationManager.getLastKnownLocation(provider);
            }

            provider = LocationManager.PASSIVE_PROVIDER;

            if (location == null && provider != null) {
                location = theLocationManager.getLastKnownLocation(provider);
            }
            return location;
        }
        catch(Exception e){
            return null;
        }
    }

    public void setLocationManager(){
        theLocationManager = (LocationManager) this.getApplicationContext().getSystemService(this.getApplicationContext().LOCATION_SERVICE);
    }

    private void setMapToSA() {

        Location location = getCurrentLocation();
        float zoom = 5.0f;


        if(location == null)
        {
            location.setLatitude(-30);
            location.setLongitude(25);
        }
        else
        {
            zoom = 16.0f;
        }

        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng((float)location.getLatitude(), (float) location.getLongitude())).zoom(zoom).build();
        try{
            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            map.moveCamera(cameraUpdate);
        }
        catch(Exception e){
        }
    }

    private void getTripFromTheApi(String theTripId) {
        routeLoadingDialog = new ProgressDialog(context);
        routeLoadingDialog.setTitle("Loading route");
        routeLoadingDialog.show();
        ApiClient.mapsToken = true;
        ApiClient.getwimtApiClient(true,this).getTripRoutes(theTripId,new Callback<Trips>() {
                    @Override
                    public void success(Trips model, Response response) {
                        ApiClient.mapsToken = false;
                        routeLoadingDialog.dismiss();
                        consumeTripApiData(model);
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        routeLoadingDialog.dismiss();
                        loadingFailed();
                    }
                });
    }

    private boolean getTripIdExtra() {
        Intent intent = this.getIntent();
        stagesRouteCoordinate = new ArrayList<>();

        if(intent.hasExtra("TripId")){
            theTripId = intent.getStringExtra("TripId");
            if(intent.hasExtra("routes")){
                stagesRouteCoordinate  = intent.getExtras().getParcelableArrayList("routes");
            }
            else{
                return false;
            }
            return true;
        }
        else {
            return false;
        }
    }

    private void setupViewElements() {
        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        confirmationButtonNo = (Button) findViewById(R.id.buttonNo);
        confirmationButtonYes = (Button) findViewById(R.id.buttonYes);
        theAddressLabel = (TextView) findViewById(R.id.addressLabel);
        theConfirmationLayout = (LinearLayout) findViewById(R.id.rootConfirmationLayout);

        confirmationButtonNo.setOnClickListener(this);
        confirmationButtonYes.setOnClickListener(this);

        theConfirmationLayout.setVisibility(View.GONE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_maps, menu);
        return true;
    }

    public void loadingFailed(){
        Toast.makeText(this, "Failed to load you're routes", Toast.LENGTH_SHORT).show();
    }



    private void consumeTripApiData(Trips model) {
        centerMapOnBoundingBox(model.getBoundingBoxTopLeft(),model.getBoundingBoxBottomRight());
        addMarkers();
        for(int i=0;i<model.getRoutes().size();i++){
            points.clear();
            points.addAll(model.getRoutes().get(i).getPoints());
            setThePolyline(points,model.getRoutes().get(i).getColour());
        }
    }

    public void addMarkers(){
        MarkerOptions options = new MarkerOptions();
        for(int i = 0;i<stagesRouteCoordinate.size();i++){
            StageCoordinatesForRoutes stage = stagesRouteCoordinate.get(i);
            LatLng markerStartStage = new LatLng(stage.getStartLat(),stage.getStartLong());
            options.position(markerStartStage);
            map.addMarker(options);
            LatLng markerEndStage = new LatLng(stage.getEndLat(),stage.getEndLong());
            options.position(markerEndStage);
            map.addMarker(options);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        map.clear();
        map.addMarker(new MarkerOptions()
                .position(latLng)
                .title("Your selected location"));

        tempLatToBeConfirmed = latLng.latitude;
        tempLongToBeConfirmed = latLng.longitude;

        Double[] params = new Double[2];
        params[0] = tempLatToBeConfirmed;
        params[1] = tempLongToBeConfirmed;

        new GeocodeAddress().execute(params);
    }


    private void setThePolyline(ArrayList<CoordinateModel>routes,String color) {
        PolylineOptions polyLineOptions;
        polyLineOptions = new PolylineOptions();
        for (int j = 0; j < routes.size(); j++) {

            double lat = routes.get(j).getLatitude();
            double lng = routes.get(j).getLongitude();
            LatLng position = new LatLng(lat, lng);
            polyLineOptions.add(position);
        }
        polyLineOptions.width(10);
        polyLineOptions.color(Color.parseColor(color));

        map.addPolyline(polyLineOptions);
    }

    public void setFavouriteToBeConfirmed(String[] address){
        theConfirmationLayout.setVisibility(View.VISIBLE);
        if(address[0] == null || address[1] == null){
            favOrRecentToBeConfirmed = null;
            theConfirmationLayout.setVisibility(View.GONE);

            Double[] params = new Double[2];
            params[0] = tempLatToBeConfirmed;
            params[1] = tempLongToBeConfirmed;

            new GeocodeAddressFromGoogle().execute(params);


        }
        else {
            theConfirmationLayout.setVisibility(View.VISIBLE);
            theAddressLabel.setText(address[0]+", "+address[1]);
            favOrRecentToBeConfirmed = new FavouriteOrRecent(getCurrentDate(), address[0], address[1], false, tempLatToBeConfirmed, tempLongToBeConfirmed,false);
            dialog.dismiss();
        }
    }

    public void setFavouriteFromGoogleToBeConfirmed(String address){
        theConfirmationLayout.setVisibility(View.VISIBLE);
        String[] addressList = address.split(",");
        if(addressList[0] == null || addressList[1] == null){
            favOrRecentToBeConfirmed = null;
            Toast.makeText(this, "Failed to geocode the selected location", Toast.LENGTH_SHORT).show();
            theConfirmationLayout.setVisibility(View.GONE);
            dialog.dismiss();
        }
        else {
            theConfirmationLayout.setVisibility(View.VISIBLE);
            theAddressLabel.setText(addressList[0]+", "+addressList[1]);
            favOrRecentToBeConfirmed = new FavouriteOrRecent(getCurrentDate(), addressList[0], addressList[1], false, tempLatToBeConfirmed, tempLongToBeConfirmed,false);
            dialog.dismiss();
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id){
            case R.id.buttonYes:
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putParcelable("favourite",favOrRecentToBeConfirmed);
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
                return;
            case R.id.buttonNo:
                theConfirmationLayout.setVisibility(View.GONE);
                map.clear();
        }
    }

    private class GeocodeAddress extends AsyncTask<Double,String,String[]> {


        @Override
        protected void onPostExecute(String[] address){
            setFavouriteToBeConfirmed(address);
        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(context);
            dialog.setTitle("Loading selected location");
            dialog.show();
        }

        @Override
        protected void onProgressUpdate(String... values) {
        }

        @Override
        protected String[] doInBackground(Double[] params) {
            String[] strAddress = new String[2] ;
            Geocoder theGeocoder= new Geocoder(getApplicationContext(), Locale.ENGLISH);

            try {
                List<Address> addressesList = theGeocoder.getFromLocation(params[0],params[1],1);

                if(addressesList != null) {

                    Address fetchedAddressFromGeo = addressesList.get(0);
                    for(int i=0;i<fetchedAddressFromGeo.getMaxAddressLineIndex();i++){
                        if(fetchedAddressFromGeo.getAddressLine(i) != null || fetchedAddressFromGeo.getAddressLine(i) != ""){
                            if(strAddress[0] == null){
                                strAddress[0] = fetchedAddressFromGeo.getAddressLine(i);
                            }
                            else{
                                strAddress[1] = fetchedAddressFromGeo.getAddressLine(i);
                                break;
                            }
                        }
                    }
                }
                else{
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            return strAddress;
        }
    };

    public String getCurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd:MMMM:yyyy HH:mm:ss a");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

    private class GeocodeAddressFromGoogle extends AsyncTask<Double,String,String> {

        public ProgressDialog dialog;

        @Override
        protected void onPostExecute(String address){
            setFavouriteFromGoogleToBeConfirmed(address);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... values) {
        }

        @Override
        protected String doInBackground(Double[] params) {
            CustomReverseGeocoder geocoder = new CustomReverseGeocoder();
            String address = geocoder.reverseGeocode(params[0],params[1]);
            return address;
        }
    };

    public void centerMapOnBoundingBox(CoordinateModel bottomRight, CoordinateModel topLeft)
    {
        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        boundsBuilder.include(new LatLng(topLeft.getLatitude(), topLeft.getLongitude()));
        boundsBuilder.include(new LatLng(bottomRight.getLatitude(), bottomRight.getLongitude()));

        LatLngBounds bounds = boundsBuilder.build();

        DisplayMetrics metrics = this.getApplicationContext().getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, 0);
        map.moveCamera(cu);
    }




}
