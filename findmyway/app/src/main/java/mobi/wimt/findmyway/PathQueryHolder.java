package mobi.wimt.findmyway;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by James on 2015-02-04.
 */
public class PathQueryHolder implements Parcelable {

    public double endLatitude;
    public double endLongitude;
    public double startLatitude;
    public double startLongitude;
    public String endDate;
    public String startDate;
    public String excludedModes;
    public String excludedOperators;
    public String nameLocation;
    public String nameDestination;

    public String getNameLocation() {
        return nameLocation;
    }

    public String getExcludedOperators(){
        return excludedOperators;
    }

    public void setExcludedOperators(String excludedOperators){
        this.excludedOperators = excludedOperators;
    }

    public void setNameLocation(String nameLocation) {
        this.nameLocation = nameLocation;
    }

    public String getNameDestination() {
        return nameDestination;
    }

    public void setNameDestination(String nameDestination) {
        this.nameDestination = nameDestination;
    }

    public PathQueryHolder(){
    }

    public PathQueryHolder (Parcel in){
        String[] data = new String[10];

        in.readStringArray(data);

        this.endLatitude= Double.parseDouble(data[0]);
        this.endLongitude= Double.parseDouble(data[1]);
        this.startLatitude= Double.parseDouble(data[2]);
        this.startLongitude= Double.parseDouble(data[3]);
        this.endDate= data[4];
        this.startDate = data[5];
        this.excludedModes = data[6];
        this.excludedOperators = data[7];
        this.nameDestination= data[8];
        this.nameLocation = data[9];
    }


    public String getExcludedModes() {
        return excludedModes;
    }

    public void setExcludedModes(String excludedModes) {
        this.excludedModes = excludedModes;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public double getStartLongitude() {
        return startLongitude;
    }

    public void setStartLongitude(double startLongitude) {
        this.startLongitude = startLongitude;
    }

    public double getStartLatitude() {
        return startLatitude;
    }

    public void setStartLatitude(double startLatitude) {
        this.startLatitude = startLatitude;
    }

    public double getEndLongitude() {
        return endLongitude;
    }

    public void setEndLongitude(double endLongitude) {
        this.endLongitude = endLongitude;
    }

    public double getEndLatitude() {
        return endLatitude;
    }

    public void setEndLatitude(double endLatitude) {
        this.endLatitude = endLatitude;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {String.valueOf(this.endLatitude),
                String.valueOf(this.endLongitude),
                String.valueOf(this.startLatitude),
                String.valueOf(this.startLongitude),
                this.endDate,
                this.startDate,
                this.excludedModes,
                this.excludedOperators,
                this.nameDestination,
                this.nameLocation});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public PathQueryHolder createFromParcel(Parcel in) {
            return new PathQueryHolder (in);
        }

        public PathQueryHolder [] newArray(int size) {
            return new PathQueryHolder[size];
        }
    };
}
