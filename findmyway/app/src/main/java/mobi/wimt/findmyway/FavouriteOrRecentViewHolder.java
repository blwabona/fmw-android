package mobi.wimt.findmyway;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import sqldatabase.SQLiteHelperFavouritesAndRecents;

/**
 * Created by James on 2015-02-02.
 */
public class FavouriteOrRecentViewHolder implements View.OnClickListener {

    public TextView theAddressLabel;
    public TextView theNameLabel;
    public View rowView;
    public ImageView itemHeart;
    public NestedDynamicList context;

    public FavouriteOrRecentViewHolder(TextView theNameLabel,TextView theAddressLabel,View theRowView,ImageView itemHeart,NestedDynamicList context){
        this.itemHeart = itemHeart;
        this.theAddressLabel = theAddressLabel;
        this.rowView = theRowView;
        this.theNameLabel = theNameLabel;
        this.context = context;
        itemHeart.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.favouriteHeart:
                ImageView temp = (ImageView) v;
                if (context.getCorrectItem(v) != null) {
                    FavouriteOrRecent tempFavOrRecent = context.getCorrectItem(v) ;
                    if (tempFavOrRecent.isFavourite == false) {
                        if(checkIfFavouriteOrRecentExists(tempFavOrRecent) == true){
                            makeFavouriteFromRecent(tempFavOrRecent);
                        }
                        else{
                            tempFavOrRecent.isFavourite = true;
                            addToFavouritesAndRecentsDatabase(tempFavOrRecent);
                        }
                        temp.setImageResource(R.drawable.heartfilled);
                    } else {
                        if(checkIfFavouriteOrRecentExists(tempFavOrRecent) == true){
                            makeRecentFromFavourite(tempFavOrRecent);
                        }
                        else{
                            tempFavOrRecent.isFavourite = false;
                            addToFavouritesAndRecentsDatabase(tempFavOrRecent);
                        }
                        temp.setImageResource(R.drawable.heartoutline);
                    }
                }
                break;

        }
    }

    public boolean checkIfFavouriteOrRecentExists(FavouriteOrRecent favOrRecent){
        SQLiteHelperFavouritesAndRecents theHelper = MainActivity.theFavouritesAndRecentsDatabase;
        if(theHelper.getFavouriteOrRecent(favOrRecent) !=null){
            return true;
        }
        else{
            return false;
        }
    }

    public void addToFavouritesAndRecentsDatabase(FavouriteOrRecent favouriteOrRecentToAdd){
        SQLiteHelperFavouritesAndRecents theHelper = MainActivity.theFavouritesAndRecentsDatabase;
        theHelper.addFavourite(favouriteOrRecentToAdd);
    }

    public void makeRecentFromFavourite(FavouriteOrRecent favOrRecent){
        favOrRecent.isFavourite = false;
        SQLiteHelperFavouritesAndRecents theHelper = MainActivity.theFavouritesAndRecentsDatabase;
        favOrRecent.setDateLastModified(getCurrentDate());
        theHelper.updateFavouriteOrRecent(favOrRecent);
    }

    public void makeFavouriteFromRecent(FavouriteOrRecent favOrRecent){
        favOrRecent.isFavourite = true;
        favOrRecent.setDateLastModified(getCurrentDate());
        SQLiteHelperFavouritesAndRecents theHelper = MainActivity.theFavouritesAndRecentsDatabase;
        theHelper.updateFavouriteOrRecent(favOrRecent);

    }


    public String getCurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd:MMMM:yyyy HH:mm:ss a");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

}
