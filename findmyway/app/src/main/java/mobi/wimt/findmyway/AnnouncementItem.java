package mobi.wimt.findmyway;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import controllers.CardsController;

/**
 * Created by James on 2015-01-15.
 */
public class AnnouncementItem implements View.OnClickListener,Parcelable {

    public int operatorImage;
    public String announcement;
    public TextView theLabel;
    public ImageView shareIcon;
    public ImageView operatorLogo;
    public RelativeLayout id;
    public List<String> operatorModes;
    public String modesStringArray;
    public CardsController theController;
    public String theOperator;

    public AnnouncementItem(String theOperator,String theAnnouncement,TextView theLabel,ImageView theOperatorLogo,ImageView theShareIcon,int operatorImage,List<String> modes,CardsController theController) {
        this.operatorImage = operatorImage;
        this.announcement = theAnnouncement;
        this.theLabel = theLabel;
        this.shareIcon = theShareIcon;
        this.operatorLogo = theOperatorLogo;
        this.theOperator = theOperator;
        this.operatorModes = modes;
        this.theController = theController;
        setupElements();
    }

    private void setupElements() {
        checkForLinks(announcement);
        operatorLogo.setImageResource(operatorImage);
        shareIcon.setOnClickListener(this);
        modesStringArray = convertToString(operatorModes);
    }

    public AnnouncementItem (Parcel in){
        String[] data = new String[4];

        in.readStringArray(data);
        this.theOperator = data[0];
        this.announcement= data[1];
        this.operatorImage = Integer.parseInt(data[2]);
        this.modesStringArray = data[3];
    }

    public void checkForLinks(String announcement){
        if(announcement.contains("http://")){
            int linkStart = announcement.indexOf("http://");
            int linkEnd = announcement.indexOf(" ",linkStart);
            if(linkEnd == -1){
                linkEnd = announcement.length();
            }
            String store = announcement.substring(linkStart,linkEnd);
            String Beginning = (announcement.substring(0,linkStart));
            String End = announcement.substring(linkEnd,announcement.length());
            String link = String.format("<a href=\"%s\">"+store+"</a> ", store);
            String ToConvert = Beginning+link+End;
            theLabel.setText(Html.fromHtml(ToConvert));
            theLabel.setMovementMethod(android.text.method.LinkMovementMethod.getInstance());
        }
        else{
            theLabel.setText(announcement);
        }
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id){
            case R.id.announceShare:
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, announcement);
                theController.theView.startActivity(Intent.createChooser(intent, "Share with"));
                break;
        }

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {this.theOperator,this.announcement,String.valueOf(this.operatorImage),this.modesStringArray});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        public AnnouncementItem createFromParcel(Parcel in) {
            return new AnnouncementItem (in);
        }

        public AnnouncementItem [] newArray(int size) {
            return new AnnouncementItem[size];
        }
    };

    public String convertToString(List<String> modes){
        String store="";
        String deliminator = ",";
        for(String mode: modes){
           store = store+mode+deliminator;
        }

        return store;
    }
}
