package mobi.wimt.findmyway;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import models.StageModel;
import models.StagePointModel;

/**
 * Created by James on 2015-02-06.
 */
public class StageItem {

    public LinearLayout stageViewRootLayout;
    public View stageView;
    public String theStageName;
    public StageModel theModel;
    public List<StagePointModel> stagePoints;
    public String stageType;
    public String description;
    public String stageColor;
    public String stageCost;
    public String operator;
    public Activity theView;
    public ArrayList<View> stageLineViews;
    public ArrayList<View> intermediateStopsToExpand;
    public LinearLayout intermediateLayout;
    public boolean hasIntermediates = false;
    public boolean intermediatesExpanded = false;
    public View intermediateOverflowRow;
    public TextView intermediateStopCountLabel;

    public StageItem(View stageView,LinearLayout stageViewRootLayout,StageModel theModel,Activity theView){
        this.stageViewRootLayout = stageViewRootLayout;
        this.theModel = theModel;
        this.stageView = stageView;
        this.theView = theView;
        setupStageData();
        setupViews();
        determineType();
    }

    private void setupStageData() {
        stageLineViews = new ArrayList<>();
        stagePoints = theModel.getStageLocations();
        stageType = theModel.getMode();
        description = theModel.getDescription();
        stageColor = theModel.getColour();
        stageCost = theModel.getCost();
        operator = theModel.getOperator();
        theStageName = theModel.getName();
    }

    public void setupViews(){
        intermediateStopsToExpand =new ArrayList<>();
        intermediateLayout = new LinearLayout(theView);
        intermediateLayout.setOrientation(LinearLayout.VERTICAL);
        ViewGroup.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        intermediateLayout.setLayoutParams(params);
    }

    private void determineType() {
        switch(stageType){
            case "Bus":
                setBusStages();
                break;
            case "Rail":
                setRailStages();
                break;
            case "Pedestrian":
                setPedestrianStages();
                break;
            case  "Taxi":
                setTaxiStages();
                break;
            case "Boat":
                setFerryStages();
        }
    }

    private void setFerryStages() {
        LayoutInflater theInflater = (LayoutInflater) theView.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View stageLineItemView = null;
        for(int i=0;i<stagePoints.size();i++) {
            StagePointModel point = stagePoints.get(i);
            stageLineItemView = theInflater.inflate(R.layout.overview_line_item, null, false);
            TextView theStageLabel = (TextView) stageLineItemView.findViewById(R.id.overviewDescription);
            TextView stageLineTime = (TextView) stageLineItemView.findViewById(R.id.stageTime);
            TextView operatorLabel = (TextView) stageLineItemView.findViewById(R.id.subTextOperator);
            ImageView theStageTypeIcon = (ImageView) stageLineItemView.findViewById(R.id.stageItemType);
            TextView subHeaderLabel = (TextView) stageLineItemView.findViewById(R.id.subText);
            if (i == 0) {
                String theStageLineName = point.getName();
                theStageLabel.setText(theStageLineName);
                subHeaderLabel.setText(theStageName);
                theStageLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size));
                subHeaderLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size_sub_header));
                operatorLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size_sub_header));

                stageLineTime.setText(extractTimeFromDate(point.getTime()));
                operatorLabel.setText(theModel.getOperator());
                theStageTypeIcon.setImageDrawable(getStageLineIcon(getStartStageDrawable(stageType)));
                stageLineViews.add(stageLineItemView);
            } else if (i == stagePoints.size() - 1) {
                String theStageLineName = point.getName();
                theStageLabel.setText(theStageLineName);
                theStageLabel.setPadding(0, 14, 0, 0);
                theStageLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size));
                operatorLabel.setVisibility(View.GONE);
                subHeaderLabel.setVisibility(View.GONE);
                stageLineTime.setText(extractTimeFromDate(UTCToLocalTime(point.getTime())));
                theStageTypeIcon.setImageDrawable(getStageLineIcon(getStopStageDrawable(stageType)));
                stageLineViews.add(stageLineItemView);
            } else {
                String theStageLineName = point.getName();
                theStageLabel.setText(theStageLineName);
                subHeaderLabel.setVisibility(View.GONE);
                operatorLabel.setVisibility(View.GONE);
                theStageLabel.setPadding(0, 14, 0, 0);
                theStageLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size_sub_header));
                stageLineTime.setText(extractTimeFromDate(UTCToLocalTime(point.getTime())));
                theStageTypeIcon.setImageDrawable(getStageLineIcon(getPointToPointDrawable(stageType)));
                intermediateStopsToExpand.add(stageLineItemView);
                if (hasIntermediates == false) {
                    stageLineViews.add(intermediateLayout);
                    addIntermediateOverflowRow();
                    hasIntermediates = true;
                }
            }
        }
    }

    private void setTaxiStages() {
    }

    public int getPointToPointDrawable(String StageType){
        switch(stageType){
            case "Taxi":
                return R.drawable.pointtopointtaxi;
            case "Rail":
                return R.drawable.pointtopointtrain;
            case "Bus":
                return R.drawable.pointtopointbus;
            case "Pedestrian":
                return R.drawable.pointtopointwalking;
            case "Boat":
                return R.drawable.pointtopointboat;
            default:
                return R.drawable.pointtopointtaxi;
        }
    }

    public int getStartStageDrawable(String stageType){
        switch(stageType){
            case "Taxi":
                return R.drawable.startstoptaxi;
            case "Pedestrian":
                return R.drawable.startstopwalk;
            case "Rail":
                return R.drawable.startstoptrain;
            case "Bus":
                return R.drawable.startstopbus;
            case "Boat":
                return R.drawable.startstopboat;
            default:
                return R.drawable.startstoptaxi;
        }
    }

    public int getStopStageDrawable(String stageType){
        switch(stageType){
            case "Taxi":
                return R.drawable.finalstoptrain;
            case "Pedestrian":
                return R.drawable.finalstopwalking;
            case "Rail":
                return R.drawable.finalstoptrain;
            case "Bus":
                return R.drawable.finalstopbus;
            case "Boat":
                return R.drawable.finalstopboat;
            default:
                return R.drawable.finalstopboat;
        }
    }


    private void setRailStages() {
        LayoutInflater theInflater = (LayoutInflater) theView.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View stageLineItemView = null;
        for(int i=0;i<stagePoints.size();i++){
            StagePointModel point = stagePoints.get(i);
            stageLineItemView= theInflater.inflate(R.layout.overview_line_item, null, false);
            TextView theStageLabel =(TextView) stageLineItemView.findViewById(R.id.overviewDescription);
            TextView stageLineTime = (TextView) stageLineItemView.findViewById(R.id.stageTime);
            TextView operatorLabel = (TextView)stageLineItemView.findViewById(R.id.subTextOperator);
            ImageView theStageTypeIcon = (ImageView) stageLineItemView.findViewById(R.id.stageItemType);
            TextView subHeaderLabel = (TextView)stageLineItemView.findViewById(R.id.subText);
            if(i == 0){
                String theStageLineName = point.getName();
                theStageLabel.setText(theStageLineName);
                subHeaderLabel.setText(theStageName);
                theStageLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size));
                subHeaderLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size_sub_header));
                operatorLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size_sub_header));

                stageLineTime.setText(extractTimeFromDate(UTCToLocalTime(point.getTime())));
                operatorLabel.setText(theModel.getOperator());
                theStageTypeIcon.setImageDrawable(getStageLineIcon(getStartStageDrawable(stageType)));
                stageLineViews.add(stageLineItemView);
            }
            else if(i == stagePoints.size()-1){
                String theStageLineName = point.getName();
                theStageLabel.setText(theStageLineName);
                theStageLabel.setPadding(0,14,0,0);
                theStageLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size));
                operatorLabel.setVisibility(View.GONE);
                subHeaderLabel.setVisibility(View.GONE);
                stageLineTime.setText(extractTimeFromDate(UTCToLocalTime(point.getTime())));
                theStageTypeIcon.setImageDrawable(getStageLineIcon(getStopStageDrawable(stageType)));
                stageLineViews.add(stageLineItemView);
            }
            else{
                String theStageLineName = point.getName();
                theStageLabel.setText(theStageLineName);
                subHeaderLabel.setVisibility(View.GONE);
                operatorLabel.setVisibility(View.GONE);
                theStageLabel.setPadding(0, 14, 0, 0);
                theStageLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size_sub_header));
                stageLineTime.setText(extractTimeFromDate(UTCToLocalTime(point.getTime())));
                theStageTypeIcon.setImageDrawable(getStageLineIcon(getPointToPointDrawable(stageType)));
                intermediateStopsToExpand.add(stageLineItemView);
                if(hasIntermediates == false){
                    stageLineViews.add(intermediateLayout);
                    addIntermediateOverflowRow();
                    hasIntermediates = true;
                }
            }
        }
        if(hasIntermediates == true){
            intermediateStopCountLabel.setText(intermediateStopsToExpand.size()+" intermediate stops to expand");
        }
    }

    public void addIntermediateOverflowRow(){
        LayoutInflater theInflater = (LayoutInflater) theView.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        intermediateOverflowRow =  theInflater.inflate(R.layout.overview_line_item, null, false);
        ImageView theStageTypeIcon = (ImageView) intermediateOverflowRow.findViewById(R.id.stageItemType);
        TextView stageLineTime = (TextView) intermediateOverflowRow.findViewById(R.id.stageTime);
        TextView subText  = (TextView)intermediateOverflowRow.findViewById(R.id.subText);
        TextView operatorLabel = (TextView)intermediateOverflowRow.findViewById(R.id.subTextOperator);
        intermediateStopCountLabel = (TextView)intermediateOverflowRow.findViewById(R.id.overviewDescription);
        intermediateStopCountLabel.setPadding(0,15,0,0);
        stageLineTime.setVisibility(View.GONE);
        operatorLabel.setVisibility(View.GONE);
        subText.setVisibility(View.GONE);
        intermediateStopCountLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size_sub_header));
        intermediateStopCountLabel.setTextColor(Color.GRAY);
        theStageTypeIcon.setImageDrawable(getStageLineIcon(R.drawable.overflowindicator));
        intermediateLayout.addView(intermediateOverflowRow);
    }

    public Drawable getStageLineIcon(int drawableId){
        Drawable drawable = theView.getResources().getDrawable(drawableId);
        drawable  = drawable.mutate();
        drawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor(stageColor), PorterDuff.Mode.MULTIPLY));
        return drawable;
    }

    private void setPedestrianStages() {
        LayoutInflater theInflater = (LayoutInflater) theView.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View stageLineItemView = null;
        for(int i=0;i<stagePoints.size();i++){
            StagePointModel point = stagePoints.get(i);
            stageLineItemView= theInflater.inflate(R.layout.overview_line_item, null, false);

            TextView theStageLabel =(TextView) stageLineItemView.findViewById(R.id.overviewDescription);
            TextView stageLineTime = (TextView) stageLineItemView.findViewById(R.id.stageTime);
            TextView operatorLabel = (TextView)stageLineItemView.findViewById(R.id.subTextOperator);
            ImageView theStageTypeIcon = (ImageView) stageLineItemView.findViewById(R.id.stageItemType);
            TextView subHeaderLabel = (TextView)stageLineItemView.findViewById(R.id.subText);
            if(i == 0){
                String theStageLineName = point.getName();
                theStageLabel.setText(theStageLineName);
                theStageLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size));
                subHeaderLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size_sub_header));
                operatorLabel.setVisibility(View.GONE);
                subHeaderLabel.setText(theStageName);
                stageLineTime.setText(extractTimeFromDate(UTCToLocalTime(point.getTime())));
                theStageTypeIcon.setImageDrawable(getStageLineIcon(getStartStageDrawable(stageType)));
                stageLineViews.add(stageLineItemView);
            }
            else if(i == stagePoints.size()-1){
                String theStageLineName = point.getName();
                theStageLabel.setText(theStageLineName);
                theStageLabel.setPadding(0, 14, 0, 0);
                theStageLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size));
                subHeaderLabel.setVisibility(View.GONE);
                operatorLabel.setVisibility(View.GONE);
                stageLineTime.setText(extractTimeFromDate(UTCToLocalTime(point.getTime())));
                theStageTypeIcon.setImageDrawable(getStageLineIcon(getStopStageDrawable(stageType)));
                stageLineViews.add(stageLineItemView);

            }
            else{
                String theStageLineName = point.getName();
                theStageLabel.setText(theStageLineName);
                subHeaderLabel.setVisibility(View.GONE);
                operatorLabel.setVisibility(View.GONE);
                theStageLabel.setPadding(0,15,0,15);
                theStageLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size_sub_header));
                stageLineTime.setText(extractTimeFromDate(UTCToLocalTime(point.getTime())));
                theStageTypeIcon.setImageDrawable(getStageLineIcon(getPointToPointDrawable(stageType)));
                intermediateStopsToExpand.add(stageLineItemView);
                if(hasIntermediates == false){
                    stageLineViews.add(intermediateLayout);
                    hasIntermediates = true;
                }
            }
        }

        if(hasIntermediates == true){
            intermediateStopCountLabel.setText(intermediateStopsToExpand.size()+" intermediate stops to expand");
        }

    }

    private void setBusStages() {
        LayoutInflater theInflater = (LayoutInflater) theView.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View stageLineItemView = null;
        for(int i=0;i<stagePoints.size();i++){
            StagePointModel point = stagePoints.get(i);
            stageLineItemView= theInflater.inflate(R.layout.overview_line_item, null, false);
            TextView theStageLabel =(TextView) stageLineItemView.findViewById(R.id.overviewDescription);
            TextView stageLineTime = (TextView) stageLineItemView.findViewById(R.id.stageTime);
            TextView operatorLabel = (TextView)stageLineItemView.findViewById(R.id.subTextOperator);
            ImageView theStageTypeIcon = (ImageView) stageLineItemView.findViewById(R.id.stageItemType);
            TextView subHeaderLabel = (TextView)stageLineItemView.findViewById(R.id.subText);
            if(i == 0){
                String theStageLineName = point.getName();
                theStageLabel.setText(theStageLineName);
                subHeaderLabel.setText(theStageName);
                theStageLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size));
                subHeaderLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size_sub_header));
                operatorLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size_sub_header));
                stageLineTime.setText(extractTimeFromDate(UTCToLocalTime(point.getTime())));
                operatorLabel.setText(theModel.getOperator());
                theStageTypeIcon.setImageDrawable(getStageLineIcon(getStartStageDrawable(stageType)));
                stageLineViews.add(stageLineItemView);
            }
            else if(i == stagePoints.size()-1){
                String theStageLineName = point.getName();
                theStageLabel.setText(theStageLineName);
                theStageLabel.setPadding(0,14,0,0);
                subHeaderLabel.setVisibility(View.GONE);
                theStageLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size));
                operatorLabel.setVisibility(View.GONE);
                stageLineTime.setText(extractTimeFromDate(UTCToLocalTime(point.getTime())));
                theStageTypeIcon.setImageDrawable(getStageLineIcon(getStopStageDrawable(stageType)));
                stageLineViews.add(stageLineItemView);
            }
            else{
                String theStageLineName = point.getName();
                theStageLabel.setText(theStageLineName);
                subHeaderLabel.setVisibility(View.GONE);
                operatorLabel.setVisibility(View.GONE);
                theStageLabel.setPadding(0,15,0,15);
                stageLineTime.setText(extractTimeFromDate(UTCToLocalTime(point.getTime())));
                theStageLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, theView.getResources().getDimensionPixelSize(R.dimen.overview_text_size_sub_header));
                theStageTypeIcon.setImageDrawable(getStageLineIcon(getPointToPointDrawable(stageType)));
                intermediateStopsToExpand.add(stageLineItemView);
                if(hasIntermediates == false){
                    stageLineViews.add(intermediateLayout);
                    addIntermediateOverflowRow();
                    hasIntermediates = true;
                }
            }
        }

        if(hasIntermediates == true){
            intermediateStopCountLabel.setText(intermediateStopsToExpand.size()+" intermediate stops to expand");
        }
    }

    public View addLineStages(){
        stageViewRootLayout.removeAllViews();
        for(int i=0;i<stageLineViews.size();i++){
            stageViewRootLayout.addView(stageLineViews.get(i));
        }
        return stageView;
    }


    public String extractTimeFromDate(String dateTime){
        int startIndex = dateTime.indexOf("T");
        String tempHourMinute = dateTime.substring(startIndex+1,dateTime.length());
        return tempHourMinute;
    }


    public void addIntermediateStages(){
        intermediateLayout.removeAllViews();
        intermediatesExpanded = true;
        for(int i=0;i<intermediateStopsToExpand.size();i++){
            intermediateLayout.addView(intermediateStopsToExpand.get(i));
        }
    }

    public void collapseIntermediates(){
        intermediateLayout.removeAllViews();
        addIntermediateOverflowRow();
        if(hasIntermediates == true){
            intermediateStopCountLabel.setText(intermediateStopsToExpand.size()+" intermediate stops to expand");
        }
        intermediatesExpanded = false;
    }

    public String UTCToLocalTime(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date dateToConvert = null;
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            dateToConvert = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(dateToConvert).toString();
    }
}
