package adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import mobi.wimt.findmyway.R;

/**
 * Created by James on 2015-01-07.
 * This is a custom adapter that deals with the placing and populating
 * of the navigation drawer. The individual items are specified in their
 * own layout file and are replicated 5 times, one for each menu option
 */
public class CustomDrawerAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final String[] options;

    public CustomDrawerAdapter(Context context, String[] options){
        super(context, R.layout.drawer_list_item,options);
        this.context = context;
        this.options = options;
    }

    @Override
    public View getView(int position,View convertView, ViewGroup parent) {

        LayoutInflater theInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View theRowView = theInflater.inflate(R.layout.drawer_list_item, parent, false);

        TextView theOption = (TextView) theRowView.findViewById(R.id.label);
        ImageView theIcon = (ImageView) theRowView.findViewById(R.id.Icon);

        View divider = (View) theRowView.findViewById(R.id.divider);//get the divider for each item
        theOption.setText(options[position]);

        switch (position) {
            case 0:
                theIcon.setImageResource(R.drawable.ic_action_favorite);
                divider.setBackgroundColor(Color.WHITE);//make the divider invisible for all apart from two menu options
                break;
            case 1:
                theIcon.setImageResource(R.drawable.ic_action_event);
                break;
            default:
                break;
        }

        return theRowView;
    }

 }
