package adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import mobi.wimt.findmyway.AnnouncementItemExpanded;
import mobi.wimt.findmyway.AnnouncementViewHolderExpanded;
import mobi.wimt.findmyway.R;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by James on 2015-01-19.
 */
public class AnnouncementsRecyclerExpanded extends RecyclerView.Adapter<AnnouncementViewHolderExpanded> {

    private List<AnnouncementItemExpanded> allAnnouncements;
    private int lastPosition = -1;
    private Activity context;

    public AnnouncementsRecyclerExpanded(ArrayList<AnnouncementItemExpanded> theAnnouncements,Activity context){
        this.allAnnouncements = theAnnouncements;
        this.context = context;
    }

    @Override
    public AnnouncementViewHolderExpanded onCreateViewHolder(ViewGroup parent, int position) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.announcement_line, parent, false);
        return new AnnouncementViewHolderExpanded(itemView,context);
    }

    @Override
    public void onBindViewHolder(AnnouncementViewHolderExpanded holder, int position) {
        checkForLinks(allAnnouncements.get(position).announcement, holder.theAnnouncementLabel);
        holder.operatorLogo.setImageResource(allAnnouncements.get(position).operatorImage);
        setAnimation(holder.theContainer, position);
     }

    private void setAnimation(View viewToAnimate, int position)
    {
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void checkForLinks(String announcement,TextView view){
        if(announcement.contains("http://")){
            int linkStart = announcement.indexOf("http://");
            int linkEnd = announcement.indexOf(" ",linkStart);
            if(linkEnd == -1){
                linkEnd = announcement.length();
            }
            String store = announcement.substring(linkStart,linkEnd);
            String Beginning = (announcement.substring(0,linkStart));
            String End = announcement.substring(linkEnd,announcement.length());
            String link = String.format("<a href=\"%s\">"+store+"</a> ", store);
            String ToConvert = Beginning+link+End;
            view.setText(Html.fromHtml(ToConvert));
            view.setMovementMethod(android.text.method.LinkMovementMethod.getInstance());
        }
        else{
            view.setText(announcement);
        }
    }


    @Override
    public int getItemCount() {
            return allAnnouncements.size();
        }

    public void updateList(List<AnnouncementItemExpanded> data) {
        allAnnouncements = data;
        notifyDataSetChanged();
    }

    public void addItem(int position, AnnouncementItemExpanded data) {
        allAnnouncements.add(position, data);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        allAnnouncements.remove(position);
        notifyItemRemoved(position);
     }

}
