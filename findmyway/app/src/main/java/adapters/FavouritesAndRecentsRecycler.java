package adapters;

import mobi.wimt.findmyway.FavouriteOrRecent;

/**
 * Created by James on 2015-01-21.
 */
public interface FavouritesAndRecentsRecycler {

    public void returnResultToParent(FavouriteOrRecent tempFavourite);

}
