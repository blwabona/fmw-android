package models;

import java.util.Date;

/**
 * Created by James on 2015-02-02.
 */
public class StagePointModel {

    public String name;
    public String description;
    public String time;
    public String type;
    public String platformNumber;
    public CoordinateModel point;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPlatformNumber() {
        return platformNumber;
    }

    public void setPlatformNumber(String platformNumber) {
        this.platformNumber = platformNumber;
    }

    public CoordinateModel getPoint() {
        return point;
    }

    public void setPoint(CoordinateModel point) {
        this.point = point;
    }
}
