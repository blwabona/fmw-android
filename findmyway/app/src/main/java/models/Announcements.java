package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Announcements {

    private List<AnnouncementModel> announcements = new ArrayList<AnnouncementModel>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The announcements
     */
    public List<AnnouncementModel> getAnnouncements() {
        return announcements;
    }

    /**
     *
     * @param announcements
     * The announcements
     */
    public void setAnnouncements(List<AnnouncementModel> announcements) {
        this.announcements = announcements;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
