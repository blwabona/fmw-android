package models;


/**
 * Created by James on 2015-01-28.
 */
public class SearchItemModel {

    private CoordinateModel point;
    private String name;
    private String description;
    private String resourceId;
    private float distance;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CoordinateModel getPoint() {
        return point;
    }

    public void setPoint(CoordinateModel coordinates) {
        this.point = point;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

}
