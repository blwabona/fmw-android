package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by James on 2015-01-27.
 */
public class AnnouncementModel {


    private CoordinateModel point;
    private String announcementType;
    private String operator;
    private List<String> modes = new ArrayList<String>();
    private String description;
    private Date startDate;
    private Date endDate;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public void setPoint(CoordinateModel point) {
        this.point = point;
    }


    public void setAnnouncementType(String announcementType) {
        this.announcementType = announcementType;
    }


    public void setOperator(String operator) {
        this.operator = operator;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }


    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


    public String getAnnouncementType() {
        return announcementType;
    }


    public String getOperator() {
        return operator;
    }


    public String getDescription() {
        return description;
    }


    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }


    public CoordinateModel getPoint() {
        return point;
    }


    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    /**
     *
     * @return
     * The modes
     */
    public List<String> getModes() {
        return modes;
    }

    /**
     *
     * @param modes
     * The modes
     */
    public void setModes(List<String> modes) {
        this.modes = modes;
    }

}
