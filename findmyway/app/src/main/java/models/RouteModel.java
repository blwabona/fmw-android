package models;

/**
 * Created by James on 2015-02-24.
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RouteModel {

    private int totalDistance;
    private int estimatedTotalTime;
    private CoordinateModel boundingBoxTopLeft;
    private CoordinateModel boundingBoxBottomRight;
    private List<StepModel> steps = new ArrayList<StepModel>();
    private List<CoordinateModel> points = new ArrayList<CoordinateModel>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The totalDistance
     */
    public int getTotalDistance() {
        return totalDistance;
    }

    /**
     *
     * @param totalDistance
     * The totalDistance
     */
    public void setTotalDistance(int totalDistance) {
        this.totalDistance = totalDistance;
    }

    /**
     *
     * @return
     * The estimatedTotalTime
     */
    public int getEstimatedTotalTime() {
        return estimatedTotalTime;
    }

    /**
     *
     * @param estimatedTotalTime
     * The estimatedTotalTime
     */
    public void setEstimatedTotalTime(int estimatedTotalTime) {
        this.estimatedTotalTime = estimatedTotalTime;
    }

    /**
     *
     * @return
     * The boundingBoxTopLeft
     */
    public CoordinateModel getBoundingBoxTopLeft() {
        return boundingBoxTopLeft;
    }

    /**
     *
     * @param boundingBoxTopLeft
     * The boundingBoxTopLeft
     */
    public void setBoundingBoxTopLeft(CoordinateModel boundingBoxTopLeft) {
        this.boundingBoxTopLeft = boundingBoxTopLeft;
    }

    /**
     *
     * @return
     * The boundingBoxBottomRight
     */
    public CoordinateModel getBoundingBoxBottomRight() {
        return boundingBoxBottomRight;
    }

    /**
     *
     * @param boundingBoxBottomRight
     * The boundingBoxBottomRight
     */
    public void setBoundingBoxBottomRight(CoordinateModel boundingBoxBottomRight) {
        this.boundingBoxBottomRight = boundingBoxBottomRight;
    }

    /**
     *
     * @return
     * The steps
     */
    public List<StepModel> getSteps() {
        return steps;
    }

    /**
     *
     * @param stepModels
     * The steps
     */
    public void setSteps(List<StepModel> stepModels) {
        this.steps = stepModels;
    }

    /**
     *
     * @return
     * The points
     */
    public List<CoordinateModel> getPoints() {
        return points;
    }

    /**
     *
     * @param points
     * The points
     */
    public void setPoints(List<CoordinateModel> points) {
        this.points = points;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
