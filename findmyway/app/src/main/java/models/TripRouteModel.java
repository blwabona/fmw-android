package models;

/**
 * Created by James on 2015-03-02.
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class TripRouteModel {

    private String colour;
    private List<CoordinateModel> points = new ArrayList<CoordinateModel>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The colour
     */
    public String getColour() {
        return colour;
    }

    /**
     *
     * @param colour
     * The colour
     */
    public void setColour(String colour) {
        this.colour = colour;
    }

    /**
     *
     * @return
     * The points
     */
    public List<CoordinateModel> getPoints() {
        return points;
    }

    /**
     *
     * @param points
     * The points
     */
    public void setPoints(List<CoordinateModel> points) {
        this.points = points;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
