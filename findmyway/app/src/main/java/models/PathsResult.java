

package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PathsResult {

    private String tripId;
    private String startTime;
    private String endTime;
    private String estimatedTotalCost;
    private List<String> fareMessages = new ArrayList<String>();
    private int totalWalkingDistance;
    private int initialWalkingDistance;
    private List<StageModel> stages;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The tripId
     */
    public String getTripId() {
        return tripId;
    }

    /**
     *
     * @param tripId
     * The tripId
     */
    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    /**
     *
     * @return
     * The arrivingTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     *
     * @param startTime
     * The arrivingTime
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     *
     * @return
     * The departingTime
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     *
     * @param endTime
     * The departingTime
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     *
     * @return
     * The estimatedTotalCost
     */
    public String getEstimatedTotalCost() {
        return estimatedTotalCost;
    }

    /**
     *
     * @param estimatedTotalCost
     * The estimatedTotalCost
     */
    public void setEstimatedTotalCost(String estimatedTotalCost) {
        this.estimatedTotalCost = estimatedTotalCost;
    }

    /**
     *
     * @return
     * The fareMessages
     */
    public List<String> getFareMessages() {
        return fareMessages;
    }

    /**
     *
     * @param fareMessages
     * The fareMessages
     */
    public void setFareMessages(List<String> fareMessages) {
        this.fareMessages = fareMessages;
    }

    /**
     *
     * @return
     * The totalWalkingDistance
     */
    public int getTotalWalkingDistance() {
        return totalWalkingDistance;
    }

    /**
     *
     * @param totalWalkingDistance
     * The totalWalkingDistance
     */
    public void setTotalWalkingDistance(int totalWalkingDistance) {
        this.totalWalkingDistance = totalWalkingDistance;
    }

    /**
     *
     * @return
     * The initialWalkingDistance
     */
    public int getInitialWalkingDistance() {
        return initialWalkingDistance;
    }

    /**
     *
     * @param initialWalkingDistance
     * The initialWalkingDistance
     */
    public void setInitialWalkingDistance(int initialWalkingDistance) {
        this.initialWalkingDistance = initialWalkingDistance;
    }

    /**
     *
     * @return
     * The stages
     */
    public List<StageModel> getStages() {
        return stages;
    }

    /**
     *
     * @param stages
     * The stages
     */
    public void setStages(List<StageModel> stages) {
        this.stages = stages;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
