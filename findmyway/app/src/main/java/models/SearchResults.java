package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by James on 2015-01-28.
 */
public class SearchResults {

    private List<SearchItemModel> searchItems = new ArrayList<SearchItemModel>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The searchItems
     */
    public List<SearchItemModel> getResults() {
        return searchItems;
    }

    /**
     *
     * @param searchItems
     * The searchItems
     */
    public void setResults(List<SearchItemModel> searchItems) {
        this.searchItems = searchItems;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }


}
