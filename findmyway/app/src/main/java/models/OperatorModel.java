package models;

/**
 * Created by James on 2015-02-28.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OperatorModel {

    private String name;
    private String displayName;
    private List<String> modes = new ArrayList<String>();
    private String category;
    private String twitterHandle;
    private String facebookPage;
    private String websiteAddress;
    private String routeMapUrl;
    private String contactEmail;
    private String contactNumber;
    private boolean isPublic;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     *
     * @param displayName
     * The displayName
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     *
     * @return
     * The modes
     */
    public List<String> getModes() {
        return modes;
    }

    /**
     *
     * @param modes
     * The modes
     */
    public void setModes(List<String> modes) {
        this.modes = modes;
    }

    /**
     *
     * @return
     * The category
     */
    public String getCategory() {
        return category;
    }

    /**
     *
     * @param category
     * The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     *
     * @return
     * The twitterHandle
     */
    public String getTwitterHandle() {
        return twitterHandle;
    }

    /**
     *
     * @param twitterHandle
     * The twitterHandle
     */
    public void setTwitterHandle(String twitterHandle) {
        this.twitterHandle = twitterHandle;
    }

    /**
     *
     * @return
     * The facebookPage
     */
    public String getFacebookPage() {
        return facebookPage;
    }

    /**
     *
     * @param facebookPage
     * The facebookPage
     */
    public void setFacebookPage(String facebookPage) {
        this.facebookPage = facebookPage;
    }

    /**
     *
     * @return
     * The websiteAddress
     */
    public String getWebsiteAddress() {
        return websiteAddress;
    }

    /**
     *
     * @param websiteAddress
     * The websiteAddress
     */
    public void setWebsiteAddress(String websiteAddress) {
        this.websiteAddress = websiteAddress;
    }

    /**
     *
     * @return
     * The routeMapUrl
     */
    public String getRouteMapUrl() {
        return routeMapUrl;
    }

    /**
     *
     * @param routeMapUrl
     * The routeMapUrl
     */
    public void setRouteMapUrl(String routeMapUrl) {
        this.routeMapUrl = routeMapUrl;
    }

    /**
     *
     * @return
     * The contactEmail
     */
    public String getContactEmail() {
        return contactEmail;
    }

    /**
     *
     * @param contactEmail
     * The contactEmail
     */
    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    /**
     *
     * @return
     * The contactNumber
     */
    public String getContactNumber() {
        return contactNumber;
    }

    /**
     *
     * @param contactNumber
     * The contactNumber
     */
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    /**
     *
     * @return
     * The isPublic
     */
    public boolean getIsPublic() {
        return isPublic;
    }

    /**
     *
     * @param isPublic
     * The isPublic
     */
    public void setIsPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
