package models;

/**
 * Created by James on 2015-03-11.
 */
public class MessagePost {

    public String emailAddress;
    public String subject;
    public String body;

    public MessagePost(){

    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFormattedMessage(){
        String store ="";
        store = emailAddress+"/n/n"+subject+"/n/n"+body;
        return store;
    }


}
