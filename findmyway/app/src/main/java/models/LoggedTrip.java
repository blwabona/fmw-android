package models;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by James on 2015-03-14.
 */
public class LoggedTrip {

    public String startLocation;
    public String endLocation;
    public String creationDate;
    public int carbonPoints;
    public TextView startLocationLabel;
    public TextView endLocationLabel;
    public TextView dateCreatedLabel;
    public TextView carbonPointsLabel;
    public View rowView;

    public LoggedTrip(String creationDate,String startLocation,String endLocation,int carbonPoints){
        this.carbonPoints = carbonPoints;
        this.creationDate = creationDate;
        this.endLocation = endLocation;
        this.startLocation = startLocation;
    }

    public LoggedTrip(String creationDate,String startLocation,String endLocation,int carbonPoints,TextView startLocationLabel,TextView endLocationLabel,TextView dateCreatedLabel,TextView carbonPointsLabel,View rowView){
        this.carbonPoints = carbonPoints;
        this.creationDate = creationDate;
        this.endLocation = endLocation;
        this.startLocation = startLocation;
        this.rowView = rowView;
        this.carbonPointsLabel = carbonPointsLabel;
        this.startLocationLabel = startLocationLabel;
        this.endLocationLabel = endLocationLabel;
        this.dateCreatedLabel = dateCreatedLabel;
        setupViewElements();
    }

    private void setupViewElements() {
        startLocationLabel.setText("From : "+startLocation);
        endLocationLabel.setText("To : "+endLocation);
        dateCreatedLabel.setText(extractTimeFromDate(creationDate));
        carbonPointsLabel.setText(String.valueOf(carbonPoints));
    }

    private String extractTimeFromDate(String date){
        int startIndex = date.indexOf("T");
        int endIndex = date.length();
        return date.substring(startIndex+1,endIndex).replace(":","h");
    }


}
