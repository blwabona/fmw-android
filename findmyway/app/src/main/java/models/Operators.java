package models;

/**
 * Created by James on 2015-02-28.
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Operators {

    private List<OperatorModel> operators = new ArrayList<OperatorModel>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The operators
     */
    public List<OperatorModel> getOperators() {
        return operators;
    }

    /**
     *
     * @param operators
     * The operators
     */
    public void setOperators(List<OperatorModel> operators) {
        this.operators = operators;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

