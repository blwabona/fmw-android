package models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by James on 2015-02-02.
 */
public class StageModel {

    public String name;
    public String description;
    public String operator;
    public String mode;
    public String colour;
    public String vehicleNumber;
    public float duration;
    public String cost;
    public List<StagePointModel> stageLocations;
    public List<TripAnnouncementModel> announcements;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public List<StagePointModel> getStageLocations() {
        return stageLocations;
    }

    public void setStageLocations(List<StagePointModel> stageLocations) {
        this.stageLocations = stageLocations;
    }

    public List<TripAnnouncementModel> getAnnouncements() {
        return announcements;
    }

    public void setAnnouncements(List<TripAnnouncementModel> announcements) {
        this.announcements = announcements;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

}
