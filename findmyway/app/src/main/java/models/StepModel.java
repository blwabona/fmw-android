package models;

/**
 * Created by James on 2015-02-24.
 */

import java.util.HashMap;
import java.util.Map;

public class StepModel {

    private int order;
    private String instructions;
    private int distance;
    private int duration;
    private CoordinateModel point;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The order
     */
    public int getOrder() {
        return order;
    }

    /**
     *
     * @param order
     * The order
     */
    public void setOrder(int order) {
        this.order = order;
    }

    /**
     *
     * @return
     * The instructions
     */
    public String getInstructions() {
        return instructions;
    }

    /**
     *
     * @param instructions
     * The instructions
     */
    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    /**
     *
     * @return
     * The distance
     */
    public int getDistance() {
        return distance;
    }

    /**
     *
     * @param distance
     * The distance
     */
    public void setDistance(int distance) {
        this.distance = distance;
    }

    /**
     *
     * @return
     * The duration
     */
    public int getDuration() {
        return duration;
    }

    /**
     *
     * @param duration
     * The duration
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     *
     * @return
     * The point
     */
    public CoordinateModel getPoint() {
        return point;
    }

    /**
     *
     * @param point
     * The point
     */
    public void setPoint(CoordinateModel point) {
        this.point = point;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

