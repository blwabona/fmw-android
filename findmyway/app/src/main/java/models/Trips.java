package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Created by James on 2015-03-02.
 */
public class Trips {
    private CoordinateModel boundingBoxTopLeft;
    private CoordinateModel boundingBoxBottomRight;
    private List<TripRouteModel> routes = new ArrayList<TripRouteModel>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The boundingBoxTopLeft
     */
    public CoordinateModel getBoundingBoxTopLeft() {
        return boundingBoxTopLeft;
    }

    /**
     *
     * @param boundingBoxTopLeft
     * The boundingBoxTopLeft
     */
    public void setBoundingBoxTopLeft(CoordinateModel boundingBoxTopLeft) {
        this.boundingBoxTopLeft = boundingBoxTopLeft;
    }

    /**
     *
     * @return
     * The boundingBoxBottomRight
     */
    public CoordinateModel getBoundingBoxBottomRight() {
        return boundingBoxBottomRight;
    }

    /**
     *
     * @param boundingBoxBottomRight
     * The boundingBoxBottomRight
     */
    public void setBoundingBoxBottomRight(CoordinateModel boundingBoxBottomRight) {
        this.boundingBoxBottomRight = boundingBoxBottomRight;
    }

    /**
     *
     * @return
     * The routes
     */
    public List<TripRouteModel> getRoutes() {
        return routes;
    }

    /**
     *
     * @param routes
     * The routes
     */
    public void setRoutes(List<TripRouteModel> routes) {
        this.routes = routes;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
