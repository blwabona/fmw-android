package models;

import java.util.HashMap;
import java.util.Map;

public class About {

    private String developer;
    private String webAddress;
    private String emailAddress;
    private String locationPolicy;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The developer
     */
    public String getDeveloper() {
        return developer;
    }

    /**
     *
     * @param developer
     * The developer
     */
    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    /**
     *
     * @return
     * The webAddress
     */
    public String getWebAddress() {
        return webAddress;
    }

    /**
     *
     * @param webAddress
     * The webAddress
     */
    public void setWebAddress(String webAddress) {
        this.webAddress = webAddress;
    }

    /**
     *
     * @return
     * The emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     *
     * @param emailAddress
     * The emailAddress
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     *
     * @return
     * The locationPolicy
     */
    public String getLocationPolicy() {
        return locationPolicy;
    }

    /**
     *
     * @param locationPolicy
     * The locationPolicy
     */
    public void setLocationPolicy(String locationPolicy) {
        this.locationPolicy = locationPolicy;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}