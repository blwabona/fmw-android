package api;

/**
 * Created by James on 2015-01-26.
 */
import android.content.Context;
import android.content.SharedPreferences;

import com.squareup.okhttp.OkHttpClient;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import models.MessageFindMyWay;
import models.Users;

import models.About;
import models.Announcements;
import models.Operators;
import models.Paths;
import models.Routes;
import models.SearchResults;
import models.Trips;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedString;


public class ApiClient {

    private static wimtApiInterface wimtApiService;
    private static findmywayApiInterface findmywayApiService;
    public static final String TOKEN = "TheUserToken";
    public static boolean mapsToken = false;
    public static boolean messageToken = false;

    public static wimtApiInterface getwimtApiClient(final boolean requiresToken, final Context context){
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60*1000, TimeUnit.MILLISECONDS);
        if(wimtApiService == null){
            RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("http://wimtgateway.wimt.co.za/")
                    .setRequestInterceptor(new RequestInterceptor() {
                        @Override
                        public void intercept(RequestFacade request) {
                            request.addHeader("Accept","application/json");
                            request.addHeader("Content-type","application/json");
                            if(mapsToken == true){
                                System.out.println("Token "+getToken(context));
                                request.addHeader("Token",getToken(context));
                            }
                        }
                    }).build();
            wimtApiService =restAdapter.create(wimtApiInterface.class);
        }

        return wimtApiService;
    }

    public static String getToken(Context context){
        SharedPreferences prefs = context.getSharedPreferences(TOKEN, context.MODE_PRIVATE);
        Map<String,?> keys = prefs.getAll();
        String theToken = "";
        if(keys == null || keys.size()<=0){
            return null;
        }
        else {
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                if(entry.getKey().equals("Token")){
                    theToken = entry.getValue().toString();
                }
            }
        }
        return theToken;
    }

    public static findmywayApiInterface getfindmywayApiClient(final Context context){
        if(findmywayApiService == null){
            RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("https://fmwgateway.wimt.co.za/").setRequestInterceptor(new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade request) {
                    request.addHeader("Accept","application/json");
                    request.addHeader("Content-type","application/json");
                    if(messageToken == true){
                        request.addPathParam("token",getToken(context));
                        request.addHeader("Token", getToken(context));
                    }
                }
            }).build();;
            findmywayApiService =restAdapter.create(findmywayApiInterface.class);
        }

        return findmywayApiService;
    }

    public interface findmywayApiInterface{
        @Headers({
                "AppKey:11E46DD4-D9D7-40EF-8356-41796C0CBD75"
        })
        @POST("/v1/users")
        public void getUserToken(Callback<Users> callback);

        @Headers({
                "AppKey:11E46DD4-D9D7-40EF-8356-41796C0CBD75"
        })
        @POST("/v1/users/{token}/messages")
        void postMessage(@Body MessageFindMyWay MessagePost,Callback<Object> callback);
    }

    public interface wimtApiInterface {

        @Headers({
                "AppKey:11E46DD4-D9D7-40EF-8356-41796C0CBD75"
        })

        @GET("/v1/about")
        public About getAbout();

        @Headers({
                "AppKey:11E46DD4-D9D7-40EF-8356-41796C0CBD75"
        })
        @GET("/v1/Announcements")
        public void getAnnouncements(Callback<Announcements> callback);

        @Headers({
                "AppKey:11E46DD4-D9D7-40EF-8356-41796C0CBD75"
        })
        @GET("/v1/SearchItems")
        public void getSearchResults(@Query("SearchText") String searchText, @Query("Latitude") String latitude, @Query("Longitude") String longitude, Callback<SearchResults> callback);

        @Headers({
                "AppKey:11E46DD4-D9D7-40EF-8356-41796C0CBD75"
        })
        @GET("/v1/Paths")
        public void getPathResults(@Query("StartLatitude") double startLatitude,
                                   @Query("StartLongitude") double startLongitude,
                                   @Query("EndLatitude") double endLatitude,
                                   @Query("EndLongitude") double endLongitude,
                                   @Query("StartDate") String startDate,
                                   @Query("EndDate") String endDate,
                                   @Query("ExcludedModes[]") String[] excludedModes,
                                   @Query("ExcludedOperators[]") String[] excludedOperators, Callback<Paths> callback);

        @Headers({
                "AppKey:11E46DD4-D9D7-40EF-8356-41796C0CBD75"
        })
        @GET("/v1/Routes")
        public void getRoutes(@Query("StartLatitude") double startLatitude,
                              @Query("StartLongitude") double startLongitude,
                              @Query("EndLatitude") double endLatitude,
                              @Query("EndLongitude") double endLongitude,
                              @Query("WithDirections") boolean withDirections,
                              @Query("WithTraffic") boolean withTraffic,
                              @Query("AlternativeRoutes") int alternativeRoutes,
                              @Query("Mode") String mode, Callback<Routes> callback);

        @Headers({
                "AppKey:11E46DD4-D9D7-40EF-8356-41796C0CBD75"
        })
        @GET("/v1/Operators")
        public void getOperators(Callback<Operators> callback);

        @Headers({
                "AppKey:11E46DD4-D9D7-40EF-8356-41796C0CBD75"
        })
        @GET("/v1/Trips")
        public void getTripRoutes(@Query("TripId") String tripId, Callback<Trips> callback);

    }
}
